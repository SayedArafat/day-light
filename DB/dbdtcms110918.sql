-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 12, 2018 at 01:34 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbdtcms`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_06_26_090837_create_expense_category_table', 1),
(4, '2018_06_26_090903_create_expense_table', 1),
(5, '2018_06_26_154834_create_employee_salary_table', 1),
(6, '2018_06_26_155128_create_employee_salary_history_table', 1),
(7, '2018_06_27_053950_create_designations_table', 1),
(8, '2018_06_27_054003_create_departments_table', 1),
(9, '2018_06_29_054407_create_activity_logs_table', 1),
(10, '2018_06_29_104425_create_give_loan_table', 1),
(11, '2018_06_29_104612_create_received_loan_table', 1),
(12, '2018_06_29_104636_create_received_loan_history_table', 1),
(13, '2018_06_29_104657_create_give_loan_history_table', 1),
(14, '2018_07_03_100152_create_advance_payment_table', 1),
(15, '2018_07_03_100521_create_advance_payment_history_table', 1),
(16, '2018_07_11_102115_create_billcode_table', 1),
(17, '2018_07_21_033646_create_income_source_category_table', 1),
(20, '2018_07_29_144215_create_orders_details_table', 1),
(21, '2018_07_29_145327_create_customers_table', 1),
(26, '2018_08_11_134201_create_billcodeitems_table', 2),
(29, '2018_07_29_143520_create_orders_table', 4),
(34, '2018_08_20_105218_create_finish_part_table', 6),
(35, '2018_07_21_033731_create_income_sources_table', 7),
(36, '2018_09_03_103842_add_delivered_column_to_orders_table', 7),
(39, '2018_08_20_083203_create_stock_fabric_table', 8),
(43, '2018_09_04_083204_create_bank_transaction_table', 9),
(44, '2018_08_15_092208_create_tbcompany_info_table', 10),
(47, '2018_09_08_072708_create_lc_info_table', 11),
(48, '2018_08_20_083715_create_bank_info_table', 12);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) NOT NULL,
  `token` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbactivity_logs`
--

CREATE TABLE `tbactivity_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `activity` text NOT NULL,
  `createdBy` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbadvance_payment`
--

CREATE TABLE `tbadvance_payment` (
  `id` int(10) UNSIGNED NOT NULL,
  `empId` tinyint(4) NOT NULL,
  `remarks` text,
  `paymentDate` date DEFAULT NULL,
  `createdBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbadvance_payment_history`
--

CREATE TABLE `tbadvance_payment_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `advancePaymentId` tinyint(4) NOT NULL,
  `paymentType` tinyint(4) DEFAULT NULL,
  `expenseTitle` varchar(191) DEFAULT NULL,
  `categoryName` varchar(191) DEFAULT NULL,
  `amount` varchar(10) DEFAULT NULL,
  `remarks` text,
  `aphDate` date DEFAULT NULL,
  `createdBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbbank_info`
--

CREATE TABLE `tbbank_info` (
  `id` int(10) UNSIGNED NOT NULL,
  `bankName` text,
  `bankAccountNo` varchar(191) DEFAULT NULL,
  `bankSwiftCode` varchar(191) DEFAULT NULL,
  `bankVatNo` varchar(191) DEFAULT NULL,
  `bankAddress` varchar(191) DEFAULT NULL,
  `remarks` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbbank_info`
--

INSERT INTO `tbbank_info` (`id`, `bankName`, `bankAccountNo`, `bankSwiftCode`, `bankVatNo`, `bankAddress`, `remarks`, `created_at`, `updated_at`) VALUES
(2, 'JAMUNA BANK LTD.', '00370210018879', 'JAMUNABDDH054', '000618307', 'Uttara 11, Dhaka', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbbank_transaction`
--

CREATE TABLE `tbbank_transaction` (
  `id` int(10) UNSIGNED NOT NULL,
  `payType` tinyint(4) DEFAULT NULL,
  `bankId` int(11) DEFAULT NULL,
  `creditAmount` varchar(15) DEFAULT NULL,
  `debitAmount` varchar(15) DEFAULT NULL,
  `transDate` date DEFAULT NULL,
  `reference` varchar(191) DEFAULT NULL,
  `description` text,
  `paymentMethod` tinyint(4) DEFAULT NULL,
  `createdBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbbank_transaction`
--

INSERT INTO `tbbank_transaction` (`id`, `payType`, `bankId`, `creditAmount`, `debitAmount`, `transDate`, `reference`, `description`, `paymentMethod`, `createdBy`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '50000', NULL, '2018-09-05', 'Rima', 'None', 0, 3, '2018-09-05 02:36:24', '2018-09-05 02:36:24'),
(3, 2, 2, NULL, '2500', '2018-09-05', 'ALif', 'None', 0, 3, '2018-09-05 03:57:13', '2018-09-05 03:57:13'),
(4, 1, 2, '4500', NULL, '2018-09-05', 'Rima', 'None', 0, 3, '2018-09-05 02:36:24', '2018-09-05 02:36:24'),
(5, 1, 2, '50000', NULL, '2018-09-09', NULL, NULL, 0, 3, '2018-09-08 22:22:11', '2018-09-08 22:22:11');

-- --------------------------------------------------------

--
-- Table structure for table `tbbillcode`
--

CREATE TABLE `tbbillcode` (
  `id` int(10) UNSIGNED NOT NULL,
  `billcode` varchar(191) DEFAULT NULL,
  `bDate` date DEFAULT NULL,
  `remarks` text,
  `createdBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbbillcode`
--

INSERT INTO `tbbillcode` (`id`, `billcode`, `bDate`, `remarks`, `createdBy`, `created_at`, `updated_at`) VALUES
(2, '101', '2018-08-14', 'None', 3, '2018-08-15 05:22:07', '2018-08-15 05:22:07'),
(3, '102', '2018-08-15', NULL, 3, '2018-08-15 05:31:00', '2018-08-15 05:31:00'),
(4, '103', '2018-08-29', NULL, 3, '2018-08-29 01:49:47', '2018-08-29 01:49:47');

-- --------------------------------------------------------

--
-- Table structure for table `tbbillcodeitems`
--

CREATE TABLE `tbbillcodeitems` (
  `id` int(10) UNSIGNED NOT NULL,
  `billcodeId` varchar(12) DEFAULT NULL,
  `bcPi_number` varchar(12) DEFAULT NULL,
  `bcOritem_id` varchar(12) DEFAULT NULL,
  `bcQuantity` varchar(10) DEFAULT NULL,
  `bcUnitPrice` varchar(12) DEFAULT NULL,
  `bcFrom` varchar(191) DEFAULT NULL,
  `bcTo` varchar(191) DEFAULT NULL,
  `bcRemarks` varchar(191) DEFAULT NULL,
  `createdBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbbillcodeitems`
--

INSERT INTO `tbbillcodeitems` (`id`, `billcodeId`, `bcPi_number`, `bcOritem_id`, `bcQuantity`, `bcUnitPrice`, `bcFrom`, `bcTo`, `bcRemarks`, `createdBy`, `created_at`, `updated_at`) VALUES
(1, '2', '1', '3', '450', '28', 'NAHAR', 'RONY', 'None', 3, '2018-08-15 05:25:32', '2018-08-15 05:25:32'),
(2, '2', '1', '4', '250', '30', 'BAHAR', 'JK Textile', 'None', 3, '2018-08-15 05:26:12', '2018-08-15 05:26:12'),
(3, '2', '1', '6', '950', '25', 'KAWSER', 'MM TEXTILE', 'None', 3, '2018-08-15 05:29:05', '2018-08-15 05:29:05'),
(4, '3', '1', '5', '120', '25', 'JJ', 'HHNAM', NULL, 3, '2018-08-15 05:31:26', '2018-08-15 05:31:26'),
(5, '3', '1', '3', '500', '27', 'BAHAR', 'GG TEXTILE', 'ZYTA', 3, '2018-08-20 09:49:08', '2018-08-20 09:49:08'),
(6, '4', '2', '7', '5000', '25', 'NAHAR', 'ZYTA', 'None', 3, '2018-08-29 01:50:11', '2018-08-29 01:50:11'),
(7, '4', '2', '7', '5000', '25', 'NAHAR', 'NUHA', 'ZYTA', 3, '2018-08-29 01:52:11', '2018-08-29 01:52:11');

-- --------------------------------------------------------

--
-- Table structure for table `tbcompany_info`
--

CREATE TABLE `tbcompany_info` (
  `id` int(10) UNSIGNED NOT NULL,
  `companyName` varchar(191) DEFAULT NULL,
  `companyTagline` varchar(191) DEFAULT NULL,
  `companyPhone` varchar(191) DEFAULT NULL,
  `companyEmail` varchar(191) DEFAULT NULL,
  `companyBAddress` text,
  `companyFAddress` text,
  `companyLogo` text,
  `companysignature1` text NOT NULL,
  `companysignature2` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbcompany_info`
--

INSERT INTO `tbcompany_info` (`id`, `companyName`, `companyTagline`, `companyPhone`, `companyEmail`, `companyBAddress`, `companyFAddress`, `companyLogo`, `companysignature1`, `companysignature2`, `created_at`, `updated_at`) VALUES
(1, 'Daylight Textile Corporation', '100% Export Oriented Fabrics Manufacturer & Supplier', '+88-02-48954746', 'info@daylighttextile-bd.com', 'House#29(5th floor), Road#05, Sector#10, Uttara, Dhaka-1230.', 'Monohorpur, Madabdi, Narshindhi, Bangladesh.', 'dlogo.png', '', '', NULL, NULL),
(2, 'Southfield TExtile Mills', '100% Export Oriented Fabrics Manufacturer & Supplier', '+88-02-48954746', 'info@southfieldbd.com.com', 'House#29(5th floor), Road#05, Sector#10, Uttara, Dhaka-1230.', 'Monohorpur, Madabdi, Narshindhi, Bangladesh.', 'slogo.png', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbcustomers`
--

CREATE TABLE `tbcustomers` (
  `id` int(10) UNSIGNED NOT NULL,
  `customerName` varchar(191) DEFAULT NULL,
  `customerPhone` varchar(191) DEFAULT NULL,
  `customerEmail` varchar(191) DEFAULT NULL,
  `customerAddress` varchar(191) DEFAULT NULL,
  `remarks` text,
  `createdBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbcustomers`
--

INSERT INTO `tbcustomers` (`id`, `customerName`, `customerPhone`, `customerEmail`, `customerAddress`, `remarks`, `createdBy`, `created_at`, `updated_at`) VALUES
(1, 'Manta Apparels Ltd.', '01824168996', 'manta@gmail.com', 'Jamghora, Ashulia, Savar, Dhaka', 'Khub valo customer', 3, NULL, '2018-08-15 05:05:55');

-- --------------------------------------------------------

--
-- Table structure for table `tbdepartments`
--

CREATE TABLE `tbdepartments` (
  `id` int(10) UNSIGNED NOT NULL,
  `departmentName` varchar(200) NOT NULL,
  `departmentDescription` text,
  `createdBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbdesignations`
--

CREATE TABLE `tbdesignations` (
  `id` int(10) UNSIGNED NOT NULL,
  `designationName` varchar(200) NOT NULL,
  `designationDescription` text,
  `createdBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbemployee_salary`
--

CREATE TABLE `tbemployee_salary` (
  `id` int(10) UNSIGNED NOT NULL,
  `empId` tinyint(4) NOT NULL,
  `amount` varchar(25) NOT NULL DEFAULT '0',
  `salaryMonth` date NOT NULL,
  `createdBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbemployee_salary_history`
--

CREATE TABLE `tbemployee_salary_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `empId` tinyint(4) NOT NULL,
  `paidAmount` varchar(25) NOT NULL DEFAULT '0',
  `paidDate` date NOT NULL,
  `createdBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbexpensecategory`
--

CREATE TABLE `tbexpensecategory` (
  `id` int(10) UNSIGNED NOT NULL,
  `categoryName` varchar(250) NOT NULL,
  `categoryDescription` text,
  `createdBy` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbexpenselist`
--

CREATE TABLE `tbexpenselist` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) NOT NULL,
  `categoryId` tinyint(4) NOT NULL,
  `amount` varchar(25) NOT NULL,
  `reference` varchar(191) DEFAULT NULL,
  `description` text,
  `expenseDate` date NOT NULL,
  `expenseAttachment` text,
  `createdBy` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbfinish_part`
--

CREATE TABLE `tbfinish_part` (
  `id` int(10) UNSIGNED NOT NULL,
  `fDate` date DEFAULT NULL,
  `billCodeItemId` varchar(15) DEFAULT NULL,
  `twill` varchar(10) DEFAULT NULL,
  `tc` varchar(10) DEFAULT NULL,
  `deliveryFactory` varchar(150) DEFAULT NULL,
  `comment` text,
  `createdBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbfinish_part`
--

INSERT INTO `tbfinish_part` (`id`, `fDate`, `billCodeItemId`, `twill`, `tc`, `deliveryFactory`, `comment`, `createdBy`, `created_at`, `updated_at`) VALUES
(1, '2018-08-20', '2', '', '1050', 'ZYTA', 'NONE', 1, NULL, NULL),
(2, '2018-08-21', '5', '', '1100', 'ZYTA', 'None', 3, '2018-08-20 21:49:03', '2018-08-20 21:49:03'),
(3, '2018-08-21', '3', NULL, '250', 'ZYTA', NULL, 3, '2018-08-20 21:50:19', '2018-08-20 21:50:19');

-- --------------------------------------------------------

--
-- Table structure for table `tbgive_loan`
--

CREATE TABLE `tbgive_loan` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) NOT NULL,
  `details` text,
  `address` text,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `remarks` text,
  `loanDate` date DEFAULT NULL,
  `createdBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbgive_loan_history`
--

CREATE TABLE `tbgive_loan_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `loanId` tinyint(4) NOT NULL,
  `paymentType` tinyint(4) DEFAULT NULL,
  `amount` varchar(10) DEFAULT NULL,
  `remarks` text,
  `paymentDate` date DEFAULT NULL,
  `createdBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbincome_sources`
--

CREATE TABLE `tbincome_sources` (
  `id` int(10) UNSIGNED NOT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `source` text,
  `payment_method` varchar(100) DEFAULT NULL,
  `bank_account` varchar(191) DEFAULT NULL,
  `reference` varchar(191) DEFAULT NULL,
  `description` text,
  `incomeDate` date DEFAULT NULL,
  `amount` varchar(15) DEFAULT NULL,
  `attachement` text,
  `createdBy` tinyint(4) DEFAULT NULL,
  `updatedBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbincome_source_category`
--

CREATE TABLE `tbincome_source_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `categoryName` varchar(191) DEFAULT NULL,
  `categoryDescription` varchar(191) DEFAULT NULL,
  `createdBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbincome_source_category`
--

INSERT INTO `tbincome_source_category` (`id`, `categoryName`, `categoryDescription`, `createdBy`, `created_at`, `updated_at`) VALUES
(1, 'Bank', 'None', 3, '2018-09-04 01:11:36', '2018-09-04 01:11:36');

-- --------------------------------------------------------

--
-- Table structure for table `tblc_info`
--

CREATE TABLE `tblc_info` (
  `id` int(10) UNSIGNED NOT NULL,
  `lcNumber` varchar(100) DEFAULT NULL,
  `lcDate` date DEFAULT NULL,
  `challanNo` varchar(100) DEFAULT NULL,
  `companyId` tinyint(4) DEFAULT NULL,
  `companyBankId` tinyint(4) DEFAULT NULL,
  `customerId` int(11) DEFAULT NULL,
  `customerBankName` varchar(191) DEFAULT NULL,
  `customerBankInfo` text,
  `sightDays` varchar(6) DEFAULT NULL,
  `contractNo` varchar(75) DEFAULT NULL,
  `contractDate` date DEFAULT NULL,
  `valueReceived` text,
  `IRCNo` varchar(100) DEFAULT NULL,
  `TINNo` varchar(100) DEFAULT NULL,
  `VatRegNo` varchar(100) DEFAULT NULL,
  `createdBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblc_info`
--

INSERT INTO `tblc_info` (`id`, `lcNumber`, `lcDate`, `challanNo`, `companyId`, `companyBankId`, `customerId`, `customerBankName`, `customerBankInfo`, `sightDays`, `contractNo`, `contractDate`, `valueReceived`, `IRCNo`, `TINNo`, `VatRegNo`, `createdBy`, `created_at`, `updated_at`) VALUES
(2, '075117042126', '2018-05-04', 'DTC-090318', 1, 2, 1, 'The City Bank Ltd.', 'TRADE SERVICES DIVISION, 25/A DILKUSHA C/A, Dhaka-1000, Bangladesh', '90', 'NEXT/16/2017', '2018-02-22', 'Trims fabric for 100% Export Oriented Garments Industry', 'BA-187109', '714445443869', '17111022780', 3, '2018-09-07 20:02:40', '2018-09-07 20:02:40');

-- --------------------------------------------------------

--
-- Table structure for table `tborders`
--

CREATE TABLE `tborders` (
  `id` int(10) UNSIGNED NOT NULL,
  `companyId` int(11) NOT NULL,
  `pi` varchar(191) DEFAULT NULL,
  `customerId` int(11) DEFAULT NULL,
  `orderDate` date DEFAULT NULL,
  `deliveryDate` date DEFAULT NULL,
  `shippingDate` date DEFAULT NULL,
  `goodsOrigin` varchar(191) DEFAULT NULL,
  `sources` varchar(191) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `remarks` text,
  `bankId` varchar(10) NOT NULL,
  `createdBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tborders`
--

INSERT INTO `tborders` (`id`, `companyId`, `pi`, `customerId`, `orderDate`, `deliveryDate`, `shippingDate`, `goodsOrigin`, `sources`, `status`, `remarks`, `bankId`, `createdBy`, `created_at`, `updated_at`) VALUES
(1, 1, 'DTC 010616', 1, '2018-08-15', '2018-09-05', NULL, 'Bangladesh', NULL, 0, 'None', '2', 3, '2018-08-15 05:04:41', '2018-08-15 05:04:41'),
(2, 1, 'DTC 01290818', 1, '2018-08-29', '2018-08-29', NULL, 'BD', NULL, 0, 'None', '', 3, '2018-08-29 01:48:27', '2018-08-29 01:48:27'),
(3, 0, 'DTC 010918', 1, '2018-09-01', '2018-09-20', NULL, 'Bangladesh', NULL, 0, NULL, '', 3, '2018-09-09 05:22:06', '2018-09-09 05:22:06');

-- --------------------------------------------------------

--
-- Table structure for table `tborders_details`
--

CREATE TABLE `tborders_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `rpiId` int(11) DEFAULT NULL,
  `item` varchar(255) NOT NULL,
  `style` varchar(191) DEFAULT NULL,
  `details` text,
  `color` varchar(191) DEFAULT NULL,
  `orderQuantity` varchar(25) DEFAULT NULL,
  `unitPrice` varchar(25) DEFAULT NULL,
  `createdBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delivered_quantity` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tborders_details`
--

INSERT INTO `tborders_details` (`id`, `rpiId`, `item`, `style`, `details`, `color`, `orderQuantity`, `unitPrice`, `createdBy`, `created_at`, `updated_at`, `delivered_quantity`) VALUES
(3, 1, 'TC-SHUTTLE', 'Style Ref: 412140(PS Mid Denim Shorts)', 'T/C 65% Polyester 35% Cotton, \r\nConst: 45*45/94*70\r\nWidth: 42/43\r\nNext: 105', 'NAVY (B.P)', '1100', '.90', 3, '2018-08-15 05:09:30', '2018-08-15 05:09:30', 0),
(4, 1, 'TC-SHUTTLE', 'Style Ref: 184141(Grey Denim Shorts)', 'T/C 65% Polyester 35% Cotton, \r\nConst: 45*45/94*70\r\nWidth: 42/43\r\nNext: 105', 'BLACK (B.P)', '1100', '.90', 3, '2018-08-15 05:10:29', '2018-08-15 05:10:29', 0),
(5, 1, 'TC SHUTTLE', 'ITEM No: 537906', 'T/C 65% Polyester 35% Cotton, Const: 45*45/94*70 Width: 42/43 Next: 105', 'WHITE TC', '2850', '.55', 3, '2018-08-15 05:11:57', '2018-08-15 05:11:57', 0),
(6, 1, 'WHITE TC', 'ITEM No: 185536(PS Regular Vintage Jean)', 'T/C 65% Polyester 35% Cotton, Const: 45*45/94*70 Width: 42/43 Next: 105', 'WHITE TC', '4700', '.55', 3, '2018-08-15 05:13:05', '2018-08-15 05:13:05', 0),
(7, 2, 'TC', 'Style Ref: 412140(PS Mid Denim Shorts)', 'T/C 65% Polyester 35% Cotton, Const: 45*45/94*70 Width: 42/43 Next: 105', 'NAVY', '25000', '.95', 3, '2018-08-29 01:49:37', '2018-08-29 01:49:37', 0),
(8, 1, 'TC', 'Item No : 613629,343280', 'TC(65/35) Pocketing \r\nConst : 45x45/110x70\r\nWidth : 42\"/43\"', 'NAVY(B.P)', '4550', '.90', 3, '2018-09-09 05:24:26', '2018-09-09 05:24:26', 0),
(9, 3, 'TC', 'Item No : 613629,343280', 'TC(65/35) Pocketing\r\nConst : 45x45/110x70\r\nWidth : 42\"/43\"', 'NAVY(B.P)', '4550', '.90', 3, '2018-09-09 05:26:35', '2018-09-09 05:26:35', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbreceived_loan`
--

CREATE TABLE `tbreceived_loan` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) NOT NULL,
  `details` text,
  `address` text,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `remarks` text,
  `loanDate` date DEFAULT NULL,
  `createdBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbreceived_loan_history`
--

CREATE TABLE `tbreceived_loan_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `loanId` tinyint(4) NOT NULL,
  `paymentType` tinyint(4) DEFAULT NULL,
  `amount` varchar(10) DEFAULT NULL,
  `remarks` text,
  `paymentDate` date DEFAULT NULL,
  `createdBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbstock_fabric`
--

CREATE TABLE `tbstock_fabric` (
  `id` int(10) UNSIGNED NOT NULL,
  `sfRef` varchar(191) DEFAULT NULL,
  `sfDate` date DEFAULT NULL,
  `sfItem` varchar(191) DEFAULT NULL,
  `sfColor` varchar(191) DEFAULT NULL,
  `sfPi` varchar(191) DEFAULT NULL,
  `sfQty` varchar(191) DEFAULT NULL,
  `sfDetails` varchar(191) DEFAULT NULL,
  `sfRemarks` varchar(191) DEFAULT NULL,
  `createdBy` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `employeeNumber` varchar(50) DEFAULT NULL,
  `userType` tinyint(4) NOT NULL DEFAULT '0',
  `email` varchar(191) NOT NULL,
  `password` varchar(191) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `gender` varchar(25) DEFAULT NULL,
  `accountStatus` varchar(191) NOT NULL DEFAULT '1',
  `currentAddress` text,
  `joiningDate` date DEFAULT NULL,
  `designationId` tinyint(4) DEFAULT NULL,
  `departmentId` tinyint(4) DEFAULT NULL,
  `salary` varchar(25) NOT NULL DEFAULT '0',
  `photo` text,
  `createdBy` tinyint(4) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `employeeNumber`, `userType`, `email`, `password`, `phone`, `gender`, `accountStatus`, `currentAddress`, `joiningDate`, `designationId`, `departmentId`, `salary`, `photo`, `createdBy`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'Admin', NULL, 1, 'admin@email.com', '$2y$10$onGVQTyOFDhoWhCkxaiEbeDl38Hd8HB3qp1pcQJMoRnPoxdB0kzhe', NULL, NULL, '1', NULL, NULL, NULL, NULL, '0', 'default.jpg', NULL, '3s93wKv3L6MAxVLDvmhhMmjiRJb5sftrYO0XzsCte6HWUfeCZJzOK420SfDF', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tbactivity_logs`
--
ALTER TABLE `tbactivity_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbadvance_payment`
--
ALTER TABLE `tbadvance_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbadvance_payment_history`
--
ALTER TABLE `tbadvance_payment_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbbank_info`
--
ALTER TABLE `tbbank_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbbank_transaction`
--
ALTER TABLE `tbbank_transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tbbank_transaction_paytype_index` (`payType`),
  ADD KEY `tbbank_transaction_bankid_index` (`bankId`);

--
-- Indexes for table `tbbillcode`
--
ALTER TABLE `tbbillcode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbbillcodeitems`
--
ALTER TABLE `tbbillcodeitems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tbbillcodeitems_billcodeid_index` (`billcodeId`),
  ADD KEY `tbbillcodeitems_bcpi_number_index` (`bcPi_number`),
  ADD KEY `tbbillcodeitems_bcoritem_id_index` (`bcOritem_id`);

--
-- Indexes for table `tbcompany_info`
--
ALTER TABLE `tbcompany_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbcustomers`
--
ALTER TABLE `tbcustomers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbdepartments`
--
ALTER TABLE `tbdepartments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbdesignations`
--
ALTER TABLE `tbdesignations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbemployee_salary`
--
ALTER TABLE `tbemployee_salary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbemployee_salary_history`
--
ALTER TABLE `tbemployee_salary_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbexpensecategory`
--
ALTER TABLE `tbexpensecategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbexpenselist`
--
ALTER TABLE `tbexpenselist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbfinish_part`
--
ALTER TABLE `tbfinish_part`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbgive_loan`
--
ALTER TABLE `tbgive_loan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbgive_loan_history`
--
ALTER TABLE `tbgive_loan_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbincome_sources`
--
ALTER TABLE `tbincome_sources`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tbincome_sources_categoryid_index` (`categoryId`);

--
-- Indexes for table `tbincome_source_category`
--
ALTER TABLE `tbincome_source_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblc_info`
--
ALTER TABLE `tblc_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tborders`
--
ALTER TABLE `tborders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tborders_pi_index` (`pi`),
  ADD KEY `tborders_customerid_index` (`customerId`);

--
-- Indexes for table `tborders_details`
--
ALTER TABLE `tborders_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tborders_details_rpiid_index` (`rpiId`);

--
-- Indexes for table `tbreceived_loan`
--
ALTER TABLE `tbreceived_loan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbreceived_loan_history`
--
ALTER TABLE `tbreceived_loan_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbstock_fabric`
--
ALTER TABLE `tbstock_fabric`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `tbactivity_logs`
--
ALTER TABLE `tbactivity_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbadvance_payment`
--
ALTER TABLE `tbadvance_payment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbadvance_payment_history`
--
ALTER TABLE `tbadvance_payment_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbbank_info`
--
ALTER TABLE `tbbank_info`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbbank_transaction`
--
ALTER TABLE `tbbank_transaction`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbbillcode`
--
ALTER TABLE `tbbillcode`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbbillcodeitems`
--
ALTER TABLE `tbbillcodeitems`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbcompany_info`
--
ALTER TABLE `tbcompany_info`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbcustomers`
--
ALTER TABLE `tbcustomers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbdepartments`
--
ALTER TABLE `tbdepartments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbdesignations`
--
ALTER TABLE `tbdesignations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbemployee_salary`
--
ALTER TABLE `tbemployee_salary`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbemployee_salary_history`
--
ALTER TABLE `tbemployee_salary_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbexpensecategory`
--
ALTER TABLE `tbexpensecategory`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbexpenselist`
--
ALTER TABLE `tbexpenselist`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbfinish_part`
--
ALTER TABLE `tbfinish_part`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbgive_loan`
--
ALTER TABLE `tbgive_loan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbgive_loan_history`
--
ALTER TABLE `tbgive_loan_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbincome_sources`
--
ALTER TABLE `tbincome_sources`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbincome_source_category`
--
ALTER TABLE `tbincome_source_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblc_info`
--
ALTER TABLE `tblc_info`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tborders`
--
ALTER TABLE `tborders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tborders_details`
--
ALTER TABLE `tborders_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbreceived_loan`
--
ALTER TABLE `tbreceived_loan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbreceived_loan_history`
--
ALTER TABLE `tbreceived_loan_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbstock_fabric`
--
ALTER TABLE `tbstock_fabric`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12">
    @if(Session::has('message'))
          <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
          @endif
          @if(Session::has('failedMessage'))
          <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
          @endif
  </div>

  <div class="col-md-6 col-sm-12 col-xs-12">
     <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-list"></i> <b>L/C Details</b></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        @php $id = $lc_details->id; @endphp
        <ul class="to_do">
          <li><h4> <b>L/C Number:</b> <b style='color:blue;'>{{$lc_details->lcNumber}}</b></h4></li>
          <li>LC Date: <b>{{$lc_details->lcDate}}</b></li>
          <li>Challan No: <b>{{$lc_details->challanNo}}</b></li>
          <li>Client Name: <b>{{$lc_details->customerName}}</b></li>
          <li>Client Bank Name: <b>{{$lc_details->customerBankName}}</b></li>
          <li>Sight Days: <b>{{$lc_details->sightDays}}</b></li>
          <li>Contract No: <b>{{$lc_details->contractNo}}</b></li>
          <li>Contract Date: <b>{{$lc_details->contractDate}}</b></li>
          <li>Company Bank Name: <b>{{$lc_details->bankName}}({{$lc_details->bankAccountNo}})</b></li>
        </ul>
        <hr>

        <!-- <a href="{{route('order.order_details_pdf',$id)}}"  class="btn btn-default btn-md  animated zoomIn"><i class="fa fa-download"></i> Save as PDF </a> -->

      </div>
    </div>
  </div>
  <div class="col-md-4 col-sm-8 col-xs-12">
     <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-th"></i> <b>Actions</b></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <a href="{{route('commercial.negotiation_application_pdf',base64_encode($id))}}"  class="btn btn-default btn-md  animated zoomIn"><i class="fa fa-download"></i> Negotiation Application </a> <br >
        <a href="{{route('commercial.bill_of_exchange_pdf',base64_encode($id))}}"  class="btn btn-default btn-md  animated zoomIn"><i class="fa fa-download"></i> Bill Of Exchange </a>
        
      </div>
    </div>
  </div>


</div>

<div class="row">
  <div class="col-md-6 col-sm-12 col-xs-12">
   <div class="x_panel">
    <div class="x_title">
      <h2><i class="fa fa-th"></i> <b>Cash In History</b></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table id="datatable-responsive_" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>SN</th>
            <th>Date</th>
            <th>Amount</th>
            <th>Payment Method</th>
            <th>Reference</th>
          </tr>
        </thead>
          <tbody>
            
          </tbody>        
      </table>

    </div>
  </div>

  </div>

</div>

@endsection
@section('extra_scripts')
<script>
  $(document).ready(function() {

    $('#datatable-responsive').DataTable({
      "pageLength": 50
    });

  });
  setTimeout(function() {
        $('#alert_message').fadeOut('fast');
    }, 5000);
</script>

@endsection
@extends('layouts.master')
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">

    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-plus-square"></i> Generate New L/C Information </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        {!! Form::open(['method'=>'POST','action'=>'CommercialController@store']) !!}
      
        <div class="form-group">
          <label for="lc">L/C Number</label>
          <input type="text" autofocus name="lcNumber" class="form-control form-white" id="lc" placeholder="L/C Number " required >
        </div>

        <br />

        <div class="form-group">
          <label for="lcDate">L/C Issue Date</label>
          <input name="lcDate" id="datepicker1" value="{{date('Y-m-d')}}" class="form-control form-white" id="lcDate" required />
        </div>

        <br />

        <div class="form-group">
          <label for="challanNo">Challan No</label>
          <input type="text" name="challanNo" class="form-control form-white" id="challanNo" placeholder="Challan No" >
        </div>

        <br />
        
        <div class="form-group">
          <label for="companyId">Company</label>
          <select name="companyId" id="companyId" required class="form-control" >
            <option selected value="">Select a company</option>
            @foreach($companies as $company)
            <option value="{{$company->id}}">{{$company->companyName}}</option>
            @endforeach
          </select>
        </div>

        <br />

        <div class="form-group">
          <label for="companyBankId">Company Bank Info</label>
          <select name="companyBankId" id="companyBankId" required class="form-control" >
            <option selected value="">Select a Bank</option>
            @foreach($bank_info as $info)
            <option value="{{$info->id}}">{{$info->bankName}} ({{$info->bankAccountNo}})</option>
            @endforeach
          </select>
        </div>

        <br />

        <div class="form-group">
          <label for="customerId">Customer/Buyer</label>
          <select name="customerId" id="customerId" required class="form-control" >
            <option selected value="">Select a customer</option>
            @foreach($customers as $customer)
            <option value="{{$customer->id}}">{{$customer->customerName}}</option>
            @endforeach
          </select>
        </div>

        <br />

        <div class="form-group">
          <label for="customerBankName">Customer/Buyer Bank Name</label>
          <input name="customerBankName"  type="text" class="form-control form-white" id="customerBankName" placeholder="Customer/Buyer Bank Name" />
        </div>

        <br />

        <div class="form-group">
          <label for="customerBankInfo">Customer/Buyer Bank Info</label>
          <input name="customerBankInfo"  type="text" class="form-control form-white" id="customerBankInfo" placeholder="Customer/Buyer Bank Info" />
        </div>

        <br />

        <div class="form-group">
          <label for="sightDays">Sight days</label>
          <input name="sightDays" placeholder="Sight days" type="number" step="any" class="form-control form-white" id="sightDays" />
        </div>

        <br />

        <div class="form-group">
          <label for="contractNo">Contract No</label>
          <input name="contractNo" placeholder="Contract No" type="text" class="form-control form-white" id="contractNo" />
        </div>

        <br />

        <div class="form-group">
          <label for="contractDate">Contract Date</label>
          <input name="contractDate" id="datepicker2" value="{{date('Y-m-d')}}" class="form-control form-white" id="contractDate" />
        </div>

        <br />
        
        <div class="form-group">
          <label for="valueReceived">Value Received </label>
          <textarea name="valueReceived" class="form-control form-white" id="valueReceived" placeholder="Value Received " ></textarea>
        </div>
       
        <br />

        <div class="form-group">
          <label for="IRCNo">IRC No</label>
          <input name="IRCNo" placeholder="IRC No." type="text" class="form-control form-white" id="IRCNo" />
        </div>
        
        <br />

        <div class="form-group">
          <label for="TINNo">TIN No</label>
          <input name="TINNo" placeholder="TIN No." type="text" class="form-control form-white" id="TINNo" />
        </div>
        
        <br />

        <div class="form-group">
          <label for="VatRegNo">VAT Reg No</label>
          <input name="VatRegNo" placeholder="VAT Reg No" type="text" class="form-control form-white" id="VatRegNo" />
        </div>
      
        <br />

        <hr>

        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Generate and Save Information</button>
          <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Reset</button>
        </div>

        {{ Form::close() }}


      </div>
    </div>
  </div>

</div>
@endsection
@section('extra_scripts')

<script>
  $( function() {
    $( "#datepicker1" ).datepicker({
       format:'YYYY-MM-DD',
    });
    $( "#datepicker2" ).datepicker({
       format:'YYYY-MM-DD',
    });
  });
  setTimeout(function() {
    $('#alert_message').fadeOut('fast');
  }, 5000);
</script>
@endsection
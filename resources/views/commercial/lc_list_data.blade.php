@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
     <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-list"></i> L/C LIST FROM (<span style="color:blue;">{{date("d-M-Y", strtotime($request->start_date))}}</span>) TO (<span style="color:blue;">{{date("d-M-Y", strtotime($request->end_date))}}</span>) </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>L/C Number</th>
              <th>L/C Date</th>
              <th>Challan No</th>
              <th>Customer/Buyer</th>
              <th>Contract No</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
              @foreach($lc_list as $lc)
              <tr>
                <td>{{$lc->lcNumber}}</td>
                <td>{{date("d.m.Y", strtotime($lc->lcDate))}}</td>
                <td>{{$lc->challanNo}}</td>
                <td>{{$lc->customerName}}</td>
                <td>{{$lc->contractNo}}</td>
                <td>
                  <a href="{{route('lc.show',base64_encode($lc->id))}}"   title="View L/C Details" class="btn btn-primary btn-sm  animated fadeIn"><i class="fa fa-eye"></i></a>
                </td>
              </tr>
              @endforeach
          </tbody>
        </table>

      </div>
    </div>

  </div>

</div>


@endsection
@section('extra_scripts')
<script>

  $(document).ready(function() {
    $('#datatable-responsive').DataTable({
      "pageLength": 50
    });

  });
  </script>
  @endsection

@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    @if(Session::has('message'))
    <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
    @endif
    @if(Session::has('failedMessage'))
    <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
    @endif
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-list"></i> Stock fabric list </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li> 
            {!! Form::open(['method'=>'POST','route'=>'stock_fabric_data']) !!}
                  <input hidden name="start_date" value="{{$request->start_date}}"  />
                  <input hidden name="end_date" value="{{$request->end_date}}" />
                  <button type="submit" name='submit_type' value="pdf" class="btn btn-sm"><i class="fa fa-download"></i> Download as PDF</button>
            {{ Form::close() }}
          </li>
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Ref </th>
              <th>Date</th>
              <th>Item</th>
              <th>Color</th>
              <th>PI</th>
              <th>Q'TY/yds</th>
              <th>Details</th>
              <th>Remarks</th>
              <th>Created By</th>
            </tr>
          </thead>
          @foreach($stock_fabrics as $st)
              <tr>
                <td>{{$st->sfRef}}</td>
                <td>{{$st->sfDate}}</td>
                <td>{{$st->sfItem}}</td>
                <td>{{$st->sfColor}}</td>
                <td>{{$st->sfPi}}</td>
                <td><b>{{$st->sfQty}}</b></td>
                <td>{{$st->sfDetails}}</td>
                <td>{{$st->sfRemarks}}</td>
                <td>{{$st->created_by}}</td>
              </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>

</div>
@endsection
@section('extra_scripts')
  <script>
      $(document).ready(function() {

        $('#datatable-responsive').DataTable({
        	 "pageLength": 50
        });

      });
    </script>
@endsection
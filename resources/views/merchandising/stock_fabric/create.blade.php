@extends('layouts.master')
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">

    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-plus-square"></i> Add New Stock </h2>

        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        @if(Session::has('message'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
        @endif
        @if(Session::has('failedMessage'))
            <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
        @endif

        {!! Form::open(['method'=>'POST','route'=>'stock_fabric_store']) !!}
      
        <div class="form-group">
          <label for="Ref">Ref.</label>
          <input type="text" autofocus name="sfRef" class="form-control form-white" id="Ref" placeholder="Ref. " >

        </div>

        <br />
      
        <div class="form-group">
          <label for="sfDate">Date</label>
          <input name="sfDate" id="datepicker1" value="{{date('Y-m-d')}}" class="form-control form-white" required />
        </div>

        <br />

        <div class="form-group">
          <label for="sfItem">Item</label>
          <input type="text" placeholder="Item" name="sfItem" class="form-control form-white" id="sfItem"  >

        </div>

        <br />
      
        <div class="form-group">
          <label for="Color">Color</label>
          <input type="text" name="sfColor" class="form-control form-white" id="Color" placeholder="PI Number " >
        </div>

        <br />
      
        <div class="form-group">
          <label for="pi">PI</label>
          <input type="text" name="sfPi" class="form-control form-white" id="pi" placeholder="PI Number " >
        </div>

        <br />

        <div class="form-group">
          <label for="sfQty">Qty</label>
          <input type="number" step="any" min="0" value="0" name="sfQty" class="form-control form-white" id="sfQty" placeholder="Qty " >
        </div>

        <br />

        <div class="form-group">
          <label for="sfDetails">Details</label>
          <textarea name="sfDetails" class="form-control form-white" id="sfDetails" placeholder="Description" ></textarea>
        </div>

        <br />

        <div class="form-group">
          <label for="sfRemarks">Remarks</label>
          <textarea name="sfRemarks" class="form-control form-white" id="sfRemarks" placeholder="Remarks" ></textarea>
        </div>

        <hr>

        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
          <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Reset</button>
        </div>

        {{ Form::close() }}


      </div>
    </div>
  </div>

</div>
@endsection
@section('extra_scripts')

<script>
  $( function() {
    $( "#datepicker1" ).datepicker({
       format:'YYYY-MM-DD',
    });
  });
  setTimeout(function() {
    $('#alert_message').fadeOut('fast');
  }, 5000);
</script>
@endsection
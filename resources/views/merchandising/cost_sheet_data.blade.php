@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    @if(Session::has('message'))
    <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
    @endif
    @if(Session::has('failedMessage'))
    <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
    @endif
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-money"></i> Cost Sheet </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>PI</th>
              <th>Customer</th>
              <th>Item</th>
              <th>Color</th>
              <th>Booking QTY</th>
              <th>GREIGE QTY</th>
              <th>Unit Price</th>
              <th>TTL Greige cost</th>
              <th>Knitting cost</th>
              <th>Dyeing cost</th>
              <th>Print cost</th>
              <th>TTL cost</th>
              <th>Paid cost</th>
              <th>Outstanding cost</th>
              <th>Sources</th>
              <th>Remarks</th>
              <th>Bill code</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>

</div>
@endsection
@section('extra_scripts')
  <script>
      $(document).ready(function() {

        $('#datatable-responsive').DataTable({
        	 "pageLength": 50
        });

      });
    </script>
@endsection
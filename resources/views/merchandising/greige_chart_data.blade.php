@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
     <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-list"></i> GREIGE CHART FROM (<span style="color:blue;">{{date("d-M-Y", strtotime($request->start_date))}}</span>) TO (<span style="color:blue;">{{date("d-M-Y", strtotime($request->end_date))}}</span>) </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>DATE</th>
              <th>ITEM</th>
              <th>GREIGE FACTORY</th>
              <th>DYEING FACTORY</th>
              <th>QUANTITY</th>
              <th>BILL CODE</th>
            </tr>
          </thead>
          <tbody>
              @foreach($greige_charts as $chart)
              <tr>
                <td>{{date("d.m.Y", strtotime($chart->bDate))}}</td>
                <td>{{$chart->item}}</td>
                <td>{{$chart->bcFrom}}</td>
                <td>{{$chart->bcTo}}</td>
                <td>{{$chart->bcQuantity}}</td>
                <td>{{$chart->billcode}}</td>
              </tr>
              @endforeach
              
          </tbody>
        </table>

      </div>
    </div>

  </div>

</div>


@endsection
@section('extra_scripts')
<script>

  $(document).ready(function() {
    $('#datatable-responsive').DataTable({
      "pageLength": 50
    });

  });
  </script>
  @endsection

@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    @if(Session::has('message'))
    <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
    @endif
    @if(Session::has('failedMessage'))
    <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
    @endif
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-list"></i> Production Sheet </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>PI</th>
              <th>Customer</th>
              <th>Style/Order</th>
              <th>Details</th>
              <th>Colour</th>
              <th>Order Q'ty</th>
              <th>Dlvy Q'ty</th>
              <th>Blnce Q'ty</th>
              <th>Booking Date</th>
              <th>Estimate Date</th>
              <th>Delivered Date</th>
              <th>Sources</th>
              <th>Status</th>
            </tr>
          </thead>
          @foreach($production_sheets as $ps)
              <tr>
                <td>{{$ps->pi}}</td>
                <td>{{$ps->customerName}}</td>
                <td>{{$ps->style}}</td>
                <td>{{$ps->item}}</td>
                <td>{{$ps->color}}</td>
                <td><b>{{$ps->orderQuantity}}</b></td>
                <td>
                   @if(($ps->orderQuantity-($ps->twillsum+$ps->tcsum))==0)
                    <span style="color:green;"><b>{{$ps->twillsum+$ps->tcsum}}</b></span>
                  @elseif(($ps->orderQuantity-($ps->twillsum+$ps->tcsum))<$ps->orderQuantity)
                    <span style="color:red;"><b>{{$ps->twillsum+$ps->tcsum}}</b></span>
                  @else
                    <span><b>{{$ps->twillsum+$ps->tcsum}}</b></span>
                  @endif
                </td>
                <td>
                  @if(($ps->orderQuantity-($ps->twillsum+$ps->tcsum))==0)
                    <span style="color:green;"><b>{{($ps->orderQuantity-($ps->twillsum+$ps->tcsum))}}</b></span>
                  @elseif(($ps->orderQuantity-($ps->twillsum+$ps->tcsum))<$ps->orderQuantity)
                    <span style="color:red;"><b>{{($ps->orderQuantity-($ps->twillsum+$ps->tcsum))}}</b></span>
                  @else
                    <span><b>{{($ps->orderQuantity-($ps->twillsum+$ps->tcsum))}}</b></span>
                  @endif
                </td>
                <td><?php echo date("d-M-Y", strtotime($ps->orderDate)); ?></td>
                <td>
                  @if($ps->shippingDate!=NULL)
                    <?php echo date("d-M-Y", strtotime($ps->shippingDate)); ?>
                  @else

                  @endif
                  </td>
                <td>
                   @if($ps->shippingDate!=NULL)
                    <?php echo date("d-M-Y", strtotime($ps->deliveryDate)); ?>
                  @else
                  
                  @endif
                    
                  </td>
                <td>{{$ps->sources}}</td>
                <td>
                  <?php
                      if(($ps->status)==1){
                        echo "<span style='color:green;'>Completed</span>";
                      }elseif(($ps->status)==0){
                        echo "<span style='color:blue;'>Pending</span>";
                      }elseif(($ps->status)==2){
                        echo "<span style='color:red;'>Rejected</span>";
                      }else{
                        echo "Undefined";
                      }
                    ?>
                
              </td>
              </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>

</div>
@endsection
@section('extra_scripts')
  <script>
      $(document).ready(function() {

        $('#datatable-responsive').DataTable({
        	 "pageLength": 50
        });

      });
    </script>
@endsection
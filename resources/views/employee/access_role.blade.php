@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-users"></i> Access Role List </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th width="5%">Serial</th>
              <th>Fullname</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Department</th>
              <th>Access Role</th>
              <th>Action</th>
            </tr>
          </thead>


          <tbody>
          @php $i=0; @endphp
          @foreach($employees as $employee)
            <tr>
              <td>{{++$i}}</td>
              <td>{{$employee->name}}</td>
              <td>{{$employee->email}}</td>
              <td>{{$employee->phone}}</td>
              <td>{{$employee->departmentName}}</td>
              <td>
                <?php
                if($employee->userType=='1')
                  echo "<span class='btn btn-sm btn-flat btn-danger' style='font-size:12px;'>Super Admin</span>";
                if($employee->userType=='2')
                  echo "<span class='btn btn-sm btn-flat btn-danger' style='font-size:12px;'>Admin</span>";
                if($employee->userType=='3')
                  echo "<span class='btn btn-sm btn-flat btn-primary' style='font-size:12px;'>Accountant</span>";
                if($employee->userType=='4')
                  echo "<span class='btn btn-sm btn-flat btn-success' style='font-size:12px;'>Officer</span>";
                if($employee->userType=='5')
                  echo "<span class='btn btn-sm btn-flat btn-dark' style='font-size:12px;'>Commercial Manager</span>";
                if($employee->userType=='0')
                  echo "<span class='btn btn-sm btn-flat btn-default' style='font-size:12px;'>Employee</span>";
                
              ?>
              </td>
              <td>
                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target=".view{{$employee->id}}"><i class="fa fa-key"></i></button>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".edit{{$employee->id}}"><i class="fa fa-user"></i></button>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
@endsection
@section('extra_scripts')
  <script>
      $(document).ready(function() {

        $('#datatable-responsive').DataTable({
        	 "pageLength": 50
        });

      });
    </script>
@endsection
@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-users"></i> Employee List </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Serial</th>
              <th>Fullname</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Designation</th>
              <th>Department</th>
              <th>Action</th>
            </tr>
          </thead>


          <tbody>
          @php $i=0; @endphp
          @foreach($employees as $employee)
            <tr>
              <td>{{++$i}}</td>
              <td>{{$employee->name}}</td>
              <td>{{$employee->email}}</td>
              <td>{{$employee->phone}}</td>
              <td>{{$employee->designationName}}</td>
              <td>{{$employee->departmentName}}</td>
              <td>
                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target=".view{{$employee->id}}"><i class="fa fa-eye"></i></button>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".edit{{$employee->id}}"><i class="fa fa-edit"></i></button>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
@endsection
@section('extra_scripts')
  <script>
      $(document).ready(function() {

        $('#datatable-responsive').DataTable({
        	 "pageLength": 50
        });

      });
    </script>
@endsection
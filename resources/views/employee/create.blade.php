@extends('layouts.master')
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">

    @if(Session::has('message'))
    <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
    @endif
    @if(Session::has('failedMessage'))
    <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
    @endif

    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-user"></i> Add New Employee </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        {!! Form::open(['method'=>'POST','action'=>'EmployeeController@store', 'files'=>true]) !!}
        <div class="form-group">
          <label for="empName">Employee Name</label>
          <input type="text" name="name" class="form-control form-white" id="empName" placeholder="Employee Name" required >
        </div>

        <br />

        <div class="form-group">
          <label for="employeeNumber">Employee ID</label>
          <input name="employeeNumber" class="form-control form-white" id="employeeNumber" placeholder="Employee ID" />
        </div>

        <br />

        <div class="form-group">
          <label for="userType">Access Role</label>
          <select name="userType" id="userType" required class="form-control" >
            <option selected value="">Select Type</option>
            <option value="0">Employee</option>
            
            @if(checkPermission(['superadmin']))
            <option value="2">Admin</option>
            @endif

            <option value="3">Accountant</option>
            <option value="4">Merchandise Manager</option>
            <option value="5">Commercial Manager</option>
          </select>
        </div>

        <br />

        <div class="form-group">
          <label for="employeeEmail">Employee Email</label>
          <input name="email" type="email" class="form-control form-white" id="employeeEmail" placeholder="Employee Email" required />
        </div>

        <br />

        <div class="form-group">
          <label for="employeePassword">Employee Password</label>
          <input name="password" value="123456" class="form-control form-white" id="employeePassword" placeholder="Employee Password" />
        </div>

        <br />

        <div class="form-group">
          <label for="employeePhone">Employee Phone</label>
          <input name="phone" type="text" class="form-control form-white" id="employeePhone" placeholder="Employee Phone" />
        </div>

        <br />

        <div class="form-group">
          <label for="gender">Employee Gender</label>
          <select name="gender" id="gender" required class="form-control" >
            <option selected value="">Select Gender</option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
            <option value="Others">Others</option>
          </select>
        </div>

        <br />

        <div class="form-group">
          <label for="departmentId">Employee Department</label>
          <select name="departmentId" id="departmentId" required class="form-control" >
            <option selected value="">Select Department</option>
            @foreach($departments as $department)
            <option value="{{$department->id}}">{{$department->departmentName}}</option>
            @endforeach
          </select>
        </div>

        <br />

        <div class="form-group">
          <label for="designationId">Employee Designation</label>
          <select name="designationId" id="designationId" required class="form-control" >
            <option selected value="">Select Designation</option>
            @foreach($designations as $designation)
            <option value="{{$designation->id}}">{{$designation->designationName}}</option>
            @endforeach
          </select>
        </div>

        <br />

        <div class="form-group">
          <label for="joiningDate">Joining Date</label>
          <input name="joiningDate" type="date" class="form-control form-white" id="joiningDate" />
        </div>

        <br />

        <div class="form-group">
          <label for="salary">Salary</label>
          <input name="salary" type="number" step="any" class="form-control form-white" id="salary" placeholder="Salary Amount" />
        </div>

        <br />

        <div class="form-group">
          <label for="photo">Employee Photo</label>
          <input name="photo" type="file" class="form-control form-white" id="photo"  />
        </div>

        <br />

        <div class="form-group">
          <label for="address">Employee Address</label>
          <textarea name="address" class="form-control form-white" id="address" placeholder="Employee Address " ></textarea>
        </div>

        <hr>

        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save Information</button>
          <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Reset</button>
        </div>

        {{ Form::close() }}


      </div>
    </div>
  </div>

</div>
@endsection
@section('extra_scripts')
<script>
  $(document).ready(function() {

    $('#datatable-responsive').DataTable({
      "pageLength": 50
    });

  });
  
  setTimeout(function() {
    $('#alert_message').fadeOut('fast');
  }, 5000);
</script>
@endsection
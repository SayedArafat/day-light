@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    
    @if(Session::has('message'))
      <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
    @endif
    @if(Session::has('failedMessage'))
       <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
    @endif

    <div class="x_panel">
      <div class="x_title">
        <h2>Department List </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-plus-square "></i> Add New </button>
          </li>
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Serial</th>
              <th>Department Name</th>
              <th>Department Description</th>
              <th>Created At</th>
              <th>Created By</th>
              <th>Action</th>
            </tr>
          </thead>


          <tbody>
          @php $i=0; @endphp
          @foreach($departments as $department)
            <tr>
              <td>{{++$i}}</td>
              <td>{{$department->departmentName}}</td>
              <td>{{$department->departmentDescription}}</td>
              <td>{{$department->created_at}}</td>
              <td>{{$department->created_by}}</td>
              <td>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".edit{{$department->id}}"><i class="fa fa-edit"></i></button>
                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target=".delete{{$department->id}}"><i class="fa fa-trash"></i></button>
              </td>
            </tr>



          <div class="modal fade edit{{$department->id}}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">

                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel2"><b>Edit Department</b></h4>
                </div>
                <div class="modal-body">
                {!! Form::open(['method'=>'PATCH', 'action'=>['DepartmentController@update', $department->id]]) !!}
                  <div class="form-group">
                    <label for="departmentName">Department Name</label>
                    <input type="text" value="{{$department->departmentName}}" name="departmentName" class="form-control form-white" id="departmentName" placeholder="Department Name" required >
                  </div>

                  <br />

                  <div class="form-group">
                    <label for="departmentDescription">Department Description</label>
                    <textarea  name="departmentDescription" class="form-control form-white" id="departmentDescription" placeholder="Department Description" >{{$department->departmentDescription}}</textarea>
                  </div>
                    
                  <hr>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                  </div>

                {{ Form::close() }}
                 
                </div>
                <div class="modal-footer">
                </div>

              </div>
            </div>
          </div>


            <div class="modal fade delete{{$department->id}}" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2"><b>Confirm Delete</b></h4>
                  </div>
                  <div class="modal-body">
                    {!! Form::open(['method'=>'DELETE', 'action'=>['DepartmentController@destroy', $department->id]]) !!}
                      <button type="submit" class="btn btn-danger"><i class="fa fa-check-circle"></i> Confirm</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    {!! Form::close() !!}
                    
                  </div>
                  <div class="modal-footer">
                  </div>

                </div>
              </div>
            </div>


            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2"><b>New Department</b></h4>
      </div>
      <div class="modal-body">
      {!! Form::open(array('route' => 'department.store', 'method' => 'POST')) !!}
        <div class="form-group">
          <label for="departmentName">Department Name</label>
          <input type="text" name="departmentName" class="form-control form-white" id="departmentName" placeholder="Department Name" required >
        </div>

        <br />

        <div class="form-group">
          <label for="departmentDescription">Department Description</label>
          <textarea  name="departmentDescription" class="form-control form-white" id="departmentDescription" placeholder="Department Description" ></textarea>
        </div>
          
        <hr>

        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
        </div>

      {{ Form::close() }}
       
      </div>
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>

@endsection
@section('extra_scripts')
  <script>
      $(document).ready(function() {

        $('#datatable-responsive').DataTable({
        	 "pageLength": 50
        });

      });
      setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
@endsection
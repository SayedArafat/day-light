@extends('layouts.master')
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">

    @if(Session::has('message'))
    <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
    @endif
    @if(Session::has('failedMessage'))
    <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
    @endif

    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-money"></i> Add New Expense </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        {!! Form::open(['method'=>'POST','action'=>'ExpensesController@store', 'files'=>true]) !!}
      
        <div class="form-group">
          <label for="categoryId">Expense Category</label>
          <select name="categoryId" id="categoryId" required class="form-control" >
            <option selected value="">Select Category</option>
            @foreach($expenseCategories as $category)
            <option value="{{$category->id}}">{{$category->categoryName}}</option>
            @endforeach
          </select>
        </div>

        <br />

        <div class="form-group">
          <label for="title">Expense Title</label>
          <input type="text" name="title" class="form-control form-white" id="title" placeholder="Expense Title " required >
        </div>

        <br />

        <div class="form-group">
          <label for="amount">Expense Amount</label>
          <input name="amount" class="form-control form-white" id="amount" type="number" step="any" placeholder="Expense Amount" required />
        </div>

        <br />

        <div class="form-group">
          <label for="expenseAttachment">Expense Attachment</label>
          <input name="expenseAttachment" type="file" class="form-control form-white" id="expenseAttachment"  />
        </div>

        <br />

        <div class="form-group">
          <label for="description">Expense Description</label>
          <textarea name="description" class="form-control form-white" id="description" placeholder="Expense Description" ></textarea>
        </div>

        <br />

        <div class="form-group">
          <label for="expenseDate">Expense Date</label>
          <input name="expenseDate"  type="date" class="form-control form-white" id="expenseDate" required />
        </div>

        <br />

        <div class="form-group">
          <label for="reference">Reference</label>
          <input type="text" name="reference" class="form-control form-white" id="reference" placeholder="Expense Reference " >
        </div>
        
        <hr>

        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save Expense Information</button>
          <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Reset</button>
        </div>

        {{ Form::close() }}


      </div>
    </div>
  </div>

</div>
@endsection
@section('extra_scripts')
<script>
  setTimeout(function() {
    $('#alert_message').fadeOut('fast');
  }, 5000);
</script>
@endsection
@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    @if(Session::has('message'))
    <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
    @endif
    @if(Session::has('failedMessage'))
    <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
    @endif
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-users"></i> Expense List </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th width="5%">Serial</th>
              <th>Expense Title</th>
              <th>Expense Category</th>
              <th>Amount</th>
              <th>Date</th>
              <th>Created By</th>
              <th width="12%">Action</th>
            </tr>
          </thead>


          <tbody>
          @php $i=0; @endphp
          @foreach($expenses as $expense)
            <tr>
              <td>{{++$i}}</td>
              <td>{{$expense->title}}</td>
              <td>{{$expense->categoryName}}</td>
              <td>{{$expense->amount}}</td>
              <td>{{$expense->expenseDate}}</td>
              <td>{{$expense->created_by}}</td>
              <td>
                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target=".view{{$expense->id}}"><i class="fa fa-eye"></i></button>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".edit{{$expense->id}}"><i class="fa fa-edit"></i></button>
                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target=".delete{{$expense->id}}"><i class="fa fa-trash"></i></button>
              </td>
            </tr>



            <div class="modal fade view{{$expense->id}}" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2"><b>Edit Expense Information</b></h4>
                  </div>
                  <div class="modal-body">

                      <div class="form-group">
                        <label for="categoryId">Expense Category</label>
                        <select name="categoryId" id="categoryId" readonly required class="form-control" >
                          <option selected value="">Select Category</option>
                          @foreach($expenseCategories as $category)
                          <option <?php if($expense->categoryId==$category->id){ echo "selected"; } ?> value="{{$category->id}}">{{$category->categoryName}}</option>
                          @endforeach
                        </select>
                      </div>

                      <br />

                      <div class="form-group">
                        <label for="title">Expense Title</label>
                        <input type="text" name="title" readonly value="{{$expense->title}}" class="form-control form-white" id="title" placeholder="Expense Title " required >
                      </div>

                      <br />

                      <div class="form-group">
                        <label for="amount">Expense Amount</label>
                        <input name="amount" readonly value="{{$expense->amount}}" class="form-control form-white" id="amount" type="number" step="any" placeholder="Expense Amount" required />
                      </div>

                      <br />

                      <div class="form-group">
                        <label for="expenseAttachment">Expense Attachment</label>
                        <br />
                        <?php if(!empty($expense->expenseAttachment)){ ?>
                          
                          <a class="btn btn-primary" target="_NEW" href="expense_attachment/{{$expense->expenseAttachment}}"><i class="fa fa-download"></i> Download Attachment</a>
                         <?php }else{
                            echo "No Attachment Found.";
                          } ?>
                      </div>

                      <br />

                      <div class="form-group">
                        <label for="description">Expense Description</label>
                        <textarea readonly rows="8" name="description" class="form-control form-white" id="description" placeholder="Expense Description" >{{$expense->description}}</textarea>
                      </div>

                      <br />

                      <div class="form-group">
                        <label for="expenseDate">Expense Date</label>
                        <input readonly name="expenseDate" value="{{$expense->expenseDate}}"  class="form-control form-white" id="expenseDate" required />
                      </div>

                      <br />

                      <div class="form-group">
                        <label for="reference">Reference</label>
                        <input readonly type="text" value="{{$expense->reference}}" name="reference" class="form-control form-white" id="reference" placeholder="Expense Reference " >
                      </div>
                      
                    
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                  </div>

                </div>
              </div>
            </div>



            <div class="modal fade edit{{$expense->id}}" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2"><b>Edit Expense Information</b></h4>
                  </div>
                  <div class="modal-body">
                    {!! Form::open(['method'=>'PATCH', 'action'=>['ExpensesController@update', $expense->id],'files'=>true]) !!}

                      <div class="form-group">
                        <label for="categoryId">Expense Category</label>
                        <select name="categoryId" id="categoryId" required class="form-control" >
                          <option selected value="">Select Category</option>
                          @foreach($expenseCategories as $category)
                          <option <?php if($expense->categoryId==$category->id){ echo "selected"; } ?> value="{{$category->id}}">{{$category->categoryName}}</option>
                          @endforeach
                        </select>
                      </div>

                      <br />

                      <div class="form-group">
                        <label for="title">Expense Title</label>
                        <input type="text" name="title" value="{{$expense->title}}" class="form-control form-white" id="title" placeholder="Expense Title " required >
                      </div>

                      <br />

                      <div class="form-group">
                        <label for="amount">Expense Amount</label>
                        <input name="amount" value="{{$expense->amount}}" class="form-control form-white" id="amount" type="number" step="any" placeholder="Expense Amount" required />
                      </div>

                      <br />

                      <div class="form-group">
                        <label for="expenseAttachment">Expense Attachment</label>
                        <br />
                        <?php if(!empty($expense->expenseAttachment)){ ?>
                          
                          <a class="btn btn-primary" target="_NEW" href="expense_attachment/{{$expense->expenseAttachment}}"><i class="fa fa-download"></i> Download Attachment</a>
                         <?php }else{
                            echo "No Attachment Found.";
                          } ?>
                        <br />
                        <br />
                        <input name="expenseAttachment" type="file" class="form-control form-white" id="expenseAttachment"  />
                        <input name="expenseAttachmentPre" hidden value="{{$expense->expenseAttachment}}"  />
                      </div>

                      <br />

                      <div class="form-group">
                        <label for="description">Expense Description</label>
                        <textarea name="description" class="form-control form-white" id="description" placeholder="Expense Description" >{{$expense->description}}</textarea>
                      </div>

                      <br />

                      <div class="form-group">
                        <label for="expenseDate">Expense Date</label>
                        <input name="expenseDate" value="{{$expense->expenseDate}}"  class="form-control form-white" id="expenseDate" required />
                      </div>

                      <br />

                      <div class="form-group">
                        <label for="reference">Reference</label>
                        <input type="text" value="{{$expense->reference}}" name="reference" class="form-control form-white" id="reference" placeholder="Expense Reference " >
                      </div>
                      
                    <hr>

                    <div class="form-group">
                      <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    </div>

                    {{ Form::close() }}
                    
                  </div>
                  <div class="modal-footer">
                  </div>

                </div>
              </div>
            </div>


            <div class="modal fade delete{{$expense->id}}" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2"><b>Confirm Delete</b></h4>
                  </div>
                  <div class="modal-body">
                    {!! Form::open(['method'=>'DELETE', 'action'=>['ExpensesController@destroy', $expense->id]]) !!}
                    <button type="submit" class="btn btn-danger"><i class="fa fa-check-circle"></i> Confirm</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    {!! Form::close() !!}
                    
                  </div>
                  <div class="modal-footer">
                  </div>

                </div>
              </div>
            </div>

            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
@endsection
@section('extra_scripts')
  <script>
      $(document).ready(function() {

        $('#datatable-responsive').DataTable({
        	 "pageLength": 50
        });

      });
    </script>
@endsection
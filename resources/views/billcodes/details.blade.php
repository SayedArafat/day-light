@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row animated fadeIn">
  <div class="col-md-12 col-sm-12 col-xs-12">
     <div class="x_panel">
      <div class="x_title">
        
        <ul class="to_do">
          <li><h3 style='color:blue;'> <b>Bill Code: {{$billcodeDetails->billcode}}</b></h3></li>
          <li>Date: <b>{{$billcodeDetails->bDate}}</b></li>
          <li>Remarks: <b>{{$billcodeDetails->remarks}}</b></li>
          <br />
          <center>
            <button type="button" class="btn btn-primary animated fadeIn" data-toggle="modal" data-target=".add_new_item_to_billcode "><i class="fa fa-plus-square "></i> Add New </button>
            <a href="{{route('billcode.download_pdf',$billcodeDetails->id)}}"  class="btn btn-danger btn-md animated fadeIn "><i class="fa fa-download"></i> Save as PDF </a>
          </center>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th width="5%">Serial</th>
              <th>Description Of The Goods</th>
              <th>PI</th>
              <th>Colour</th>
              <th>Booking QTY</th>
              <th>QTY</th>
              <th>Unit Price (TK)</th>
              <th>Total Amount(TK)</th>
              <th>From</th>
              <th>To</th>
              <th>Remarks</th>
              <th>#</th>
            </tr>
          </thead>
          <tbody>
            @php $i=0;$qty=0;$total=0; @endphp
              @foreach($billcodeitems as $bci)
              @php $qty+=($bci->bcQuantity);$total+=($bci->bcQuantity)*($bci->bcUnitPrice); @endphp

              <tr>
                <td>{{++$i}}</td>
                <td>{{$bci->details}}</td>
                <td>{{$bci->pi}}</td>
                <td>{{$bci->color}}</td>
                <td>{{$bci->bookingQuantity}}</td>
                <td>{{$bci->bcQuantity}}</td>
                <td>@money($bci->bcUnitPrice)</td>
                <td>@money(($bci->bcQuantity)*($bci->bcUnitPrice))</td>
                <td>{{($bci->bcFrom)}}</td>
                <td>{{($bci->bcTo)}}</td>
                <td>{{($bci->bcRemarks)}}</td>
                <td>
                  <!-- <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target=".delete{{$bci->id}}"><i class="fa fa-trash"></i></button> -->
                </td>
              </tr>


            <div class="modal fade delete{{$bci->id}}" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2"><b>Confirm Delete</b></h4>
                  </div>
                  <div class="modal-body">
                    {!! Form::open(['method'=>'DELETE', 'action'=>['BillcodeItemController@destroy', $bci->id]]) !!}
                      <button type="submit" class="btn btn-danger"><i class="fa fa-check-circle"></i> Confirm</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    {!! Form::close() !!}
                  </div>
                  <div class="modal-footer">
                  </div>

                </div>
              </div>
            </div>


              @endforeach
              
          </tbody>
          <tfoot>
            <tr>
              <th colspan="5" style="text-align:right;">BUY GREIGE</th>
              <th>{{$qty}}</th>
              <th></th>
              <th>@money($total)</th>
              <th colspan="4"></th>
            </tr>
          </tfoot>  
        </table>

      </div>
    </div>

  </div>

</div>

<div class="modal fade add_new_item_to_billcode" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2"><b>New Billcode Item</b></h4>
      </div>
      <div class="modal-body">
      {!! Form::open(array('route' => 'billcodesitem.store', 'method' => 'POST')) !!}
        <div class="form-group">
            <input name="billcodeId" value="<?php echo base64_decode($billcodeId); ?>" hidden /> 
            {!! Form::label('pi','PI',['class'=>'control-label']) !!}
            <div class="option-group">
            <select name="bcPi_number" class="form-control pi_number_item" data-search="true" required >
              <option value=""> Select PI</option>
              @foreach($orderId as $data)
                <option value="{{$data->id}}">{{$data->pi}}</option>
              @endforeach
            </select>
            </div>
        </div>

        <div class="form-group">
              {!! Form::label('color','Color',['class'=>'control-label']) !!}
              <div class="option-group">
                  <select class="form-control pi_items " required  name="bcOritem_id"  >
                      <option value="">Select PI First</option>
                  </select>
              </div>
          </div>

        <div class="form-group">
          <label for="quantity">Quantity</label>
          <input  name="bcQuantity" required type="number" step="any" class="form-control form-white" id="quantity" placeholder="Quantity" />
        </div>

        <div class="form-group">
          <label for="unitPrice">Unit Price(TK)</label>
          <input  name="bcUnitPrice" required type="number" step="any" class="form-control form-white" id="unitPrice" placeholder="Unit Price(TK)" />
        </div>

        <div class="form-group">
          <label for="from">From</label>
          <input  name="bcFrom" required type="text" class="form-control form-white" id="from" placeholder="From " />
        </div>
          
        <div class="form-group">
          <label for="bcTo">To</label>
          <input  name="bcTo" required type="text" class="form-control form-white" id="bcTo" placeholder="To " />
        </div>

        <div class="form-group">
          <label for="remarks">Remarks</label>
          <textarea  name="bcRemarks" type="text" class="form-control form-white" id="remarks" placeholder="Remarks"></textarea>
        </div>
          
        <hr>

        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
        </div>

      {{ Form::close() }}
       
      </div>
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>


@endsection
@section('extra_scripts')
<script>

  $(document).ready(function() {
            $("select.pi_number_item").change(function () {
                var pin= $(".pi_number_item option:selected").val();
                if(!pin){
                    var rr='<select class="form-control pi_items" name="item_id">'+
                    '<option value="">Select PI First</option>'+

                    '</select>';
                    $('.pi_items').html(rr);
                }
                else {
                    $.ajax({
                        url: 'pi/items/' + pin,
                        method: 'get',
                        dataType: 'html',
                        success: function (response) {
                            $('.pi_items').html(response);
                        }
                    });
                }

            });

    $('#datatable-responsive').DataTable({
      "pageLength": 50
    });

  });
  </script>
  @endsection
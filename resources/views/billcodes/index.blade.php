@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    
    @if(Session::has('message'))
      <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
    @endif
    @if(Session::has('failedMessage'))
       <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
    @endif

    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-th"></i> Billcode List </h2>
        <ul class="nav navbar-right panel_toolbox">
          <!-- <li><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".add_new"><i class="fa fa-plus-square "></i> Add New </button></li> -->
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
         
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th width="5%">Serial</th>
              <th>Bill Code</th>
              <th>Date</th>
              <th>Remarks</th>
              <th>Created By</th>
              <th width="15%">Action</th>
            </tr>
          </thead>


          <tbody>
          @php $i=0; @endphp
          @foreach($billcodes as $billcode)
            <tr>
              <td>{{++$i}}</td>
              <td>{{$billcode->billcode}}</td>
              <td>{{$billcode->bDate}}</td>
              <td>{{$billcode->remarks}}</td>
              <td>{{$billcode->created_by}}</td>
              <td>

                <a href="{{route('billcodes.show',base64_encode($billcode->id))}}"   title="View Billcode Details" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>

                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target=".edit{{$billcode->id}}"><i class="fa fa-edit"></i></button>
                
                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target=".delete{{$billcode->id}}"><i class="fa fa-trash"></i></button>
              </td>
            </tr>



          <div class="modal fade edit{{$billcode->id}}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">

                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel2"><b>Edit Information</b></h4>
                </div>
                <div class="modal-body">
                {!! Form::open(['method'=>'PATCH', 'action'=>['billcodeController@update', $billcode->id]]) !!}
                  

                  <div class="form-group">
                    <label for="billcode">Billcode</label>
                    <input type="text" name="billcode" class="form-control form-white" id="billcode" placeholder="Billcode" value="{{$billcode->billcode}}" required >
                  </div>

                  <br />

                  <div class="form-group">
                    <label for="bDate">Date</label>
                    <input type="date" name="bDate" value="{{$billcode->bDate}}" class="form-control form-white" id="bDate" required >
                  </div>

                  <br />

                  <div class="form-group">
                    <label for="remarks">Remarks</label>
                    <textarea  name="remarks" class="form-control form-white" id="remarks" placeholder="Remarks" >{{$billcode->remarks}}</textarea>
                  </div>
                  <hr>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                  </div>

                {{ Form::close() }}
                 
                </div>
                <div class="modal-footer">
                </div>

              </div>
            </div>
          </div>


            <div class="modal fade delete{{$billcode->id}}" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2"><b>Confirm Delete</b></h4>
                  </div>
                  <div class="modal-body">
                    {!! Form::open(['method'=>'DELETE', 'action'=>['billcodeController@destroy', $billcode->id]]) !!}
                      <button type="submit" class="btn btn-danger"><i class="fa fa-check-circle"></i> Confirm</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    {!! Form::close() !!}
                    
                  </div>
                  <div class="modal-footer">
                  </div>

                </div>
              </div>
            </div>


            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


<div class="modal fade add_new" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2"><b><i class="fa fa-ils"></i> Generate New Billcode</b></h4>
      </div>
      <div class="modal-body">
      {!! Form::open(array('action' => 'billcodeController@store', 'method' => 'POST')) !!}

        <div class="form-group">
          <label for="billcode">Bill Code</label>
          <input type="text" name="billcode" class="form-control form-white" id="billcode" placeholder="Billcode" required >
        </div>

        <br />

        <div class="form-group">
          <label for="loanDate">Date</label>
          <input type="date" name="bDate" class="form-control form-white" id="bDate" required >
        </div>

        <br />

        <div class="form-group">
          <label for="remarks">Remarks</label>
          <textarea name="remarks" class="form-control form-white" id="remarks" placeholder="Remarks" ></textarea>
        </div>

        <hr>

        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
        </div>

        {{ Form::close() }}

      </div>
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>


@endsection
@section('extra_scripts')
  <script>
      $(document).ready(function() {

        $('#datatable-responsive').DataTable({
        	 "pageLength": 50
        });

      });
      setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
@endsection
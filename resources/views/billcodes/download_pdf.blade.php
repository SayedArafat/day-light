<!DOCTYPE html>
<html>
<head>
	<title>Billcode</title>
</head>
<body>
	<style type="text/css">
		@page { sheet-size: A4-L; }
		.table2{
			border-collapse: collapse;
			width: 100%;
			text-align: center;
		}
		.table2 td,th{
			border:1px solid black;
			font-size: 12px;
		}
		.table2 td{
			padding: 7px 2px;
		}
		.table2 th{
			padding: 3px 1px;
		}
		.table3{
			border-collapse: collapse;
			text-align: center;
			width: 100%;
			text-decoration: underline;
			font-weight: bold;
			font-size: 12px;
			padding-bottom:30px; 
		}
		.table3 td{
			text-decoration: underline;
		}
	</style>
	<table border="1px" class="table2">
		<tr>
			<th rowspan="2">BUY GREIGE</th>
			<th>Yds</th>
			<th>Unit Price</th>
			<th>Total</th>
			<th colspan="4" rowspan="2">PURCHESS</th>
			<th colspan="2">Date: {{$billcodeDetails->bDate}}</th>
		</tr>
		<tr>
			<td>{{$billcodeitems->totbcQuantity}}</td>
			<td>{{$billcodeitems->tunit}}</td>
			<td>{{($billcodeitems->totbcQuantity)*($billcodeitems->tunit)}}</td>
			<th colspan="2">BILL Code: {{$billcodeDetails->billcode}}</th>
		</tr>
		<tr>
			<th>Description Of <br /> The Goods</th>
			<th width="12%">PI</th>
			<th width="10%">Color</th>
			<th width="8%">Booking QTY</th>
			<th width="7%">QTY</th>
			<th width="7%">Unit Price (TK)</th>
			<th width="8%">Total Amount (TK)</th>
			<th width="10%">From</th>
			<th width="10%">To</th>
			<th width="10%">Remarks</th>
		</tr>
		<tbody>
	    @php $i=0;$qty=0;$total=0; @endphp
	     @forelse($billcodeitems as $bci)
	      @php $qty+=($bci->bcQuantity);$total+=($bci->bcQuantity)*($bci->bcUnitPrice); @endphp
	      <tr>
	        <td>{{$bci->details}}</td>
	        <td>{{$bci->pi}}</td>
	        <td>{{$bci->color}}</td>
	        <td>{{$bci->bookingQuantity}}</td>
	        <td>{{$bci->bcQuantity}}</td>
	        <td>{{$bci->bcUnitPrice}}</td>
	        <td>{{($bci->bcQuantity)*($bci->bcUnitPrice)}}</td>
	        <td>{{($bci->bcFrom)}}</td>
	        <td>{{($bci->bcTo)}}</td>
	        <td>{{($bci->bcRemarks)}}</td>
	      </tr>
	     @empty
	      <tr>
	        <td colspan="10"> No data found.</td>
	      </tr>
	      @endforelse
	    </tbody>
	</table>
	<div style="font-size:13px;font-weight:bold;padding:30px 25px;">NOTES:</div>
	<table class="table3" style="margin-bottom:50px;">
		<tr>
			<td width="25%">Merchandiser</td>
			<td width="25%">Accounts</td>
			<td width="25%">GM</td>
			<td width="25%">MD</td>
		</tr>
	</table>
	<div style="font-size:13px;font-weight:bold;padding:25px 25px;text-decoration:underline;">Comments:</div>

</body>
</html>

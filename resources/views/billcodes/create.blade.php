@extends('layouts.master')
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">

    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-money"></i> Generate New Bill Code </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        {!! Form::open(['method'=>'POST','action'=>'billcodeController@store']) !!}
      
        <div class="form-group">
          <label for="billcode">Bill Code</label>
          <input type="text" readonly value="{{$billcode}}" name="billcode" class="form-control form-white" id="billcode" placeholder="Bill Code Number " required >
        </div>

        <br />

        <div class="form-group">
          <label for="bDate">Bill Code Date</label>
          <input name="bDate" value="{{date('Y-m-d')}}" type="text" class="form-control form-white" id="bDate" required />
        </div>

        <br />
        
        <div class="form-group">
          <label for="remarks">Remarks</label>
          <textarea name="remarks" class="form-control form-white" id="remarks" placeholder="Remarks" ></textarea>
        </div>

        <hr>

        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save Billcode</button>
          <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Reset</button>
        </div>

        {{ Form::close() }}


      </div>
    </div>
  </div>

</div>
@endsection
@section('extra_scripts')
<script>
  setTimeout(function() {
    $('#alert_message').fadeOut('fast');
  }, 5000);
</script>
@endsection
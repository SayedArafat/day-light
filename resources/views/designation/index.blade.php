@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    
    @if(Session::has('message'))
    <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
    @endif
    @if(Session::has('failedMessage'))
    <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
    @endif

    <div class="x_panel">
      <div class="x_title">
        <h2>Designation List </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".add_new"><i class="fa fa-plus-square "></i> Add New </button>
          </li>
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Serial</th>
              <th>Designation Name</th>
              <th>Designation Description</th>
              <th>Created At</th>
              <th>Created By</th>
              <th>Action</th>
            </tr>
          </thead>


          <tbody>
            @php $i=0; @endphp
            @foreach($designations as $designation)
            <tr>
              <td>{{++$i}}</td>
              <td>{{$designation->designationName}}</td>
              <td>{{$designation->designationDescription}}</td>
              <td>{{$designation->created_at}}</td>
              <td>{{$designation->created_by}}</td>
              <td>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".edit{{$designation->id}}"><i class="fa fa-edit"></i></button>
                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target=".delete{{$designation->id}}"><i class="fa fa-trash"></i></button>
              </td>
            </tr>



            <div class="modal fade edit{{$designation->id}}" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2"><b>Edit Designation Information</b></h4>
                  </div>
                  <div class="modal-body">
                    {!! Form::open(['method'=>'PATCH', 'action'=>['DesignationController@update', $designation->id]]) !!}
                    <div class="form-group">
                      <label for="designationName">Designation Name</label>
                      <input type="text" value="{{$designation->designationName}}" name="designationName" class="form-control form-white" id="designationName" placeholder="Designation Name" required >
                    </div>

                    <br />

                    <div class="form-group">
                      <label for="designationDescription">Designation Description</label>
                      <textarea  name="designationDescription" class="form-control form-white" id="designationDescription" placeholder="Designation Description" >{{$designation->designationDescription}}</textarea>
                    </div>
                    
                    <hr>

                    <div class="form-group">
                      <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    </div>

                    {{ Form::close() }}
                    
                  </div>
                  <div class="modal-footer">
                  </div>

                </div>
              </div>
            </div>


            <div class="modal fade delete{{$designation->id}}" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2"><b>Confirm Delete</b></h4>
                  </div>
                  <div class="modal-body">
                    {!! Form::open(['method'=>'DELETE', 'action'=>['DesignationController@destroy', $designation->id]]) !!}
                    <button type="submit" class="btn btn-danger"><i class="fa fa-check-circle"></i> Confirm</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    {!! Form::close() !!}
                    
                  </div>
                  <div class="modal-footer">
                  </div>

                </div>
              </div>
            </div>


            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade add_new" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2"><b>New Designation</b></h4>
      </div>
      <div class="modal-body">
        {!! Form::open(array('action' => 'DesignationController@store', 'method' => 'POST')) !!}
        
        <div class="form-group">
          <label for="designationName">Designation Name</label>
          <input type="text" name="designationName" class="form-control form-white" id="designationName" placeholder="Designation Name" required >
        </div>

        <br />

        <div class="form-group">
          <label for="designationDescription">Designation Description</label>
          <textarea  name="designationDescription" class="form-control form-white" id="designationDescription" placeholder="Designation Description" ></textarea>
        </div>
        
        <hr>

        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
        </div>

        {{ Form::close() }}
        
      </div>
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>

@endsection
@section('extra_scripts')
<script>
  $(document).ready(function() {

    $('#datatable-responsive').DataTable({
      "pageLength": 50
    });

  });
  
  setTimeout(function() {
    $('#alert_message').fadeOut('fast');
  }, 5000);
</script>
@endsection
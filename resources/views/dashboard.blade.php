@extends('layouts.master')
@section('content')

<div class="dashboard">
	<div class="row top_tiles">
	  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
	    <div class="tile-stats">
	      <div class="icon"><i class="fa fa-users"></i></div>
	      <div class="count">{{sprintf('%03d',$users)}}</div>
	      <h4 style="padding-left: 15px;">Employees</h4>
	    </div>
	  </div>
	  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
	    <div class="tile-stats">
	      <div class="icon"><i class="fa fa-recycle"></i></div>
	      <div class="count">{{sprintf('%03d',$pendingOrder)}}</div>
	      <h4 style="padding-left: 15px;">Pending Order</h4>
	    </div>
	  </div>
	  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
	    <div class="tile-stats">
	      <div class="icon"><i class="fa fa-check-circle"></i></div>
	      <div class="count">{{sprintf('%03d',$completedOrder)}}</div>
	      <h4 style="padding-left: 15px;">Completed Order</h4>
	    </div>
	  </div>
	  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
	    <div class="tile-stats">
	      <div class="icon"><i class="fa fa-close"></i></div>
	      <div class="count">{{sprintf('%03d',$completedOrder)}}</div>
	      <h4 style="padding-left: 15px;">Rejected Order</h4>
	    </div>
	  </div>
	</div>

	<div class="row">
	  <div class="col-md-12">
	    <div class="x_panel">
	      <div class="x_title">
	        <h2>Transaction Summary <small>Weekly progress</small></h2>
	        <div class="filter">
	          <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
	            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
	            <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
	          </div>
	        </div>
	        <div class="clearfix"></div>
	      </div>
	      <div class="x_content">
	        <div class="col-md-12 col-sm-12 col-xs-12">
	          <div class="demo-container" style="height:280px">
	            <div id="chart_plot_02" class="demo-placeholder"></div>
	          </div>

	        </div>
	      </div>
	    </div>
	  </div>
	</div>



	<div class="row">
	  <div class="col-md-12">
	    <div class="x_panel">
	      <div class="x_title">
	        <h2>Weekly Order Summary <small></small></h2>
	        <ul class="nav navbar-right panel_toolbox">
	          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	          </li>
	          <li class="dropdown">
	            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	          </li>
	          <li><a class="close-link"><i class="fa fa-close"></i></a>
	          </li>
	        </ul>
	        <div class="clearfix"></div>
	      </div>
	      <div class="x_content">

	        <div class="row" style="border-bottom: 1px solid #E0E0E0; padding-bottom: 5px; margin-bottom: 5px;">
	          <div class="col-md-12" style="overflow:hidden;">
	            <span class="sparkline_one" style="height: 250px; padding: 10px 25px;">
	                          <canvas  height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
	                      </span>
	            <h4 style="margin:18px">Weekly sales progress</h4>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>



	<div class="row">
	  <div class="col-md-4">
	    <div class="x_panel" style="min-height: 500px;">
	      <div class="x_title">
	        <h2>Given Loan List<small></small></h2>
	        <ul class="nav navbar-right panel_toolbox">
	          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	          </li>
	          <li><a class="close-link"><i class="fa fa-close"></i></a>
	          </li>
	        </ul>
	        <div class="clearfix"></div>
	      </div>
	      <div class="x_content ">
	      	 @if($gloans)
		        @foreach($gloans as $loan)
			        <article class="media event">
			          <a class="pull-left date">
			            <p class="month"><?php echo date("M", strtotime($loan->loanDate)); ?></p>
			            <p class="day"><?php echo date("d", strtotime($loan->loanDate)); ?></p>
			          </a>
			          <div class="media-body">
			            <a class="title" href="#">{{$loan->name}}</a>
			            <p style="color:blue;">Loan Amount: @money(\Illuminate\Support\Facades\DB::table('tbgive_loan_history')->where([['loanId','=',$loan->id],['paymentType','=','1']])->sum('amount')) </p>
			            <p style="color:red;font-weight: 600;">Balance Amount: @money((\Illuminate\Support\Facades\DB::table('tbgive_loan_history')->where([['loanId','=',$loan->id],['paymentType','=','1']])->sum('amount'))-(\Illuminate\Support\Facades\DB::table('tbgive_loan_history')->where([['loanId','=',$loan->id],['paymentType','=','2']])->sum('amount'))) </p>
			          </div>
			        	<hr>
			        </article>
		        @endforeach
		     @else
		     	<p>No data found.</p>
		     @endif
	      </div>
	    </div>
	  </div>

	  <div class="col-md-4">
	    <div class="x_panel" style="min-height: 500px;">
	      <div class="x_title">
	        <h2>Received Loan List</h2>
	        <ul class="nav navbar-right panel_toolbox">
	          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	          </li>
	          <li class="dropdown">
	            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            
	          </li>
	          <li><a class="close-link"><i class="fa fa-close"></i></a>
	          </li>
	        </ul>
	        <div class="clearfix"></div>
	      </div>
	      <div class="x_content">
	         @if($rloans)
		        @foreach($rloans as $loan)
			        <article class="media event">
			          <a class="pull-left date">
			            <p class="month"><?php echo date("M", strtotime($loan->loanDate)); ?></p>
			            <p class="day"><?php echo date("d", strtotime($loan->loanDate)); ?></p>
			          </a>
			          <div class="media-body">
			            <a class="title" href="#">{{$loan->name}}</a>
			            <p style="color:blue;">Loan Amount: @money(\Illuminate\Support\Facades\DB::table('tbreceived_loan_history')->where([['loanId','=',$loan->id],['paymentType','=','1']])->sum('amount')) </p>
			            <p style="color:red;font-weight: 600;">Balance Amount: @money((\Illuminate\Support\Facades\DB::table('tbreceived_loan_history')->where([['loanId','=',$loan->id],['paymentType','=','1']])->sum('amount'))-(\Illuminate\Support\Facades\DB::table('tbreceived_loan_history')->where([['loanId','=',$loan->id],['paymentType','=','2']])->sum('amount'))) </p>
			          </div>
			        	<hr>
			        </article>
		        @endforeach
		     @else
		     	<p>No data found.</p>
		     @endif
	      </div>
	    </div>
	  </div>

	  <div class="col-md-4">
	    <div class="x_panel" style="min-height: 500px;">
	      <div class="x_title">
	        <h2>Advance Payment list </h2>
	        <ul class="nav navbar-right panel_toolbox">
	          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	          </li>
	          <li class="dropdown">
	            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	            <ul class="dropdown-menu" role="menu">
	              <li><a href="#">Settings 1</a>
	              </li>
	              <li><a href="#">Settings 2</a>
	              </li>
	            </ul>
	          </li>
	          <li><a class="close-link"><i class="fa fa-close"></i></a>
	          </li>
	        </ul>
	        <div class="clearfix"></div>
	      </div>
	      <div class="x_content">

	         @if($advances)
		        @foreach($advances as $advance)
			        <article class="media event">
			          <a class="pull-left date">
			            <p class="month"><?php echo date("M", strtotime($advance->paymentDate)); ?></p>
			            <p class="day"><?php echo date("d", strtotime($advance->paymentDate)); ?></p>
			          </a>
			          <div class="media-body">
			            <a class="title" href="#">{{$advance->employeeName}}</a>
			            <p style="color:blue;">Advance Amount: @money(\Illuminate\Support\Facades\DB::table('tbadvance_payment_history')->where([['advancePaymentId','=',$advance->id],['paymentType','=','1']])->sum('amount')) </p>
			            <p style="color:red;font-weight: 600;">Balance Amount: @money((\Illuminate\Support\Facades\DB::table('tbadvance_payment_history')->where([['advancePaymentId','=',$advance->id],['paymentType','=','1']])->sum('amount'))-(\Illuminate\Support\Facades\DB::table('tbadvance_payment_history')->where([['advancePaymentId','=',$advance->id],['paymentType','=','2']])->sum('amount'))) </p>
			          </div>
			        	<hr>
			        </article>
		        @endforeach
		     @else
		     	<p>No data found.</p>
		     @endif

	      </div>
	    </div>
	  </div>
	</div>

</div>
@endsection
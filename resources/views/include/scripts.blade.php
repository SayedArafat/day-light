<!-- jQuery -->
{{ Html::script('assets/vendors/jquery/dist/jquery.min.js') }}

<!-- Bootstrap -->
{{ Html::script('assets/vendors/bootstrap/dist/js/bootstrap.min.js') }}

<!-- Bootstrap Live Search-->
{{ Html::script('assets/vendors/bootstrap-select/bootstrap-select.min.js') }}

<!-- FastClick -->
{{ Html::script('assets/vendors/fastclick/lib/fastclick.js') }}

<!-- NProgress -->
{{ Html::script('assets/vendors/nprogress/nprogress.js') }}

<!-- Chart.js -->
{{ Html::script('assets/vendors/Chart.js/dist/Chart.min.js') }}

<!-- jQuery Sparklines -->
{{ Html::script('assets/vendors/jquery-sparkline/dist/jquery.sparkline.min.js') }}

<!-- DateJS -->
{{ Html::script('assets/vendors/DateJS/build/date.js') }}

<!-- bootstrap-datepicker -->
{{ Html::script('assets/vendors/moment/min/moment.min.js') }}
{{ Html::script('assets/build/js/jquery-ui.js') }}

<!-- Flot -->
{{ Html::script('assets/vendors/Flot/jquery.flot.js') }}
{{ Html::script('assets/vendors/Flot/jquery.flot.time.js') }}
{{ Html::script('assets/vendors/Flot/jquery.flot.resize.js') }}
{{ Html::script('assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js') }}
{{ Html::script('assets/vendors/flot-spline/js/jquery.flot.spline.min.js') }}

<!-- Datatables -->
{{ Html::script('assets/vendors/datatables.net/js/jquery.dataTables.min.js') }}
{{ Html::script('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}
{{ Html::script('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}
{{ Html::script('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}
{{ Html::script('assets/vendors/datatables.net-scroller/js/datatables.scroller.min.js') }}

<!-- Custom Theme Scripts -->
{{ Html::script('assets/build/js/custom.js') }}
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
      <h3></h3>
      <ul class="nav side-menu">
        <li><a  href="{{url('/dashboard')}}"><i class="fa fa-home"></i> Dashboard <span class="fa fa-chevron-right"></span></a></li>
      </ul>
    </div>
    @if(checkPermission(['merchandiser','accountant','commercial']))
    <div class="menu_section">
      <h3>General</h3>
      <ul class="nav side-menu">

        <li><a><i class="fa fa-users"></i> Manage Employee  <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">
            <li><a href="{{url('/employee_list')}}">Employee List</a></li>
          </ul>
        </li>
      </ul>
    </div>
    @endif

    @if(checkPermission(['superadmin','admin','merchandiser']))
    
    <div class="menu_section">
      <h3>Merchandising</h3>

      <ul class="nav side-menu">

       <li><a><i class="fa fa-newspaper-o"></i> Manage Bill Codes  <span class="fa fa-chevron-down"></span></a>
         <ul class="nav child_menu">
           <li><a href="{{url('/generate_bill_code')}}">Generate New Bill Code</a></li>
           <li><a href="{{url('/bill_code_list')}}">Bill Code List</a></li>
         </ul>
       </li>

       <li><a><i class="fa fa-cart-plus"></i> Manage Orders <span class="fa fa-chevron-down"></span></a>
         <ul class="nav child_menu">
           <li><a href="{{url('/new_order')}}">New Order</a></li>
           <li><a href="{{url('/order_list')}}">Order List</a></li>
         </ul>
       </li>
        
        <li><a  href="{{url('/greige_chart')}}"><i class="fa fa-bar-chart"></i> Greige Chart <span class="fa fa-chevron-right"></span></a></li>

       <li><a><i class="fa fa-archive"></i> Manage Dyeing Chart <span class="fa fa-chevron-down"></span></a>
         <ul class="nav child_menu">
           <li><a href="{{route('finishPart.add')}}">New Dyeing</a></li>
           <li><a href="{{url('/dyeing_chart')}}">Dyeing Chart List</a></li>
         </ul>
       </li>

       <li><a><i class="fa fa-archive"></i> Manage Stock Fabric <span class="fa fa-chevron-down"></span></a>
         <ul class="nav child_menu">
           <li><a href="{{route('merchandising.stock_fabric.add')}}">New Entry</a></li>
           <li><a href="{{url('/stock_fabric')}}">Stock Fabric List</a></li>
         </ul>
       </li>

       <li><a><i class="fa fa-users"></i> Manage Customer <span class="fa fa-chevron-down"></span></a>
         <ul class="nav child_menu">
           <li><a href="{{url('/new_customer')}}">New Customer</a></li>
           <li><a href="{{url('/customer_list')}}">Customer List</a></li>
         </ul>
       </li>

     </ul>
   </div>
   @endif

   @if(checkPermission(['superadmin','admin','accountant']))

   <div class="menu_section">
    <h3>Accounts</h3>

    <ul class="nav side-menu">
      <li><a><i class="fa fa-money"></i> Manage Incomes  <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
         <li><a href="{{url('/todays_income_history')}}">Today's Incomes</a></li>
            <li><a href="{{url('/incomes')}}">Income List</a></li>
         <li><a href="{{url('/new_incomes')}}">New Incomes</a></li>
         <li><a href="{{url('/incomes_category')}}">Incomes Category</a></li>
       </ul>
     </li>
     <li><a><i class="fa fa-money"></i> Manage Expense  <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
         <li><a href="{{url('/todays_expense_history')}}">Today's Expense</a></li>
         <li><a href="{{url('/expenses')}}">Expense List</a></li>
         <li><a href="{{url('/new_expense')}}">New Expense</a></li>
         <li><a href="{{url('/expense_category')}}">Expense Category</a></li>
       </ul>
     </li>
     <li><a><i class="fa fa-bank"></i> Bank Transactions  <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
         <li><a href="{{url('/cash_in')}}">Cash In</a></li>
         <li><a href="{{url('/cash_out')}}">Cash Out</a></li>
         <li><a href="{{url('/all_bank_list')}}">Bank List</a></li>
         <li><a href="{{url('/transaction_history')}}">Transaction History</a></li>
       </ul>
     </li>
     <li><a><i class="fa fa-university"></i> Manage Loan  <span class="fa fa-chevron-down"></span></a>
       <ul class="nav child_menu">
         <li><a href="{{url('/give_loan')}}">Give Loan</a></li>
         <li><a href="{{url('/receive_loan')}}">Receive Loan</a></li>
       </ul>
     </li>

     <li><a  href="{{url('/manage_advance_payment')}}"><i class="fa fa-credit-card"></i> Manage Advances  <span class="fa fa-right"></span></a>
     </li>

   </ul>
   </div>


        <div class="menu_section">
            <h3>Reports</h3>

            <ul class="nav side-menu">
                <li><a><i class="fa fa-money"></i> Account Reports  <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{route('report.advance_balance')}}">Advance Balance</a></li>
                        <li><a href="{{route('report.loans')}}">Loan Report</a></li>
                    </ul>
                </li>

            </ul>
        </div>




  @endif

  @if(checkPermission(['superadmin','admin','commercial']))

  <div class="menu_section">
    <h3>Commercial</h3>

    <ul class="nav side-menu">
      <li><a><i class="fa fa-money"></i> Manage L/C  <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
         <li><a href="{{route('lc.new_lc')}}">Generate New L/C</a></li>
         <li><a href="{{route('lc.list')}}">L/C List</a></li>
       </ul>
     </li>
     <li><a><i class="fa fa-th"></i> Reports  <span class="fa fa-chevron-down"></span></a>
       <ul class="nav child_menu">
       </ul>
     </li>

   </ul>
  </div>
  @endif

  @if(checkPermission(['superadmin','admin']))
  <div class="menu_section">
    <h3>Administrative</h3>
    <ul class="nav side-menu">
      <li><a><i class="fa fa-users"></i> Manage Employee  <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="{{url('/employee_list')}}">Employee List</a></li>
          <li><a href="{{url('/employee/create')}}">New Employee</a></li>
          <li><a href="{{url('/access_role')}}">Access Role</a></li>
          <li><a href="{{url('/designation')}}">Designations</a></li>
          <li><a href="{{url('/department')}}">Departments</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-cubes"></i> Manage Reports  <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="{{url('/expense_report')}}">Expense Report</a></li>
          <li><a href="{{url('/production_report')}}">Production Report</a></li>
          <li><a href="{{url('/cost_report')}}">Cost Report</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-cubes"></i> Manage Bank  <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="{{url('/bank_list')}}">Bank List</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-bank"></i> Manage Company  <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="{{route('company_list')}}">Company List</a></li>
        </ul>
      </li>
        <li><a href="{{route('net_balance')}}"><i class="fa fa-bank"></i> Net Balance  <span class="fa fa-chevron-down"></span></a>
        </li>
    </ul>
  </div>
    @endif
</div>
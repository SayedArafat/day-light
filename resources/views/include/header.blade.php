<link rel="shortcut icon" href="{{url('assets/images/favicon.ico')}}" type="image/png">
<title>Daylight Textile Corporation</title>

<!-- Bootstrap -->
{{ Html::style('assets/vendors/bootstrap/dist/css/bootstrap.min.css') }}

<!-- Bootstrap Select Live Search -->
{{ Html::style('assets/vendors/bootstrap-select/bootstrap-select.min.css') }}

<!-- Font Awesome -->
{{ Html::style('assets/vendors/font-awesome/css/font-awesome.min.css') }}

<!-- NProgress -->
{{ Html::style('assets/vendors/nprogress/nprogress.css') }}

<!-- bootstrap-datepicker -->
{{ Html::style('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}
{{ Html::style('assets/build/css/jquery-ui.css') }}

{{ Html::style('assets/build/css/animate.css') }}

<!-- Datatables -->
{{ Html::style('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}
{{ Html::style('assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}
{{ Html::style('assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}
{{ Html::style('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}
{{ Html::style('assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}

<!-- Custom Theme Style -->
{{ Html::style('assets/build/css/custom.css') }}

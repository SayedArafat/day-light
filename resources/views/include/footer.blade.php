<footer>
  <div class="pull-left">
    Copyright &copy; <?php echo date('Y');?> | <strong>Daylight Textile Corporation</strong> 
  </div>
  <div class="pull-right">
    Developed by <strong>MSBCSE.</strong>
  </div>
  <div class="clearfix"></div>
</footer>
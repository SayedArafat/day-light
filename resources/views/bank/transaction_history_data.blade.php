@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
     <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-list"></i> Transaction History From (<span style="color:blue;">{{date("d-M-Y", strtotime($request->start_date))}}</span>) TO (<span style="color:blue;">{{date("d-M-Y", strtotime($request->end_date))}}</span>) </h2>
        <ul class="nav navbar-right panel_toolbox">
           <li> {!! Form::open(['method'=>'POST','route'=>'transaction_history_data']) !!}
                  <input hidden name="start_date" value="{{$request->start_date}}"  />
                  <input hidden name="end_date" value="{{$request->end_date}}" />
                  <button type="submit" name='submit_type' value="pdf" class="btn btn-sm"><i class="fa fa-download"></i> Download as PDF</button>
            {{ Form::close() }}
            </li>
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>SN</th>
              <th>Date</th>
              <th>Payment Type</th>
              <th>Bank Name</th>
              <th>Amount</th>
              <th>Payment Method</th>
              <th>#</th>
            </tr>
          </thead>
          <tbody>
              @php $i=0; @endphp
              @foreach($transaction_history as $th)
              <tr>
                <td width="5%">{{++$i}}</td>
                <td>{{date("d-M-Y", strtotime($th->transDate))}}</td>
                <td>
                  <?php 
                  if(($th->payType)==1){
                    echo "<span style='color:green;font-weight:600;'>Cash In</span>";
                  }
                  if(($th->payType)==2){
                    echo "<span style='color:red;font-weight:600;'>Cash Out</span>";
                  } ?>
                </td>
                <td>{{$th->bankName}}</td>
                <td><b>
                  <?php 
                  if(($th->payType)==1){ ?>
                    @money($th->creditAmount)
                  <?php }
                  if(($th->payType)==2){ ?>
                    @money($th->debitAmount)
                  <?php } ?>
                  </b>
                </td>
                <td>
                  <?php 
                  if(($th->paymentMethod)==0){
                    echo "Cash";
                  } 
                  ?>
                </td>
                <td>
                  <button class="btn-primary btn btn-xs" data-toggle="modal" data-target=".view{{$th->id}}"><i class="fa fa-search"></i> View</button>
                </td>
              </tr>

            <div class="modal fade view{{$th->id}}" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2"><b>Transaction Details</b></h4>
                  </div>
                  <div class="modal-body">
                      <ul class="to_do">
                        <li>Date: <b>{{date("d.m.Y", strtotime($th->transDate))}}</b> </li>
                      <li>
                      Payment Type : <b>
                          <?php 
                          if(($th->payType)==1){
                            echo "<span style='color:green;font-weight:600;'>Cash In</span>";
                          }
                          if(($th->payType)==2){
                            echo "<span style='color:red;font-weight:600;'>Cash Out</span>";
                          } ?></b>
                          </li>
                          <li>Bank Name: <b>{{$th->bankName}}</b></li>
                          <li>
                          Amount: <b>
                          <?php 
                          if(($th->payType)==1){ ?>
                            @money($th->creditAmount)
                          <?php }
                          if(($th->payType)==2){ ?>
                            @money($th->debitAmount)
                          <?php } ?></b>
                          </li>
                          <li>
                          Payment Method: <b>
                          <?php 
                          if(($th->paymentMethod)==0){
                            echo "Cash";
                          } 
                          ?></b>
                        </li>
                          <li>
                          Reference: <b>{{$th->reference}}</b>
                        </li>
                          <li>
                          Description: <b>{{$th->description}}</b>
                        </li>
                      </ul>

                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                  </div>

                </div>
              </div>
            </div>





              @endforeach
              
          </tbody>
        </table>

      </div>
    </div>

  </div>

</div>


@endsection
@section('extra_scripts')
<script>

  $(document).ready(function() {
    $('#datatable-responsive').DataTable({
      "pageLength": 50
    });

  });
  </script>
  @endsection
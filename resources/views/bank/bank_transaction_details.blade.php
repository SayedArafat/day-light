@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12">
    @if(Session::has('message'))
          <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
          @endif
          @if(Session::has('failedMessage'))
          <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
          @endif
  </div>

  <div class="col-md-6 col-sm-12 col-xs-12">
     <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-bank"></i> <b>Bank Details</b></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        @php $id = $bank_info->id; @endphp
        <ul class="to_do">
          <li><h4> <b>Bank Name:</b> <b style='color:blue;'>{{$bank_info->bankName}}</b></h4></li>
          <li>Account Number: <b>{{$bank_info->bankAccountNo}}</b></li>
          <li>Swift Code: <b>{{$bank_info->bankSwiftCode}}</b></li>
          <li>Vat No: <b>{{$bank_info->bankVatNo}}</b></li>
          <li>Remarks: <b>{{$bank_info->remarks}}</b></li>
        </ul>
        <hr>

        <!-- <a href="{{route('order.order_details_pdf',$id)}}"  class="btn btn-default btn-md  animated zoomIn"><i class="fa fa-download"></i> Save as PDF </a> -->

      </div>
    </div>
  </div>
  <div class="col-md-4 col-sm-8 col-xs-12">
     <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-th"></i> <b>Transaction Summary</b></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="tile-stats bg-blue text-center">
              <div class="count">@money($bank_info->tcreditAmount)</div>
              <h3 style="color:white;">Total Cash In Amount</h3>
            </div>
        </div>
        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="tile-stats bg-red text-center">
              <div class="count">@money($bank_info->tdebitAmount)</div>
              <h3 style="color:white;">Total Cash Out Amount</h3>
            </div>
        </div>
        <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="tile-stats text-center" style="background-color: green;">
              <div class="count"  style="color: white;">@money(($bank_info->tcreditAmount)-($bank_info->tdebitAmount))</div>
              <h3 style="color:white;">Current Balance Amount</h3>
            </div>
        </div>

      </div>
    </div>
  </div>


</div>

<div class="row">
  <div class="col-md-6 col-sm-12 col-xs-12">
   <div class="x_panel">
    <div class="x_title">
      <h2><i class="fa fa-th"></i> <b>Cash In History</b></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table id="datatable-responsive_" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>SN</th>
            <th>Date</th>
            <th>Amount</th>
            <th>Payment Method</th>
            <th>Reference</th>
          </tr>
        </thead>
          <tbody>
            @php $i=0; @endphp
              @foreach($cash_in as $th)
              <tr>
                <td>{{++$i}}</td>
                <td>{{date("d.m.Y", strtotime($th->transDate))}}</td>
                <td>
                  <?php 
                  if(($th->payType)==1){ ?>
                    @money($th->creditAmount)
                  <?php } ?>
                </td>
                <td>
                  <?php 
                  if(($th->paymentMethod)==0){
                    echo "Cash";
                  } 
                  ?>
                </td>
                <td>{{$th->reference}}</td>
              </tr>
              @endforeach
          </tbody>        
      </table>

    </div>
  </div>

  </div>

  <div class="col-md-6 col-sm-12 col-xs-12">
   <div class="x_panel">
    <div class="x_title">
      <h2><i class="fa fa-list"></i> <b>Cash Out History</b></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table id="datatable-responsive_" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>SN</th>
            <th>Date</th>
            <th>Amount</th>
            <th>Payment Method</th>
            <th>Reference</th>
          </tr>
        </thead>
          <tbody>
            @php $i=0; @endphp
              @foreach($cash_out as $th)
              <tr>
                <td>{{++$i}}</td>
                <td>{{date("d.m.Y", strtotime($th->transDate))}}</td>
                <td>
                  <?php 
                  if(($th->payType)==2){ ?>
                    @money($th->debitAmount)
                  <?php } ?>
                </td>
                <td>
                  <?php 
                  if(($th->paymentMethod)==0){
                    echo "Cash";
                  } 
                  ?>
                </td>
                <td>{{$th->reference}}</td>
              </tr>
              @endforeach
          </tbody>        
      </table>

    </div>
  </div>

  </div>


</div>

@endsection
@section('extra_scripts')
<script>
  $(document).ready(function() {

    $('#datatable-responsive').DataTable({
      "pageLength": 50
    });

  });
  setTimeout(function() {
        $('#alert_message').fadeOut('fast');
    }, 5000);
</script>

@endsection
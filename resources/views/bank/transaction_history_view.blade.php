@extends('layouts.master')
@section('content')

    <div class="clearfix"></div>

    <div class="row animated fadeIn">
        <div class="col-md-6 col-sm-6 col-xs-12">

            @if(Session::has('message'))
                <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
            @endif
            @if(Session::has('failedMessage'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
            @endif

            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-line-chart"></i> Transaction History</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    {!! Form::open(['method'=>'POST','route'=>'transaction_history_data']) !!}

                    <div class="form-group">
                        <div class="col-sm-3"><label for="">Start Date</label></div>
                        <input name="start_date" value="{{date('Y-m-d')}}" type="text" class="form-control form-white" id="datepicker1" required />
                    </div>

                    <br/>
                    <div class="form-group">
                        <label for="">End Date</label>
                        <input name="end_date" value="{{date('Y-m-d')}}" type="text" class="form-control form-white" id="datepicker2" required />
                    </div>

                    <br />
                    <hr>

                    <div class="form-group">
                        <button type="submit" name='submit_type' value="preview" class="btn btn-primary"><i class="fa fa-search"></i> Preview</button>
                        <button type="submit" name='submit_type' value="pdf" class="btn btn-success"><i class="fa fa-download"></i> Download as PDF</button>
                        <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Reset</button>
                    </div>

                    {{ Form::close() }}


                </div>
            </div>
        </div>

    </div>
@endsection
@section('extra_scripts')
<script>
  $( function() {
    $( "#datepicker1" ).datepicker({
       format:'YYYY-MM-DD',
    });
    $( "#datepicker2" ).datepicker({
       format:'YYYY-MM-DD',
    });
  });
  setTimeout(function() {
    $('#alert_message').fadeOut('fast');
  }, 5000);
</script>
@endsection
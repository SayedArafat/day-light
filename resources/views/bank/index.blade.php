@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    
    @if(Session::has('message'))
      <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
    @endif
    @if(Session::has('failedMessage'))
       <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
    @endif

    <div class="x_panel">
      <div class="x_title">
        <h2>Bank List </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-plus-square "></i> Add New </button>
          </li>
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Serial</th>
              <th>Bank Name</th>
              <th>Account Number</th>
              <th>Swift Code</th>
              <th>Vat No</th>
              <th>Action</th>
            </tr>
          </thead>


          <tbody>
          @php $i=0; @endphp
          @foreach($banks as $bank)
            <tr>
              <td>{{++$i}}</td>
              <td>{{$bank->bankName}}</td>
              <td>{{$bank->bankAccountNo}}</td>
              <td>{{$bank->bankSwiftCode}}</td>
              <td>{{$bank->bankVatNo}}</td>
              <td>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".view{{$bank->id}}"><i class="fa fa-eye"></i></button>
                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target=".edit{{$bank->id}}"><i class="fa fa-edit"></i></button>
                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target=".delete{{$bank->id}}"><i class="fa fa-trash"></i></button>
              </td>
            </tr>

            <div class="modal fade view{{$bank->id}}" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2"><b>Bank Details</b></h4>
                  </div>
                  <div class="modal-body">
                      <div class="form-group">
                        <label for="bankName">Bank Name</label>
                        <input type="text" name="bankName" class="form-control form-white" id="bankName" placeholder="Bank Name" value="{{$bank->bankName}}" required readonly >
                      </div>

                      <br />
                      
                      <div class="form-group">
                        <label for="bankAccountNo">Account Number</label>
                        <input type="text" name="bankAccountNo" class="form-control form-white" id="bankAccountNo" placeholder="Account Number" value="{{$bank->bankAccountNo}}" required readonly >
                      </div>

                      <br />
                      
                      <div class="form-group">
                        <label for="bankSwiftCode">Swift Code</label>
                        <input type="text" name="bankSwiftCode" class="form-control form-white" id="bankSwiftCode" placeholder="Swift Code" value="{{$bank->bankSwiftCode}}" required readonly >
                      </div>

                      <br />

                      <div class="form-group">
                        <label for="bankVatNo">Vat No</label>
                        <input type="text" name="bankVatNo" class="form-control form-white" id="bankVatNo" placeholder="Vat No" value="{{$bank->bankVatNo}}" required readonly >
                      </div>

                      <br />

                      <div class="form-group">
                        <label for="remarks">Remarks</label>
                        <textarea  name="remarks" class="form-control form-white" id="remarks" placeholder="Remarks" readonly >{{$bank->remarks}}</textarea>
                      </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                  </div>

                </div>
              </div>
            </div>



          <div class="modal fade edit{{$bank->id}}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">

                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel2"><b>Edit Bank Information</b></h4>
                </div>
                <div class="modal-body">
                {!! Form::open(['method'=>'PATCH', 'action'=>['BankController@update', $bank->id]]) !!}
                  <div class="form-group">
                    <label for="bankName">Bank Name</label>
                    <input type="text" name="bankName" class="form-control form-white" id="bankName" placeholder="Bank Name" value="{{$bank->bankName}}" required >
                  </div>

                  <br />
                  
                  <div class="form-group">
                    <label for="bankAccountNo">Account Number</label>
                    <input type="text" name="bankAccountNo" class="form-control form-white" id="bankAccountNo" placeholder="Account Number" value="{{$bank->bankAccountNo}}" required >
                  </div>

                  <br />
                  
                  <div class="form-group">
                    <label for="bankSwiftCode">Swift Code</label>
                    <input type="text" name="bankSwiftCode" class="form-control form-white" id="bankSwiftCode" placeholder="Swift Code" value="{{$bank->bankSwiftCode}}" required >
                  </div>

                  <br />

                  <div class="form-group">
                    <label for="bankVatNo">Vat No</label>
                    <input type="text" name="bankVatNo" class="form-control form-white" id="bankVatNo" placeholder="Vat No" value="{{$bank->bankVatNo}}" required >
                  </div>

                  <br />

                  <div class="form-group">
                    <label for="remarks">Remarks</label>
                    <textarea  name="remarks" class="form-control form-white" id="remarks" placeholder="Remarks" >{{$bank->remarks}}</textarea>
                  </div>
                    
                    
                  <hr>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                  </div>

                {{ Form::close() }}
                 
                </div>
                <div class="modal-footer">
                </div>

              </div>
            </div>
          </div>


            <div class="modal fade delete{{$bank->id}}" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2"><b>Confirm Delete</b></h4>
                  </div>
                  <div class="modal-body">
                    {!! Form::open(['method'=>'DELETE', 'action'=>['BankController@destroy', $bank->id]]) !!}
                      <button type="submit" class="btn btn-danger"><i class="fa fa-check-circle"></i> Confirm</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    {!! Form::close() !!}
                    
                  </div>
                  <div class="modal-footer">
                  </div>

                </div>
              </div>
            </div>


            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2"><b>New Bank Information</b></h4>
      </div>
      <div class="modal-body">
      {!! Form::open(array('route' => 'bank.store', 'method' => 'POST')) !!}
        <div class="form-group">
          <label for="bankName">Bank Name</label>
          <input type="text" name="bankName" class="form-control form-white" id="bankName" placeholder="Bank Name" required >
        </div>

        <br />
        
        <div class="form-group">
          <label for="bankAccountNo">Account Number</label>
          <input type="text" name="bankAccountNo" class="form-control form-white" id="bankAccountNo" placeholder="Account Number" required >
        </div>

        <br />
        
        <div class="form-group">
          <label for="bankSwiftCode">Swift Code</label>
          <input type="text" name="bankSwiftCode" class="form-control form-white" id="bankSwiftCode" placeholder="Swift Code" required >
        </div>

        <br />

        <div class="form-group">
          <label for="bankVatNo">Vat No</label>
          <input type="text" name="bankVatNo" class="form-control form-white" id="bankVatNo" placeholder="Vat No" required >
        </div>

        <br />

        <div class="form-group">
          <label for="remarks">Remarks</label>
          <textarea  name="remarks" class="form-control form-white" id="remarks" placeholder="Remarks" ></textarea>
        </div>
          
        <hr>

        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
        </div>

      {{ Form::close() }}
       
      </div>
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>

@endsection
@section('extra_scripts')
  <script>
      $(document).ready(function() {

        $('#datatable-responsive').DataTable({
        	 "pageLength": 50
        });

      });
      setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
@endsection
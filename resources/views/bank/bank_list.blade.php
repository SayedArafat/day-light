@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    
    @if(Session::has('message'))
      <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
    @endif
    @if(Session::has('failedMessage'))
       <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
    @endif

    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-bank"></i> All Bank List </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered animated fadeIn">
          <thead>
            <tr>
              <th width="5%">Serial</th>
              <th>Bank Name</th>
              <th>Account Number</th>
              <th>Swift Code</th>
              <th>Vat No</th>
              <th>Credit Amount</th>
              <th>Debit Amount</th>
              <th>Balance Amount</th>
              <th>Action</th>
            </tr>
          </thead>


          <tbody>
          @php $i=0; @endphp
          @foreach($banks as $bank)
            <tr>
              <td>{{++$i}}</td>
              <td>{{$bank->bankName}}</td>
              <td>{{$bank->bankAccountNo}}</td>
              <td>{{$bank->bankSwiftCode}}</td>
              <td>{{$bank->bankVatNo}}</td>
              <td>@money($bank->tcreditAmount)</td>
              <td>@money($bank->tdebitAmount)</td>
              <td>@money(($bank->tcreditAmount)-($bank->tdebitAmount))</td>
              <td>
                <a href="{{route('bank.bank_transaction_details',base64_encode($bank->id))}}"   title="View Bank Transaction Details" class="btn btn-primary btn-sm  animated fadeIn"><i class="fa fa-eye"></i></a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@endsection
@section('extra_scripts')
  <script>
      $(document).ready(function() {

        $('#datatable-responsive').DataTable({
        	 "pageLength": 50
        });

      });
      setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
@endsection
@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    
    @if(Session::has('message'))
      <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
    @endif
    @if(Session::has('failedMessage'))
       <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
    @endif

    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-money "></i> New Transaction (Cash Out)</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

            {!! Form::open(array('route' => 'bank.cash_out_store', 'method' => 'POST')) !!}

            <div id="cash_in">

              <div class="form-group" id="bank_account_form">
                <label for="bank_account">Cash Out From </label>
                <input name="payment_type" value="2" hidden > 
                <input name="payment_method" value="0" hidden >
                <select name="bankId" id="bank_account" class="form-control" data-live-search="true" required > 
                  <option value="">Select Bank </option>
                  @foreach($banks as $bank)
                    <option value="{{$bank->id}}">{{$bank->bankName." (".$bank->bankAccountNo.")"}}</option>
                  @endforeach
                </select>
              </div>
                <br />

                <div class="form-group">
                  <label for="amount">Cash Out Amount</label>
                  <input type="number" step="any" name="amount" class="form-control form-white" id="amount" placeholder="Cash Out Amount" required >
                </div>

                <br />
                
                <div class="form-group">
                  <label for="transDate">Date</label>
                  <input type="text" name="transDate" value="{{date('Y-m-d')}}" class="form-control form-white" id="datepicker1" placeholder="Date " required >
                </div>

                <br />

                <div class="form-group">
                  <label for="reference">Reference</label>
                  <input type="text" name="reference" class="form-control form-white" id="reference" placeholder="Reference "  >
                </div>

                <br />

                <div class="form-group">
                  <label for="description">Description</label>
                  <textarea name="description" class="form-control form-white" id="description" placeholder="Description" ></textarea>
                </div>
              
            </div>
            <hr>

            <div class="form-group">
              <button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure to cash out?')"><i class="fa fa-save"></i> Save Trasaction</button>
            </div>

          {{ Form::close() }}
      </div>
    </div>
  </div>
</div>

@endsection
@section('extra_scripts')
  <script>

      $(document).ready(function() {
         $( "#datepicker1" ).datepicker({
               format:'YYYY-MM-DD',
          });
      });
      setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
@endsection
@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    
    @if(Session::has('message'))
      <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
    @endif
    @if(Session::has('failedMessage'))
       <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
    @endif

    <div class="x_panel">
      <div class="x_title">
        <h2>Company List </h2>
        <ul class="nav navbar-right panel_toolbox">
          <!-- <li><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-plus-square "></i> Add New </button>
          </li> -->
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Serial</th>
              <th>Company Name</th>
              <th>Company Email</th>
              <th>Company Phone</th>
              <th>Company Logo</th>
              <th>Action</th>
            </tr>
          </thead>


          <tbody>
          @php $i=0; @endphp
          @foreach($company_info as $info)
            <tr>
              <td>{{++$i}}</td>
              <td>{{$info->companyName}}</td>
              <td>{{$info->companyEmail}}</td>
              <td>{{$info->companyPhone}}</td>
              <td>
              <img class="pull-left img-responsive" src="
                @if($info->companyLogo ==null)
                  No logo
                @endif
                @if($info->companyLogo !=null)
                {{asset("logo/".$info->companyLogo)}}
                @endif
                ">
              <td></td>
             
            </tr>

            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@endsection
@section('extra_scripts')
  <script>
      $(document).ready(function() {

        $('#datatable-responsive').DataTable({
        	 "pageLength": 50
        });

      });
      setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
@endsection
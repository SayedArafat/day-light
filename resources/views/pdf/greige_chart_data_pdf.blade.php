<!DOCTYPE html>
<html>
<head>
	<title>Greige Chart</title>
</head>
<body>
	<style type="text/css">
		@page { sheet-size: A4; }
		.table2{
			border-collapse: collapse;
			width: 100%;
			text-align: center;
		}
		.table2 td,th{
			border:1px solid black;
		}
		.table2 td{
			font-size: 11px;
			padding: 2px 2px;
		}
		.table2 th{
			font-size: 11px;
			padding: 3px 1px;
		}
		.table3{
			border-collapse: collapse;
			text-align: center;
			width: 100%;
			text-decoration: underline;
			font-weight: bold;
			font-size: 12px;
		}
		.table3 td{
			text-decoration: underline;
		}
		.table1{
			text-align: center;
			width: 100%;
		}
		.caddress{
			font-size: 11px;
		}
	</style>
	<table class="table1">
		<tr style="padding-bottom:-5px;">
			<td><h3>{{$companyInfo->companyName}}</h3></td>
		</tr>
		<tr>
			<td class="caddress">Business office: {{$companyInfo->companyBAddress}}</td>
		</tr>
		<tr>
			<td class="caddress">Factory Address: {{$companyInfo->companyFAddress}}</td>
		</tr>
		<tr>
			<td class="caddress"><b>GREIGE CHART</b></td>
		</tr>
		<tr>
			<td><hr /></td>
		</tr>
	</table>
	@if(!$greige_charts->isEmpty())
	<table border="1px" class="table2">
		<thead>
            <tr>
              <th>DATE</th>
              <th>ITEM</th>
              <th>GREIGE FACTORY</th>
              <th>DYEING FACTORY</th>
              <th>QUANTITY</th>
              <th>BILL CODE</th>
              <th width="5%"></th>
            </tr>
          </thead>
          <tbody>
              @foreach($greige_charts as $chart)
              <tr>
                <td>{{date("d.m.Y", strtotime($chart->bDate))}}</td>
                <td>{{$chart->item}}</td>
                <td>{{$chart->bcFrom}}</td>
                <td>{{$chart->bcTo}}</td>
                <td>{{$chart->bcQuantity}}</td>
                <td>{{$chart->billcode}}</td>
                <td></td>
              </tr>
              @endforeach
              
          </tbody>
	</table>
	@else
		<p style="text-align: center; font-size: 11px;">No data found.</p>
	@endif
</body>
</html>

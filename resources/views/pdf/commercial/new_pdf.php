<!DOCTYPE html>
<html>
<head>
	<title>Negotiation Application</title>
</head>
<body>
	<style type="text/css">
		*{
			padding: 0;
			margin: 0;
		}
		@page { sheet-size: A4; }
		.table2{
			border-collapse: collapse;
			width: 100%;
			text-align: center;
		}
		.table2 td,th{
			border:1px solid black;
		}
		.table2 td{
			font-size: 11px;
			padding: 2px 2px;
		}
		.table2 th{
			font-size: 11px;
			padding: 3px 1px;
		}
		.table3{
			border-collapse: collapse;
			text-align: center;
			width: 100%;
			text-decoration: underline;
			font-weight: bold;
			font-size: 12px;
		}
		.table3 td{
			text-decoration: underline;
		}
		.table1{
			text-align: left;
			width: 100%;
			margin-top: -100px;
		}
		.caddress{
			font-size: 11px;
			width: 80%;
		}
	</style>
	<table class="table1">
		<tr style="padding-bottom:-5px;">
			<td width="20%"></td>
			<td><h1 style="color:#582506;font-size: 35px;">{{$company_info->companyName}}</h1></td>
		</tr>
		<tr>
			<td width="20%"></td>
			<td class="caddress"><h2>{{$company_info->companyTagline}}</h2></td>
		</tr>
		<tr>
			<td width="20%"></td>
			<td class="caddress"><b>Head Office:</b> {{$company_info->companyBAddress}}</td>
		</tr>
		<tr>
			<td width="20%"></td>
			<td class="caddress"><b>Factory Address:</b> {{$company_info->companyFAddress}}</td>
		</tr>
		<tr>
			<td width="20%"></td>
			<td class="caddress"><b>Email</b>:{{$company_info->companyEmail}}. <b>Tel:</b>{{$company_info->companyPhone}}</td>
		</tr>
		<tr>
			<td colspan="2"><hr /></td>
		</tr>
	</table>
	
</body>
</html>

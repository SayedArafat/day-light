<!DOCTYPE html>
<html>
<head>
	<title>Negotiation Application</title>
</head>
<body>
	<style type="text/css">
		*{
			padding: 0;
			margin: 0;
		}
		@page { sheet-size: A4; }
		.table2{
			border-collapse: collapse;
			width: 100%;

		}

		.table2 td{
			font-size: 12px;
			padding: 10px 5px;
		}
		.table2 th{
			font-size: 11px;
			padding: 3px 1px;
		}
		.table3{
			border-collapse: collapse;
			text-align: center;
			width: 100%;
			text-decoration: underline;
			font-weight: bold;
			font-size: 12px;
		}
		.table3 td{
			text-decoration: underline;
		}
		.table1{
			text-align: left;
			width: 100%;
		}
		.caddress{
			font-size: 11px;
			width: 80%;
		}
		.ptext{
			line-height: 25px;
		}
		.ptext1{
			line-height: 20px;
		}
	</style>

	<table class="table1">
		<tr style="padding-bottom:-5px;">
			<td><img src="{{asset('logo/dlogo.jpg')}}"></td>
			<td>
				<h3>{{$company_info->companyName}}</h3>
				<h5>{{$company_info->companyTagline}}</h5>
				<p style="font-size: 11px;"><b>Head Office:</b> {{$company_info->companyBAddress}}</p>
				<p style="font-size: 11px;"><b>Factory Address:</b> {{$company_info->companyFAddress}}</p>
				<p style="font-size: 11px;"><b>Email</b>:{{$company_info->companyEmail}}. <b>Tel:</b>{{$company_info->companyPhone}}</p>

			</td>
		</tr>
		<tr>
			<td colspan="2"><hr /></td>
		</tr>
	</table>
	<table class="table2">
		<tr>
			<td class="ptext1">
				Date: <?php echo date('d-m-Y'); ?><br />
				To <br />
				The Manager<br />
				{{$lc_details->bankName}}<br />
				{{$lc_details->bankAddress}}<br />
			</td>
		</tr>
		<tr>
			<td class="ptext">
				Subject: <b>Negotiation of Export Documents for US$0000</b><br />
			</td>
		</tr>
		<tr>
			<td class="ptext">
				Dear Sir,<br />
				We have the pleasue to submit enclosed export documents for negotiation against <b>Back to Back L/C No. {{$lc_details->lcNumber}} , Date: {{$lc_details->lcDate}}</b> of <b>{{$lc_details->customerBankName}}</b> Opened in favour of <b>{{$company_info->companyName}}, {{$company_info->companyFAddress}}</b> By <b>{{$lc_details->customerName}}, {{$lc_details->customerAddress}}</b>. Which is advised through yours <b>{{$lc_details->bankName}}, {{$lc_details->bankAddress}}</b> Therefore, we will highly appreciate if you are kind enough to collect the Bill of Maturity from the L/C opening bank. <br /><br />
			</td>
		</tr>
		<tr>
			<td class="ptext1">

				Sincerely yours,<br/>
				<img src="{{asset('logo/dlogo.jpg')}}">
				<br /><br />

				Enclosed: <br />
				1. Proforma Invoice <br />
				2. Bill of Exchange  <br />
				3. Delivery Challan <br />
				4. Commercial Invoice <br />
				5. Packing List (4 Copy) <br />
				6. Truck receipt challan <br />
				7. Country of origin(2 copy) <br />
				8. Beneficiary Certificate 
			</td>
		</tr>
	</table>
	
</body>
</html>

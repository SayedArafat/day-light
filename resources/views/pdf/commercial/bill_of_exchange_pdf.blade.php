<!DOCTYPE html>
<html>
<head>
	<title>Bill of Exchange</title>
</head>
<body>
	<style type="text/css">
		*{
			padding: 0;
			margin: 0;
		}
		@page { sheet-size: A4; }
		.table2{
			border-collapse: collapse;
			width: 100%;

		}

		.table2 td{
			font-size: 12px;
			padding: 10px 5px;
		}
		.table2 th{
			font-size: 11px;
			padding: 3px 1px;
		}
		.table3{
			border-collapse: collapse;
			width: 100%;
			font-size: 13px;
		}
		.table1{
			text-align: left;
			width: 100%;
		}
		.caddress{
			font-size: 11px;
			width: 80%;
		}
		.ptext{
			line-height: 25px;
		}
		.tag{
			width: 2%;
		}
		.imptext{
			font-weight: bold;
			text-decoration: underline;
		}
	</style>

	<table class="table2">
		<tr>
			<td class="tag">
				<h1>Daylight</h1>
			</td>
			<td width="2%"></td>
			<td>
				<table class="table3">
					<tr>
						<td colspan="2" style="text-align:center;"><h2>BILL OF EXCHANGE</h2></td>
					</tr>
					<tr>
						<td>
							No: {{$lc_details->challanNo}}
						</td>
						<td style="text-align:right;">
							Date: {{date('d.m.Y')}}
						</td>
					</tr>
					<tr>
						<td colspan="2" class="ptext" style="text-align:justify;">
							Documentary Credit No. <span class="imptext">{{$lc_details->lcNumber}}, Date: {{$lc_details->lcDate}}</span> Of <span class="imptext">{{$lc_details->customerBankName}},{{$lc_details->customerBankInfo}}.</span> Exchange for <span class="imptext">US$00.00</span> at <span class="imptext">{{$lc_details->sightDays}}</span> Days Sight of this First Of Exchange (second of the Same Tenor and Date being unpaid)  <span class="imptext">Under the export Contract no. {{$lc_details->contractNo}} DATED {{$lc_details->contractDate}}</span>. Pay to The Order of <span class="imptext">{{$lc_details->customerName}}.</span> Sum of <span class="imptext">U.S. Dollar $00.00</span> , Value Received <span class="imptext">{{$lc_details->valueReceived}}</span> Drawn Under {{$lc_details->bankName}}, {{$lc_details->bankAddress}}.
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<b>To of {{$lc_details->customerName}}, {{$lc_details->customerAddress}}</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<br />
	<br />
	<br />
	<br />
	<br />
	<br />

	<table class="table2">
		<tr>
			<td class="tag">
				<h1>Daylight</h1>
			</td>
			<td width="2%"></td>
			<td>
				<table class="table3">
					<tr>
						<td colspan="2" style="text-align:center;"><h2>BILL OF EXCHANGE</h2></td>
					</tr>
					<tr>
						<td>
							No: {{$lc_details->challanNo}}
						</td>
						<td style="text-align:right;">
							Date: {{date('d.m.Y')}}
						</td>
					</tr>
					<tr>
						<td colspan="2" class="ptext" style="text-align:justify;">
							Documentary Credit No. <span class="imptext">{{$lc_details->lcNumber}}, Date: {{$lc_details->lcDate}}</span> Of <span class="imptext">{{$lc_details->customerBankName}},{{$lc_details->customerBankInfo}}.</span> Exchange for <span class="imptext">US$00.00</span> at <span class="imptext">{{$lc_details->sightDays}}</span> Days Sight of this First Of Exchange (second of the Same Tenor and Date being unpaid)  <span class="imptext">Under the export Contract no. {{$lc_details->contractNo}} DATED {{$lc_details->contractDate}}</span>. Pay to The Order of <span class="imptext">{{$lc_details->customerName}}.</span> Sum of <span class="imptext">U.S. Dollar $00.00</span> , Value Received <span class="imptext">{{$lc_details->valueReceived}}</span> Drawn Under {{$lc_details->bankName}}, {{$lc_details->bankAddress}}.
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<b>To of {{$lc_details->customerName}}, {{$lc_details->customerAddress}}</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</body>
</html>
<!DOCTYPE html>
<html>
<head>
	<title>Bill of Exchange</title>
</head>
<body>
	<style type="text/css">
		*{
			padding: 0;
			margin: 0;
		}
		@page { sheet-size: A4; }
		.table2{
			border-collapse: collapse;
			width: 100%;

		}

		.table2 td{
			font-size: 12px;
			padding: 10px 5px;
		}
		.table2 th{
			font-size: 11px;
			padding: 3px 1px;
		}
		.table3{
			border-collapse: collapse;
			text-align: center;
			width: 100%;
			text-decoration: underline;
			font-weight: bold;
			font-size: 12px;
		}
		.table3 td{
			text-decoration: underline;
		}
		.table1{
			text-align: left;
			width: 100%;
		}
		.caddress{
			font-size: 11px;
			width: 80%;
		}
		.ptext{
			line-height: 25px;
		}
		.ptext1{
			line-height: 20px;
		}
	</style>

	<table class="table2">
		<tr>
			<td width="5%"></td>
			<td>
				<table class="table3">
					<tr>
						<td width="50%">
							No: 
						</td>
						<td width="50%">
							Date: 
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</body>
</html>

<!DOCTYPE html>
<html>
<head>
	<title>Proforma Invoice</title>
</head>
<body>
	<style type="text/css">
		@page { sheet-size: A4; }
		*{
			font-size: 12px;
		}
		.table2{
			border-collapse: collapse;
			width: 100%;
		}
		.table2 td{
			font-size: 12px;
			padding: 2px 2px;
		}
		.table2 th{
			font-size: 11px;
			padding: 3px 1px;
		}
		.table3{
			border-collapse: collapse;
			text-align: center;
			width: 100%;
			text-decoration: underline;
			font-weight: bold;
			font-size: 12px;
		}
		.table3 td{
			text-decoration: underline;
		}
		.table1{
			text-align: center;
			width: 100%;
		}
		.caddress{
			font-size: 11px;
		}
		.customerInfo{
			text-align: left;
		}
		.piDetails{
			text-align: right;
		}
		.table4{
			width: 100%;
			border-collapse: collapse;
			font-size: 11px;
		}
		.table4 td,th{
			text-align: center;
			border: 1px solid #333;
			padding: 3px 1px;
		}
		p{
			font-size: 12px;
		}
		.bankInfo{
			width: 100%;
			font-size: 12px;
		}
	</style>
	<table class="table1">
		<tr style="padding-bottom:-5px;">
			<td><h3>{{$companyInfo->companyName}}</h3></td>
		</tr>
		<tr>
			<td class="caddress">Business office: {{$companyInfo->companyBAddress}}</td>
		</tr>
		<tr>
			<td class="caddress">Factory Address: {{$companyInfo->companyFAddress}}</td>
		</tr>
		<tr>
			<td class="caddress"><b>Proforma Invoice</b></td>
		</tr>
		<hr />
	</table>
	<table class="table2">
		<tr>
			<td width="40%" class="customerInfo">
				To,<br />
				{{$orders->customerName}} <br />
				{{$orders->customerAddress}}

			</td>
			<td></td>
			<td width="25%" class="piDetails">
				Date : <?php echo date("d-M-Y", strtotime($orders->orderDate)); ?><br />
				PI: {{$orders->pi}}
			</td>
		</tr>
		<tr><td colspan="3">&nbsp;</td></tr>
	</table>
	<table class="table4">
      <thead>
        <tr>
          <th width="28%">Description of the goods</th>
          <th width="14%">Style</th>
          <th width="14%">Colour</th>
          <th width="14%">Item Quantity</th>
          <th width="14%">Unit Price</th>
          <th>Total Amount</th>
        </tr>
      </thead>
        <tbody>
          @php $total=0;$tquantity=0; @endphp
          @forelse($orderItems as $item)
          <tr>
            <td>{{$item->details}}</td>
            <td>{{$item->style}}</td>
            <td>{{$item->color}}</td>
            <td>@quantity($item->orderQuantity)</td>
            <td>@dollarMoney($item->unitPrice)</td>
            <td>@dollarMoney(($item->orderQuantity*$item->unitPrice))</td>
          </tr>
          <?php $total += $item->orderQuantity*$item->unitPrice; $tquantity+=($item->orderQuantity); ?>
          @empty
            <tr>
              <td colspan="6"> No data found.</td>
            </tr>
          @endforelse
        </tbody>
        <tfoot>
          <tr>
            <th colspan="3" style="text-align:right;">Total &nbsp;&nbsp;</th>
            <th>@quantity($tquantity)</th>
            <th></th>
            <th>@dollarMoney($total)</th>
          </tr>
        </tfoot>
    </table>
	<table class="bankInfo">
		<tr>
			<td colspan="2"></td>
		</tr>
		<tr>
			<td colspan="2">(Say Total: )</td>
		</tr>
		<tr>
			<td colspan="2">1) Goods Delivery destination: {{$orders->customerName}}</td>
		</tr>
		<tr>
			<td colspan="2">2) Goods Origin: {{$orders->goodsOrigin}} </td>
		</tr>
		<tr>
			<td colspan="2">3) Advising Bank: {{$orders->bankName}}</td>
		</tr>
		<tr>
			<td width="15%"></td>
			<td>A/C: {{$orders->bankAccountNo}}</td>
		</tr>
		<tr>
			<td></td>
			<td>Swift Code: {{$orders->bankSwiftCode}}</td>
		</tr>
		<tr>
			<td></td>
			<td>VAT No: {{$orders->bankVatNo}}</td>
		</tr>
		<tr>
			<td colspan="2">4) Allowence: 3% more or less to be accepted in quantity and amount.</td>
		</tr>
		<tr>
			<td colspan="2">5) Packing: By standared packing</td>
		</tr>
		<tr>
			<td colspan="2">6) Shipment Date: <?php echo date("d-M-Y", strtotime($orders->shippingDate)); ?> </td>
		</tr>
		<tr>
			<td colspan="2">7) Paymenmt: By 100% confirmed irrevocable L/C at sight 90 days from the date of bill of exchange issued.</td>
		</tr>
	</table>
	
    <p>
    	<span style="text-decoration:underline;"><b>Remarks:</b></span><br />
    	a)	Part shipment should be allowed.<br />
		b)	Negotiable : Any bank in Bangladesh<br />
		c)	L/C amendment charge are on L/C applicant account <br />
		d)	After signature of delivery challan any discrepancy won’t be acceptable.<br />
		e)	Overdue interest (If any) should be paid by opener.<br />
		f)	Payment to be made on maturity date in US Dollar with interest at libor rate<br />
		g)	The buyer should be provide a copy of the master L/C and garments export UD before shipment.<br />
		h)	All apparent errors /discrepancy (If any) would be accepted by opener.<br />		
		i)	Item and garments should be mentioned in L/C.<br />

    </p>
    <br />
    <p>Envoy group</p><br />
    <table>
    	<tr><td></td></tr>
    	<tr>
    		<td style="font-size:12px;border-top:1px solid #888;margin-top:30px;max-width:150px">Authorize Signature</td>
    	</tr>
    </table>
</body>
</html>

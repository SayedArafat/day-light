<!DOCTYPE html>
<html>
<head>
	<title>Stock Fabric</title>
</head>
<body>
	<style type="text/css">
		@page { sheet-size: A4; }
		.table2{
			border-collapse: collapse;
			width: 100%;
			text-align: center;
		}
		.table2 td,th{
			border:1px solid black;
		}
		.table2 td{
			font-size: 11px;
			padding: 2px 2px;
		}
		.table2 th{
			font-size: 11px;
			padding: 3px 1px;
		}
		.table3{
			border-collapse: collapse;
			text-align: center;
			width: 100%;
			text-decoration: underline;
			font-weight: bold;
			font-size: 12px;
		}
		.table3 td{
			text-decoration: underline;
		}
		.table1{
			text-align: center;
			width: 100%;
		}
		.caddress{
			font-size: 11px;
		}
	</style>
	<table class="table1">
		<tr style="padding-bottom:-5px;">
			<td><h3>{{$companyInfo->companyName}}</h3></td>
		</tr>
		<tr>
			<td class="caddress">Business office: {{$companyInfo->companyBAddress}}</td>
		</tr>
		<tr>
			<td class="caddress">Factory Address: {{$companyInfo->companyFAddress}}</td>
		</tr>
		<tr>
			<td class="caddress"><b>Stock Fabric From {{date("d.m.Y", strtotime($request->start_date))}} to {{date("d.m.Y", strtotime($request->end_date))}}</b></td>
		</tr>
		<tr>
			<td><hr /></td>
		</tr>
	</table>
	@if(!$stock_fabrics->isEmpty())
	<table border="1px" class="table2">
		 <thead>
            <tr>
              <th>Ref </th>
              <th>Date</th>
              <th>Item</th>
              <th>Color</th>
              <th>PI</th>
              <th>Q'TY/yds</th>
              <th>Details</th>
              <th>Remarks</th>
            </tr>
          </thead>
          <tbody>
	          @foreach($stock_fabrics as $st)
	              <tr>
	                <td>{{$st->sfRef}}</td>
	                <td>{{$st->sfDate}}</td>
	                <td>{{$st->sfItem}}</td>
	                <td>{{$st->sfColor}}</td>
	                <td>{{$st->sfPi}}</td>
	                <td><b>{{$st->sfQty}}</b></td>
	                <td>{{$st->sfDetails}}</td>
	                <td>{{$st->sfRemarks}}</td>
	              </tr>
	          @endforeach
          </tbody>
	</table>
	@else
		<p style="text-align: center; font-size: 11px;">No data found.</p>
	@endif
</body>
</html>

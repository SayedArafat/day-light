<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Advance Payment List </title>
    <style>
        @page { sheet-size: A4; }
        body{
            font-family: 'bangla', sans-serif;
            font-size: 10px;

        }
        p{
            line-height: 1px;
        }


        #employeeDetails td, #employeeDetails th {
            /*border: 1px solid #ddd;*/
            font-size: 12px;

        }
        #customers {

            border-collapse: collapse;
            width: 100%;
            text-align: center;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            text-align: center !important;

        }

        #customers th {
            font-size: 15px;
            text-align: center;
            padding: 5px;

        }
        #customers td {
            font-size: 13px;

        }

        .reportHeaderArea{
            text-align: center;
        }

        .reportHeader{
            line-height: 4px;
        }

        .reportHeader{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            font-size: 10px;
        }

        .reportHeaderCompany{
            font-size: 18px !important;

        }

        #cardFooter{
            border-collapse: collapse;
            font-size:11px;
            width:70%;
            margin:0px auto;
            margin-top:15px;
            /*float:right; */

        }
        .reportDateRange{
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif  !important;
            font-size: 12px !important;
            font-weight: bold;
        }
        #cardFooter td {
            text-align: left !important;
        }

    </style>
</head>
<body>

<div class="container">
    <div class="reportHeaderArea">
        <h2 class="reportHeaderCompany">Daylight Corporation</h2>
        <p class="reportHeader">Uttora, Dhaka </p>
        <p class="reportHeader">daylight@email.com</p>
        <p class="reportHeader">020109278</p>
    </div>

    <div style="text-align: center">
        <h2>
            <strong>Received Loan List From
                {{\Carbon\Carbon::parse($request->start_date)->format('d M Y')}} to
                {{\Carbon\Carbon::parse($request->end_date)->format('d M Y')}}
            </strong>
        </h2>
    </div>


    @if(count($received_loan)!=0)
        <table id='customers' style="margin-top:10px;font-size:10px;" border="1px">
            <thead>
            <tr>
                <th>Serial</th>
                <th>Name/Others</th>
                <th>Loan Date</th>
                <th>Loan Amount</th>
                <th>Return Amount</th>
                <th>Balance</th>
            </tr>
            </thead>


            <tbody>
            @php
                $i=0;
                $am=0;
                $em=0;
            @endphp
            @foreach($received_loan as $rl)
                <?php
                $loan_amount=\Illuminate\Support\Facades\DB::table('tbreceived_loan_history')->where('loanId','=',$rl->id)->where('paymentType','=','1')->sum('amount');
                $return_amount=\Illuminate\Support\Facades\DB::table('tbreceived_loan_history')->where('loanId','=',$rl->id)->where('paymentType','=','2')->sum('amount');
                ?>
                <tr>
                    <td>{{++$i}}</td>
                    <td>{{$rl->name}}</td>
                    <td>{{$rl->loanDate}}</td>
                    <td>
                        {{ $loan_amount }}
                    </td>
                    <td>
                        {{$return_amount}}
                    </td>
                    <td>
                        {{$loan_amount-$return_amount}}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <hr>
        <h4 style="color:red; text-align: center;"> No Data Found </h4>
    @endif



</div>


</body>
</html>
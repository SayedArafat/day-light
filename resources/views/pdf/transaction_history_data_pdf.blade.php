<!DOCTYPE html>
<html>
<head>
	<title>Bank Transaction History</title>
</head>
<body>
	<style type="text/css">
		@page { sheet-size: A4; }
		.table2{
			border-collapse: collapse;
			width: 100%;
			text-align: center;
		}
		.table2 td,th{
			border:1px solid black;
		}
		.table2 td{
			font-size: 11px;
			padding: 2px 2px;
		}
		.table2 th{
			font-size: 10px;
			padding: 3px 1px;
		}
		.table3{
			border-collapse: collapse;
			text-align: center;
			width: 100%;
			text-decoration: underline;
			font-weight: bold;
			font-size: 11px;
		}
		.table3 td{
			text-decoration: underline;
		}
		.table1{
			text-align: center;
			width: 100%;
		}
		.caddress{
			font-size: 11px;
		}
	</style>
	<table class="table1">
		<tr style="padding-bottom:-5px;">
			<td><h3>{{$companyInfo->companyName}}</h3></td>
		</tr>
		<tr>
			<td class="caddress">Business office: {{$companyInfo->companyBAddress}}</td>
		</tr>
		<tr>
			<td class="caddress">Factory Address: {{$companyInfo->companyFAddress}}</td>
		</tr>
		<tr>
			<td class="caddress"><b>Transaction History ({{date("d-M-Y", strtotime($request->start_date))}} to {{date("d-M-Y", strtotime($request->end_date))}})</b></td>
		</tr>
		<tr>
			<td><hr /></td>
		</tr>
	</table>
	@if(!$transaction_history->isEmpty())
	<table border="1px" class="table2">
          <thead>
            <tr>
              <th>SN</th>
              <th>Date</th>
              <th>Payment Type</th>
              <th>Bank Name</th>
              <th>Amount</th>
              <th>Payment Method</th>
            </tr>
          </thead>
          <tbody>
              @php $i=0; @endphp
              @foreach($transaction_history as $th)
              <tr>
                <td width="5%">{{++$i}}</td>
                <td>{{date("d-M-Y", strtotime($th->transDate))}}</td>
                <td>
                  <?php 
                  if(($th->payType)==1){
                    echo "<span style='color:green;font-weight:600;'>Cash In</span>";
                  }
                  if(($th->payType)==2){
                    echo "<span style='color:red;font-weight:600;'>Cash Out</span>";
                  } ?>
                </td>
                <td>{{$th->bankName}}</td>
                <td><b>
                  <?php 
                  if(($th->payType)==1){ ?>
                   	{{$th->creditAmount}}
                  <?php }
                  if(($th->payType)==2){ ?>
                    {{$th->debitAmount}}
                  <?php } ?>
                  </b>
                </td>
                <td>
                  <?php 
                  if(($th->paymentMethod)==0){
                    echo "Cash";
                  } 
                  ?>
                </td>
              </tr>
              @endforeach
          </tbody>
	</table>
	@else
		<p style="text-align: center; font-size: 11px;">No data found.</p>
	@endif
</body>
</html>

<!DOCTYPE html>
<html>
<head>
	<title>Dyeing Chart</title>
</head>
<body>
	<style type="text/css">
		@page { sheet-size: A4; }
		.table2{
			border-collapse: collapse;
			width: 100%;
			text-align: center;
		}
		.table2 td,th{
			border:1px solid black;
		}
		.table2 td{
			font-size: 11px;
			padding: 2px 2px;
		}
		.table2 th{
			font-size: 10px;
			padding: 3px 1px;
		}
		.table3{
			border-collapse: collapse;
			text-align: center;
			width: 100%;
			text-decoration: underline;
			font-weight: bold;
			font-size: 11px;
		}
		.table3 td{
			text-decoration: underline;
		}
		.table1{
			text-align: center;
			width: 100%;
		}
		.caddress{
			font-size: 11px;
		}
	</style>
	<table class="table1">
		<tr style="padding-bottom:-5px;">
			<td><h3>{{$companyInfo->companyName}}</h3></td>
		</tr>
		<tr>
			<td class="caddress">Business office: {{$companyInfo->companyBAddress}}</td>
		</tr>
		<tr>
			<td class="caddress">Factory Address: {{$companyInfo->companyFAddress}}</td>
		</tr>
		<tr>
			<td class="caddress"><b>DYEING CHART</b></td>
		</tr>
		<tr>
			<td><hr /></td>
		</tr>
	</table>
	@if(!$finish_parts->isEmpty())
	<table border="1px" class="table2">
          <thead>
            <tr>
              <th width="10%">DATE</th>
              <th width="12%">PI</th>
              <th width="12%">ITEM</th>
              <th width="12%">COLOR</th>
              <th width="10%">TWILL</th>
              <th width="10%">TC</th>
              <th width="12%">DELIVERY FACTORY</th>
              <th width="10%">BILL CODE</th>
              <th width="12%">COMMENTS</th>
            </tr>
          </thead>
          <tbody>
              @foreach($finish_parts as $chart)
              <tr>
                <td>{{date("d.m.Y", strtotime($chart->fDate))}}</td>
                <td>{{$chart->pi}}</td>
                <td>{{$chart->item}}</td>
                <td>{{$chart->color}}</td>
                <td>{{$chart->twill}}</td>
                <td>{{$chart->tc}}</td>
                <td>{{$chart->deliveryFactory}}</td>
                <td>{{$chart->billcode}}</td>
                <td>{{$chart->comment}}</td>
              </tr>
              @endforeach
              
          </tbody>
	</table>
	@else
		<p style="text-align: center; font-size: 11px;">No data found.</p>
	@endif
</body>
</html>

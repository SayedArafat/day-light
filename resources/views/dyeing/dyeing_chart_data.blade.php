@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
     <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-list"></i> DYEING CHART(Finish Part) FROM (<span style="color:blue;">{{date("d-M-Y", strtotime($request->start_date))}}</span>) TO (<span style="color:blue;">{{date("d-M-Y", strtotime($request->end_date))}}</span>) </h2>
        <ul class="nav navbar-right panel_toolbox">
           <li> {!! Form::open(['method'=>'POST','route'=>'finish_part_data']) !!}
                  <input hidden name="start_date" value="{{$request->start_date}}"  />
                  <input hidden name="end_date" value="{{$request->end_date}}" />
                  <button type="submit" name='submit_type' value="pdf" class="btn btn-sm"><i class="fa fa-download"></i> Download as PDF</button>
            {{ Form::close() }}
            </li>
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>DATE</th>
              <th>PI</th>
              <th>ITEM</th>
              <th>COLOR</th>
              <th>TWILL</th>
              <th>TC</th>
              <th>DELIVERY FACTORY</th>
              <th>BILL CODE</th>
              <th>COMMENTS</th>
            </tr>
          </thead>
          <tbody>
              @foreach($finish_parts as $chart)
              <tr>
                <td>{{date("d.m.Y", strtotime($chart->fDate))}}</td>
                <td>{{$chart->pi}}</td>
                <td>{{$chart->item}}</td>
                <td>{{$chart->color}}</td>
                <td>{{$chart->twill}}</td>
                <td>{{$chart->tc}}</td>
                <td>{{$chart->deliveryFactory}}</td>
                <td>{{$chart->billcode}}</td>
                <td>{{$chart->comment}}</td>
              </tr>
              @endforeach
              
          </tbody>
        </table>

      </div>
    </div>

  </div>

</div>


@endsection
@section('extra_scripts')
<script>

  $(document).ready(function() {
    $('#datatable-responsive').DataTable({
      "pageLength": 50
    });

  });
  </script>
  @endsection
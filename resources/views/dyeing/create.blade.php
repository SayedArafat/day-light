@extends('layouts.master')
@section('content')

    <div class="clearfix"></div>

    <div class="row animated fadeIn">
        <div class="col-md-6 col-sm-6 col-xs-12">

            @if(Session::has('message'))
                <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
            @endif
            @if(Session::has('failedMessage'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
            @endif

            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-list"></i> Dyeing Information</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    {!! Form::open(array('route' => 'finishPart.store', 'method' => 'POST')) !!}
                    <div class="form-group">
                        {!! Form::label('pi','PI',['class'=>'control-label']) !!}
                        <div class="option-group">
                        <select name="bcPi_number" class="form-control pi_number_item" data-search="true" required >
                          <option value=""> Select PI</option>
                          @foreach($orderPiList as $data)
                            <option value="{{$data->id}}">{{$data->pi}}</option>
                          @endforeach
                        </select>
                        </div>
                    </div>

                    <div class="form-group">
                      {!! Form::label('Billcode','Billcode',['class'=>'control-label']) !!}
                      <div class="option-group">
                          <select class="form-control pi_items" required  name="billcodeId"  >
                              <option value="">Select PI First</option>
                          </select>
                      </div>
                    </div>

                    <div class="form-group">
                      {!! Form::label('Item','Item',['class'=>'control-label']) !!}
                      <div class="option-group">
                          <select class="form-control billcode_items" required  name="billCodeItemId"  >
                              <option value="">Select PI and Billcode First</option>
                          </select>
                      </div>
                    </div>

                    <div class="form-group">
                        <label for="">Date</label>
                        <input name="fDate" value="{{date('Y-m-d')}}" type="text" class="form-control form-white" id="datepicker2" required />
                    </div>

                    <div class="form-group">
                      <label for="twill">Twill</label>
                      <input  name="twill" type="number" step="any" class="form-control form-white" id="twill" placeholder="Twill" />
                    </div>

                    <div class="form-group">
                      <label for="tc">TC</label>
                      <input  name="tc" type="number" step="any" class="form-control form-white" id="twill" placeholder="TC" />
                    </div>

                    <div class="form-group">
                      <label for="deliveryFactory">Delivery Factory</label>
                      <input name="deliveryFactory" class="form-control form-white" id="deliveryFactory" placeholder="Delivery Factory" />
                    </div>

                    <div class="form-group">
                      <label for="comment">Comment</label>
                      <textarea name="comment" class="form-control form-white" id="comment" placeholder="Comment" ></textarea>
                    </div>


                    <hr>

                    <div class="form-group">
                        <button type="submit" name='submit_type' value="save" class="btn btn-primary"><i class="fa fa-save"></i> Save Information</button>
                        <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Reset</button>
                    </div>

                    {{ Form::close() }}


                </div>
            </div>
        </div>

    </div>
@endsection
@section('extra_scripts')
<script>
    $(document).ready(function() {
        $("select.pi_number_item").change(function () {
            var pin= $(".pi_number_item option:selected").val();
            if(!pin){
                var rr='<select class="form-control pi_items" name="billcode_id">'+
                '<option value="">Select PI First</option>'+

                '</select>';
                $('.pi_items').html(rr);
            }
            else {
                $.ajax({
                    url: 'ajax/pi-billcodes/' + pin,
                    method: 'get',
                    dataType: 'html',
                    success: function (response) {
                        $('.pi_items').html(response);
                    }
                });
            }

        });

        $("select.pi_items").change(function () {
            var pin= $(".pi_items option:selected").val();
            // alert(pin);
            if(!pin){
                var rr='<select class="form-control billcode_items" name="item_id">'+
                '<option value="">Select Billcode First</option>'+

                '</select>';
                $('.billcode_items').html(rr);
            }
            else {
                $.ajax({
                    url: 'ajax/billcode-item/' + pin,
                    method: 'get',
                    dataType: 'html',
                    success: function (response) {
                        $('.billcode_items').html(response);
                    }
                });
            }

        });

        $( "#datepicker2" ).datepicker({
           format:'YYYY-MM-DD',
        });

        $('#datatable-responsive').DataTable({
          "pageLength": 50
        });

  });
  </script>
  @endsection
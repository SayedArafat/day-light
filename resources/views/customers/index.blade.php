@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    @if(Session::has('message'))
    <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
    @endif
    @if(Session::has('failedMessage'))
    <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
    @endif
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-users"></i> Customer List </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a href="{{url('/new_customer')}}"><button  class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> New Customer</button></a></li>
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th width="5%">Serial</th>
              <th>Customer Name</th>
              <th>Customer Phone</th>
              <th>Customer Email</th>
              <th>Customer Address</th>
              <th width="15%">Remarks</th>
              <th width="12%">Action</th>
            </tr>
          </thead>


          <tbody>
          @php $i=0; @endphp
          @foreach($customers as $customer)
            <tr>
              <td>{{++$i}}</td>
              <td>{{$customer->customerName}}</td>
              <td>{{$customer->customerPhone}}</td>
              <td>{{$customer->customerEmail}}</td>
              <td>{{$customer->customerAddress}}</td>
              <td>{{$customer->remarks}}</td>
              <td>
                <a href="{{route('customer.show',$customer->id)}}" title="View Customer Details" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>
                <a href="{{route('customer.edit',$customer->id)}}" title="Edit Customer Details" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target=".delete{{$customer->id}}"><i class="fa fa-trash"></i></button>
                
              </td>
            </tr>

            <div class="modal fade delete{{$customer->id}}" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2"><b>Confirm Delete</b></h4>
                  </div>
                  <div class="modal-body">
                    {!! Form::open(['method'=>'DELETE', 'action'=>['CustomerController@destroy', $customer->id]]) !!}
                      <button type="submit" class="btn btn-danger"><i class="fa fa-check-circle"></i> Confirm</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    {!! Form::close() !!}
                    
                  </div>
                  <div class="modal-footer">
                  </div>

                </div>
              </div>
            </div>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
@endsection
@section('extra_scripts')
  <script>
      $(document).ready(function() {

        $('#datatable-responsive').DataTable({
        	 "pageLength": 50
        });

      });
    </script>
@endsection
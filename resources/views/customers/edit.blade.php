@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">

  <div class="col-md-10 col-sm-10 col-xs-12">
     <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-user"></i> <b>Edit Customer Information</b></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
          {!! Form::open(['method'=>'PATCH', 'action'=>['CustomerController@update', $customers->id]]) !!}
          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="customerName" value="{{$customers->customerName}}" class="form-control form-white" id="customerName" placeholder="Customer Name" required >
          </div>
          <div class="form-group">
            <label for="name">Phone</label>
            <input type="text" name="customerPhone" value="{{$customers->customerPhone}}" class="form-control form-white" id="customerPhone" placeholder="Customer Phone" required >
          </div>
          <div class="form-group">
            <label for="name">Email</label>
            <input type="email" name="customerEmail" value="{{$customers->customerEmail}}" class="form-control form-white" id="customerEmail" placeholder="Customer Email" >
          </div>
          <div class="form-group">
            <label for="customerAddress">Address</label>
            <textarea name="customerAddress" class="form-control form-white" id="customerAddress" placeholder="Customer Address" >{{$customers->customerAddress}}</textarea>
          </div>
          <div class="form-group">
            <label for="remarks">Remarks</label>
            <textarea name="remarks" class="form-control form-white" id="remarks" placeholder="Remarks" >{{$customers->remarks}}</textarea>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Update Information</button>
          </div>
        {{ Form::close() }}
      </div>
    </div>
    <hr>
    
  </div>

</div>

@endsection
@section('extra_scripts')
<script>
  $(document).ready(function() {

    $('#datatable-responsive').DataTable({
      "pageLength": 50
    });

  });
  @endsection
@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">

  <div class="col-md-10 col-sm-10 col-xs-12">
     <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-user"></i> <b>Customer Details</b></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        @foreach($customers as $customer)
        <ul class="to_do">
          <li>Name: <b>{{$customer->customerName}}</b></li>
          <li>Phone: <b>{{$customer->customerPhone}}</b></li>
          <li>Email: <b>{{$customer->customerEmail}}</b></li>
          <li>Address: <b>{{$customer->customerAddress}}</b></li>
          <li>Remarks: <b>{{$customer->remarks}}</b></li>
          <li>Created By: <b>{{$customer->created_by}}</b></li>
        </ul>
        @endforeach
      </div>
    </div>
    <hr>
    <a href="{{url('/customer_list')}}" class="btn btn-primary btn-sm"><i class="fa fa-reply-all"></i> Go Back</a>

  </div>

</div>

@endsection
@section('extra_scripts')
<script>
  $(document).ready(function() {

    $('#datatable-responsive').DataTable({
      "pageLength": 50
    });

  });
  @endsection
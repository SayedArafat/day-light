@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    
    @if(Session::has('message'))
      <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
    @endif
    @if(Session::has('failedMessage'))
       <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
    @endif

    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-money"></i> Receive Loan List </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".add_new"><i class="fa fa-plus-square "></i> Add New </button>
          </li>
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
         
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Serial</th>
              <th>Name/Others</th>
              <th>Details</th>
              <th>Loan Date</th>
              <th>Receive Loan TAKA</th>
              <th>Return TAKA</th>
              <th>Balance</th>
              <th>Created By</th>
              <th>Action</th>
            </tr>
          </thead>


          <tbody>
          @php $i=0; @endphp
          @foreach($loans as $loan)
            <tr>
              <td>{{++$i}}</td>
              <td>{{$loan->name}}</td>
              <td>{{$loan->details}}</td>
              <td>{{$loan->loanDate}}</td>
              <td> @money(\Illuminate\Support\Facades\DB::table('tbreceived_loan_history')->where([['loanId','=',$loan->id],['paymentType','=','1']])->sum('amount'))</td>
              <td>@money(\Illuminate\Support\Facades\DB::table('tbreceived_loan_history')->where([['loanId','=',$loan->id],['paymentType','=','2']])->sum('amount'))</td>
              
              <td>@money((\Illuminate\Support\Facades\DB::table('tbreceived_loan_history')->where([['loanId','=',$loan->id],['paymentType','=','1']])->sum('amount'))-(\Illuminate\Support\Facades\DB::table('tbreceived_loan_history')->where([['loanId','=',$loan->id],['paymentType','=','2']])->sum('amount')))</td>

              <td>{{$loan->created_by}}</td>
              <td>

                <a href="{{route('receiveLoan.show',$loan->id)}}"   title="View Loan Details" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>

                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target=".edit{{$loan->id}}"><i class="fa fa-edit"></i></button>
                
                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target=".delete{{$loan->id}}"><i class="fa fa-trash"></i></button>
              </td>
            </tr>



          <div class="modal fade edit{{$loan->id}}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">

                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel2"><b>Edit Receive Loan Information</b></h4>
                </div>
                <div class="modal-body">
                {!! Form::open(['method'=>'PATCH', 'action'=>['receiveLoanController@update', $loan->id]]) !!}
                  

                  <div class="form-group">
                    <label for="name">Name/Others</label>
                    <input type="text" name="name" class="form-control form-white" id="name" placeholder="Loaner Name/Others" value="{{$loan->name}}" required >
                  </div>

                  <br />

                  <div class="form-group">
                    <label for="details">Details</label>
                    <textarea  name="details" class="form-control form-white" id="details" placeholder="Loan Details" >{{$loan->details}}</textarea>
                  </div>

                  <div class="form-group">
                    <label for="address">Address</label>
                    <textarea  name="address" class="form-control form-white" id="address" placeholder="Address" >{{$loan->address}}</textarea>
                  </div>

                  <div class="form-group">
                    <label for="phone">Phone Number</label>
                    <input type="text" name="phone" class="form-control form-white" id="phone" placeholder="Phone Number" value="{{$loan->phone}}" required >
                  </div>

                  <br />

                  <div class="form-group">
                    <label for="email">Email Address</label>
                    <input type="email" name="email" class="form-control form-white" id="email" placeholder="Email Address" value="{{$loan->email}}" required >
                  </div>

                  <br />

                  <div class="form-group">
                    <label for="loanDate">Loan Date</label>
                    <input type="date" name="loanDate" value="{{$loan->loanDate}}" class="form-control form-white" id="loanDate" required >
                  </div>

                  <br />

                  <div class="form-group">
                    <label for="remarks">Remarks</label>
                    <textarea  name="remarks" class="form-control form-white" id="remarks" placeholder="Remarks" >{{$loan->remarks}}</textarea>
                  </div>
                  <hr>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                  </div>

                {{ Form::close() }}
                 
                </div>
                <div class="modal-footer">
                </div>

              </div>
            </div>
          </div>


            <div class="modal fade delete{{$loan->id}}" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2"><b>Confirm Delete</b></h4>
                  </div>
                  <div class="modal-body">
                    {!! Form::open(['method'=>'DELETE', 'action'=>['receiveLoanController@destroy', $loan->id]]) !!}
                      <button type="submit" class="btn btn-danger"><i class="fa fa-check-circle"></i> Confirm</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    {!! Form::close() !!}
                    
                  </div>
                  <div class="modal-footer">
                  </div>

                </div>
              </div>
            </div>


            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


<div class="modal fade add_new" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2"><b><i class="fa fa-ils"></i> Receive New Loan</b></h4>
      </div>
      <div class="modal-body">
      {!! Form::open(array('action' => 'receiveLoanController@store', 'method' => 'POST')) !!}

        <div class="form-group">
          <label for="name">Name/Others</label>
          <input type="text" name="name" class="form-control form-white" id="name" placeholder="Loaner Name/Others" required >
        </div>

        <br />

        <div class="form-group">
          <label for="details">Details</label>
          <textarea  name="details" class="form-control form-white" id="details" placeholder="Loan Details" ></textarea>
        </div>


        <div class="form-group">
          <label for="phone">Phone Number</label>
          <input type="text" name="phone" class="form-control form-white" id="phone" placeholder="Phone Number" >
        </div>

        <br />

        <div class="form-group">
          <label for="email">Email Address</label>
          <input type="email" name="email" class="form-control form-white" id="email" placeholder="Email Address" >
        </div>

        <br />

        <div class="form-group">
          <label for="address">Address</label>
          <textarea  name="address" class="form-control form-white" id="address" placeholder="Address" ></textarea>
        </div>

        <br />

        <div class="form-group">
          <label for="amount">Loan Amount</label>
          <input type="number" placeholder="Amount" step="any" name="amount" class="form-control form-white" id="amount" required >
        </div>

        <br />

        <div class="form-group">
          <label for="loanDate">Loan Date</label>
          <input type="date" name="loanDate" class="form-control form-white" id="loanDate" required >
        </div>

        <br />

        <div class="form-group">
          <label for="remarks">Remarks</label>
          <textarea name="remarks" class="form-control form-white" id="remarks" placeholder="Remarks" ></textarea>
        </div>


        <hr>

        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
        </div>

        {{ Form::close() }}

      </div>
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>


@endsection
@section('extra_scripts')
  <script>
      $(document).ready(function() {

        $('#datatable-responsive').DataTable({
        	 "pageLength": 50
        });

      });
      setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
@endsection
@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-5 col-sm-5 col-xs-12">
     <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-user"></i> <b>Receive Loaner Details</b></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        @foreach($loans as $loan)
        <ul class="to_do">
          <li>Name/Others: <b>{{$loan->name}}</b></li>
          <li>Phone: <b>{{$loan->phone}}</b></li>
          <li>Email: <b>{{$loan->email}}</b></li>
          <li>Address: <b>{{$loan->address}}</b></li>
          <li>Loan Details: <b>{{$loan->details}}</b></li>
          <li>Remarks: <b>{{$loan->remarks}}</b></li>
          <li>Loan Date: <b><?php echo date("d-M-Y", strtotime($loan->loanDate)); ?></b></li>
          <li>Loan Type: <b>Received</b></li>
        </ul>
        @endforeach
      </div>
    </div>
  </div>

  <div class="col-md-5 col-sm-5 col-xs-12">
     <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-cubes"></i> <b>Actions</b></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        
        <a class="btn btn-danger btn-sm"  data-toggle="modal" data-target=".receive_new_payment"><i class="fa fa-money"></i> Receive New Payment</a>
        <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".receive_return_payment"><i class="fa fa-money"></i> Return Payment</a>

        <hr>

        <ul class="to_do"> 
          <li style="color:blue;"><b>Total Loan Amount :  @money($paymentAmount)</b></li>
          <li style="color:green;"><b>Total Return Amount : @money($paymentReturnAmount)</b></li>
          <li style="color:red;"><b>Balance Amount : @money($paymentAmount-$paymentReturnAmount)</b></li>
        </ul>

      </div>
    </div>
  </div>

</div>

<div class="row">
<div class="col-md-6 col-sm-6 col-xs-12">
   <div class="x_panel">
    <div class="x_title">
      <h2><i class="fa fa-money"></i> <b>Receive Amount Details</b></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table id="datatable-responsive" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th width="5%">Serial</th>
            <th>Payment Date</th>
            <th>Amount</th>
            <th>Remarks</th>
            <th>Created By</th>
          </tr>
        </thead>
        <tbody>
          @php $i=0; @endphp
          @foreach($loanAmounts as $loanAmount)
          <tr>
            <td>{{++$i}}</td>
            <td><?php echo date("d-M-Y", strtotime($loanAmount->paymentDate)); ?></td>
            <td>@money($loanAmount->amount)</td>
            <td>{{$loanAmount->remarks}}</td>
            <td>{{$loanAmount->created_by}}</td>
          </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th colspan="2" style="text-align:right;">Total</th>
            <th>@money($paymentAmount)</th>
            <th colspan="2"></th>
          </tr>
        </tfoot>
      </table>

    </div>
  </div>

</div>

<div class="col-md-6 col-sm-6 col-xs-12">
 <div class="x_panel">
  <div class="x_title">
    <h2><i class="fa fa-money"></i> <b>Return Amount Details</b></h2>
    <ul class="nav navbar-right panel_toolbox">
      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
    </ul>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <table id="datatable-responsive" class="table table-striped table-bordered">
      <thead>
        <tr>
          <th width="5%">Serial</th>
          <th>Payment Date</th>
          <th>Amount</th>
          <th>Remarks</th>
          <th>Created By</th>
        </tr>
      </thead>
      <tbody>
        @php $i=0; @endphp
        @foreach($loanReturnAmounts as $loanReturnAmount)
        <tr>
          <td>{{++$i}}</td>
          <td><?php echo date("d-M-Y", strtotime($loanReturnAmount->paymentDate)); ?></td>
          <td>@money($loanReturnAmount->amount)</td>
          <td>{{$loanReturnAmount->remarks}}</td>
          <td>{{$loanReturnAmount->created_by}}</td>
        </tr>
        @endforeach
      </tbody>
        <tfoot>
          <tr>
            <th colspan="2" style="text-align:right;">Total</th>
            <th>@money($paymentReturnAmount)</th>
            <th colspan="2"></th>
          </tr>
        </tfoot>
    </table>

  </div>
</div>

</div>

</div>

<div class="modal fade receive_return_payment" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2"><b><i class="fa fa-ils"></i> Return Amount</b></h4>
      </div>
      <div class="modal-body">
      {!! Form::open(array('url' => 'receive_return_loan_payment', 'method' => 'POST')) !!}

        <div class="form-group">
          <label for="amount">Return Amount</label>
          <input name="loanId" hidden value="{{$loanId}}" >
          <input type="number" step="any" name="amount" class="form-control form-white" id="amount" placeholder="Return Amount " required >
        </div>

        <br />

        <div class="form-group">
          <label for="loanDate">Return Date</label>
          <input type="date" name="loanDate" class="form-control form-white" id="loanDate" required placeholder="Return date" >
        </div>

        <br />

        <div class="form-group">
          <label for="remarks">Remarks</label>
          <textarea name="remarks" class="form-control form-white" id="remarks" placeholder="Remarks" ></textarea>
        </div>


        <hr>

        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
        </div>

        {{ Form::close() }}

      </div>
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>

<div class="modal fade receive_new_payment" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2"><b><i class="fa fa-ils"></i> Receive New Payment</b></h4>
      </div>
      <div class="modal-body">
      {!! Form::open(array('url' => 'receive_new_loan_payment', 'method' => 'POST')) !!}

        <div class="form-group">
          <label for="amount">Loan Amount</label>
          <input name="loanId" hidden value="{{$loanId}}" >
          <input type="number" step="any" name="amount" class="form-control form-white" id="amount" placeholder="Loan Amount " required >
        </div>

        <br />

        <div class="form-group">
          <label for="loanDate">Loan Date</label>
          <input type="date" name="loanDate" class="form-control form-white" id="loanDate" required >
        </div>

        <br />

        <div class="form-group">
          <label for="remarks">Remarks</label>
          <textarea name="remarks" class="form-control form-white" id="remarks" placeholder="Remarks" ></textarea>
        </div>


        <hr>

        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
        </div>

        {{ Form::close() }}

      </div>
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>


@endsection
@section('extra_scripts')
<script>
  $(document).ready(function() {

    $('#datatable-responsive').DataTable({
      "pageLength": 50
    });

  });
  @endsection
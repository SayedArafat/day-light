@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @if(Session::has('message'))
                <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
            @endif
            @if(Session::has('failedMessage'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
            @endif
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-users"></i>Today's Income List</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="datatable-responsive" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th width="5%">Serial</th>
                            <th>Category</th>
                            <th>Payment Type</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Created By</th>
                            <th width="12%">Action</th>
                        </tr>
                        </thead>

                        <tbody>
                        @php $i=0; @endphp
                        @foreach($income_sources as $income)
                            <tr>
                                <td>{{++$i}}</td>
                                <td>{{$income->categoryName}}</td>
                                <td>{{$income->payment_method}}</td>
                                <td>{{$income->amount}}</td>
                                <td>{{$income->incomeDate}}</td>
                                <td>{{$income->created_by}}</td>
                                <td>
                                    <a class="show_view_modal" href="{{route('incomes.show',$income->id)}}"><button type="button" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button></a>
                                    <a class="show_edit_modal" href="{{route('incomes.edit',$income->id)}}"> <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button></a>
                                    <a class="show_delete_modal" href="{{route('incomes.delete', $income->id)}}"> <button type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button></a>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        {{--View Model--}}

        <div id="viewModal" class="modal fade" role="dialog">

            <div class="modal-dialog modal-sm">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Income Details</h4>
                    </div>
                    <div class="view-modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        {{--View Model End--}}


        {{-- Edit Model--}}

        <div id="editModal" class="modal fade" role="dialog">

            <div class="modal-dialog modal-sm">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Income Details</h4>
                    </div>
                    <div class="edit-modal-body">
                    </div>
                </div>

            </div>
        </div>
        {{--Edit Model End--}}

        {{-- Delete Modal--}}

        <div id="deleteModal" class="modal fade" role="dialog">

            <div class="modal-dialog modal-sm">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete Income Details</h4>
                    </div>
                    <div class="delete-modal-body">
                    </div>
                </div>

            </div>
        </div>
        {{--Delete Modal End--}}

    </div>
@endsection
@section('extra_scripts')
    <script>
        $(document).ready(function() {

            $('.show_delete_modal').click(function (event) {
                event.preventDefault();
                var url=$(this).attr('href');
//              alert(url);
                $.ajax({
                    url:url,
                    method:"get",
                    success:function(response){
                        $('.delete-modal-body').html(response);

                    }
                });
//              alert(url);
                $('#deleteModal').modal();


            });

            setTimeout(function() {
                $('#alert_message').fadeOut('fast');
            }, 5000);

            $('.show_view_modal').click(function (event) {
                event.preventDefault();
                var url=$(this).attr('href');
//              alert(url);
                $.ajax({
                    url:url,
                    method:'GET',
                    success:function(response){
                        $('.view-modal-body').html(response);
                    }


                });
                $('#viewModal').modal();

            });
            $('.show_edit_modal').click(function (event) {
                event.preventDefault();
                var url=$(this).attr('href');
                $.ajax({
                    url:url,
                    method:"get",
                    success:function (response) {
                        $('.edit-modal-body').html(response);

                    }
                });
                $('#editModal').modal();

            });

            $('#datatable-responsive').DataTable({
                "pageLength": 50
            });

        });
    </script>
@endsection
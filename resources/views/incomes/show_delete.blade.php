<style>
    .form-group{
        padding: 13px;
    }
</style>

{!! Form::open(['method'=>'DELETE', 'action'=>['IncomeSourceController@destroy', $income->id]]) !!}
    <div class="form-group">
        {!! Form::label("Are you sure you want to delete ".$income->pi."? ") !!}

    </div>

<div class="modal-footer">
    <button type="submit" class="btn btn-danger"><i class="fa fa-check-circle"></i> Confirm</button>
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
    {!! Form::close() !!}
</div>
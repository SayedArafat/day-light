@extends('layouts.master')
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">

    @if(Session::has('message'))
    <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
    @endif
    @if(Session::has('failedMessage'))
    <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
    @endif

    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-money"></i> Add New Income Information </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        {!! Form::open(['method'=>'POST','action'=>'IncomeSourceController@store', 'files'=>true]) !!}
      
        <div class="form-group">
          <label for="categoryId">Category</label>
          <select name="categoryId" id="categoryId" required class="form-control">
            <option selected value="">Select Category</option>
            @foreach($incomeCategories as $category)
            <option value="{{$category->id}}">{{$category->categoryName}}</option>
            @endforeach
          </select>
        </div>


        <br />

        <div class="form-group">
          <label for="description">Description</label>
          <textarea name="description" class="form-control form-white" id="description" placeholder="Description" ></textarea>
        </div>

        <br />

        <div class="form-group">
          <label for="title">Payment Method</label>
          <select name="payment_type" id="payment_type" onchange="payment_type_func(this)" required class="form-control">
            <option selected value="Cash">Cash</option>
            <option value="Bank">Bank</option>
          </select>
        </div>

        <br />

        <div class="form-group" id="cash_form">
          <label for="source">Source</label>
          <input name="source" placeholder="Source" id="source" required class="form-control" >
        </div>
        <br />

        <div class="form-group" id="bank_account_form" style="display: none">
          <label for="bank_account">Bank Account</label>
          <select name="bank_account" id="bank_account" class="form-control selectpicker" data-live-search="true">
            <option selected value="">Select Bank Account</option>
            @foreach($banks as $bank)
              <option value="{{$bank->id}}">{{$bank->bankName." (".$bank->bankAccountNo.")"}}</option>
            @endforeach
          </select>
        </div>
        <br />

        <div class="form-group">
          <label for="amount"> Amount</label>
          <input name="amount" class="form-control form-white" id="amount" type="number" step="any" placeholder="Amount" required />
        </div>

        <br />

        <div class="form-group">
          <label for="incomeAttachment">Attachment</label>
          <input name="incomeAttachment" type="file" class="form-control form-white" id="incomeAttachment" />
        </div>

        <br />

        <div class="form-group">
          <label for="incomeDate">Date</label>
          <input name="incomeDate"  type="date" class="form-control form-white" id="incomeDate" required />
        </div>

        <br />
        <div class="form-group">
          <label for="title">Reference</label>
          <input type="text" name="reference" class="form-control form-white" id="title" placeholder="Reference">
        </div>

        <br />

        <hr>

        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save Income Information</button>
          <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Reset</button>
        </div>

        {{ Form::close() }}


      </div>
    </div>
  </div>

</div>
@endsection
@section('extra_scripts')
<script>
  function payment_type_func(that) {
      if(that.value==="Bank") {
//          alert("kk");
          document.getElementById('bank_account_form').style.display='block';
          document.getElementById('cash_form').style.display='none';
      }
      else {
          document.getElementById('bank_account_form').style.display='none';
          document.getElementById('cash_form').style.display='block';

      }

  }
  setTimeout(function() {
    $('#alert_message').fadeOut('fast');
  }, 5000);
</script>
@endsection
<style>
    .form-group{
        padding: 13px;
    }
</style>

<div class="form-group">
    <label for="categoryId">Category</label>
    <select name="categoryId" id="categoryId" readonly required class="form-control" >
        <option selected value="">Select Category</option>
        @foreach($incomeCategories as $category)
            <option <?php if($income_details->categoryId==$category->id){ echo "selected"; } ?> value="{{$category->id}}">{{$category->categoryName}}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="amount">Income Amount</label>
    <input name="amount" readonly value="{{$income_details->amount}}" class="form-control form-white" id="amount" type="number" step="any" placeholder="Income Amount" required />
</div>

<div class="form-group">
    <label for="payment_method">Payment Type</label>
    <input readonly type="text" value="{{$income_details->payment_method}}" name="payment_method" class="form-control form-white" id="reference" placeholder="Payment method " >
</div>

<div class="form-group">
    <label for="payment_method">Account</label>
    @if($income_details->bankName!=null)
        <input readonly type="text" value="{{$income_details->bankName." (".$income_details->bankAccountNo.")"}}" name="payment_method" class="form-control form-white" id="reference" placeholder="Payment method" >
    @else
        <input readonly type="text" name="payment_method" class="form-control form-white" id="payment_method" >
    @endif
</div>



<div class="form-group">
    <label for="expenseAttachment">Attachment</label>
    <br />
    <?php if(!empty($income_details->attachement)){ ?>

    <a class="btn btn-primary" target="_NEW" href="income_attachment/{{$income_details->attachement}}"><i class="fa fa-download"></i> Download Attachment</a>
    <?php }else{
        echo "No Attachment Found.";
    } ?>
</div>


<div class="form-group">
    <label for="description">Income Description</label>
    <textarea readonly rows="5" name="description" class="form-control form-white" id="description" placeholder="Income Description" >{{$income_details->description}}</textarea>
</div>


<div class="form-group">
    <label for="expenseDate">Income Date</label>
    <input readonly name="expenseDate" value="{{$income_details->incomeDate}}"  class="form-control form-white" id="expenseDate" required />
</div>

<div class="form-group">
    <label for="created_by">Created By</label>
    <input readonly type="text" value="{{$income_details->created_by}}" name="created_by" class="form-control form-white" id="created_by" placeholder="Created By" >
</div>

<div class="form-group">
    <label for="reference">Reference</label>
    <input readonly type="text" value="{{$income_details->reference}}" name="reference" class="form-control form-white" id="reference" placeholder="Expense Reference " >
</div>

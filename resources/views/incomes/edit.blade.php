<style>
    .form-group{
        padding: 13px;
    }
</style>

{!! Form::open(['method'=>'PATCH','action'=>['IncomeSourceController@update',$income_details->id],'files'=>true]) !!}
<div class="form-group">
    <label for="categoryId">Category</label>
    <select name="categoryId" id="categoryId" required class="form-control" >
        <option selected value="">Select Category</option>
        @foreach($incomeCategories as $category)
            <option <?php if($income_details->categoryId==$category->id){ echo "selected"; } ?> value="{{$category->id}}">{{$category->categoryName}}</option>
        @endforeach
    </select>
</div>


<div class="form-group">
    <label for="amount">Income Amount</label>
    <input name="amount" value="{{$income_details->amount}}" class="form-control form-white" id="amount" type="number" step="any" placeholder="Income Amount" required />
</div>

<div class="form-group">
    <label for="payment_method">Payment Type</label>
    <select name="payment_type" id="payment_type" required class="form-control">
        <option <?php if($income_details->payment_method=="Cash") { echo "selected"; } ?> value="Cash">Cash</option>
        <option <?php if($income_details->payment_method=="Bank") { echo "selected"; } ?>  value="Bank">Bank</option>
    </select>
</div>

<div class="form-group">
    <label for="bank_account">Account</label>
    <select name="bank_account" id="bank_account" class="form-control selectpicker" data-live-search="true">
        <option selected value="">Select Bank Account</option>
        @foreach($banks as $bank)
            <option <?php if($income_details->bank_account==$bank->id){ echo "selected";} ?> value="{{$bank->id}}">{{$bank->bankName." (".$bank->bankAccountNo.")"}}</option>
        @endforeach
    </select>
</div>



<div class="form-group">
    <label for="expenseAttachment">Attachment</label>
    <br />
    <input name="incomeAttachment" type="file" class="form-control form-white" id="incomeAttachment" /><br />


    <?php if(!empty($income_details->attachement)){ ?>

    <a class="btn btn-primary" target="_NEW" href="income_attachment/{{$income_details->attachement}}"><i class="fa fa-download"></i> Download Attachment</a>
    <?php }else{
        echo "No Attachment Found.";
    } ?>
</div>


<div class="form-group">
    <label for="description">Income Description</label>
    <textarea rows="5" name="description" class="form-control form-white" id="description" placeholder="Income Description" >{{$income_details->description}}</textarea>
</div>


<div class="form-group">
    <label for="expenseDate">Income Date</label>
    <input name="incomeDate" value="{{$income_details->incomeDate}}"  class="form-control form-white" id="incomeDate" required />
</div>

<div class="form-group">
    <label for="reference">Reference</label>
    <input type="text" value="{{$income_details->reference}}" name="reference" class="form-control form-white" id="reference" placeholder="Expense Reference " >
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-default">Update</button>
</div>
{!! Form::close() !!}

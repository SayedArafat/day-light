@extends('layouts.master')
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">

    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-plus-square"></i> Add New Order Information </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        {!! Form::open(['method'=>'POST','action'=>'orderController@store']) !!}
      
        <div class="form-group">
          <label for="pi">PI</label>
          <input type="text" autofocus name="pi" class="form-control form-white" id="pi" placeholder="PI Number " required >
        </div>

        <br />

        <div class="form-group">
          <label for="customerId">Customer/Buyer</label>
          <select name="customerId" id="customerId" required class="form-control" >
            <option selected value="">Select</option>
            @foreach($customers as $customer)
            <option value="{{$customer->id}}">{{$customer->customerName}}</option>
            @endforeach
          </select>
        </div>

        <br />

        <div class="form-group">
          <label for="orderDate">Order Date</label>
          <input name="orderDate" id="datepicker1" value="{{date('Y-m-d')}}" class="form-control form-white" id="orderDate" required />
        </div>

        <br />

        <div class="form-group">
          <label for="deliveryDate">Approx: Delivary Date</label>
          <input name="deliveryDate"  type="text" id="datepicker2" value="{{date('Y-m-d')}}" class="form-control form-white" id="deliveryDate" />
        </div>

        <br />

        <div class="form-group">
          <label for="goodsOrigin">Goods Origin</label>
          <input name="goodsOrigin" placeholder="Origin of goods" type="text" class="form-control form-white" id="goodsOrigin" />
        </div>

        <br />
        
        <div class="form-group">
          <label for="remarks">Remarks</label>
          <textarea name="remarks" class="form-control form-white" id="remarks" placeholder="Description" ></textarea>
        </div>

        <hr>

        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save Order</button>
          <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Reset</button>
        </div>

        {{ Form::close() }}


      </div>
    </div>
  </div>

</div>
@endsection
@section('extra_scripts')

<script>
  $( function() {
    $( "#datepicker1" ).datepicker({
       format:'YYYY-MM-DD',
    });
    $( "#datepicker2" ).datepicker({
       format:'YYYY-MM-DD',
    });
  });
  setTimeout(function() {
    $('#alert_message').fadeOut('fast');
  }, 5000);
</script>
@endsection
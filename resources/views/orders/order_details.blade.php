@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12">
    @if(Session::has('message'))
          <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
          @endif
          @if(Session::has('failedMessage'))
          <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
          @endif
  </div>
  <div class="col-md-7 col-sm-7 col-md-offset-1 col-sm-offset-1 col-xs-12">
     <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-user"></i> <b>Order Details</b></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        @foreach($orders as $order)
        @php $id = $order->id;@endphp
        <ul class="to_do">
          <li><h3 style='color:red;'> <b>PI: {{$order->pi}}</b></h3></li>
          <li>Customer Name: <b>{{$order->customerName}}</b></li>
          <li>Customer Phone: <b>{{$order->customerPhone}}</b></li>
          <li>Customer Email: <b>{{$order->customerEmail}}</b></li>
          <li>Customer Address: <b>{{$order->customerAddress}}</b></li>
          <li>Customer Remarks: <b>{{$order->remarks}}</b></li>
          <li>Order Date: <b><?php echo date("d-M-Y", strtotime($order->orderDate)); ?></b></li>
          <li>Order Status: <b><?php
              if(($order->status)==1){
                echo "<span style='color:green;'>Completed</span>";
              }elseif(($order->status)==0){
                echo "<span style='color:blue;'>Pending</span>";
              }else{
                echo "<span style='color:red;'>Rejected</span>";
              }

              ?>
              </b>
            <a class="btn-danger btn-xs animated zoomIn" style="cursor:pointer;" data-toggle="modal" data-target=".change_status"><i class="fa fa-reload"></i> Change Status</a>
              </li>
        </ul>
        @php $st=$order->status; @endphp
        @endforeach
        <hr>
        <?php if($st==0){ ?>  
        <a class="btn btn-primary btn-md animated zoomIn" data-toggle="modal" data-target=".add_new_item"><i class="fa fa-plus"></i> Add New Item</a>
        <?php } ?>

        <a href="{{route('order.order_details_pdf',$id)}}"  class="btn btn-default btn-md  animated zoomIn"><i class="fa fa-download"></i> Save as PDF </a>

      </div>
    </div>
  </div>


</div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
   <div class="x_panel">
    <div class="x_title">
      <h2><i class="fa fa-th"></i> <b>Item List</b></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table id="datatable-responsive" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th width="5%">Serial</th>
            <th>Description of the goods</th>
            <th>Style</th>
            <th>Colour</th>
            <th>Item Quantity</th>
            <th>Unit Price</th>
            <th>Total Amount</th>
            <th>Action</th>
          </tr>
        </thead>
          <tbody>
            @php $i=0;$total=0; @endphp
            @if(!empty($orderItems))
            @foreach($orderItems as $item)
            <tr>
              <td>{{++$i}}</td>
              <td>{{$item->details}}</td>
              <td>{{$item->style}}</td>
              <td>{{$item->color}}</td>
              <td>@quantity($item->orderQuantity)</td>
              <td>@dollarMoney($item->unitPrice)</td>
              <td>@dollarMoney(($item->orderQuantity*$item->unitPrice))</td>
              <td>
                <a class="show_edit_modal" href="{{route('orders.edit',$item->id)}}"> <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button></a>

              </td>
            </tr>
            <?php $total += $item->orderQuantity*$item->unitPrice; ?>
            @endforeach
            @else
              <tr>
                <td colspan="9"> No data found.</td>
              </tr>
            @endif
          </tbody>
          <tfoot>
            <tr>
              <th colspan="7" style="text-align:right;">Total</th>
              <th colspan="2">@dollarMoney($total)</th>
            </tr>
          </tfoot>
        
      </table>

    </div>
  </div>

  </div>
</div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
   <div class="x_panel">
    <div class="x_title">
      <h2><i class="fa fa-th"></i> <b>Greige Chart</b></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table id="datatable-responsive1" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th width="5%">Serial</th>
            <th>Description Of The Goods</th>
            <th>PI</th>
            <th>Colour</th>
            <th>Booking QTY</th>
            <th>QTY</th>
            <th>Unit Price (TK)</th>
            <th>Total Amount(TK)</th>
            <th>From</th>
            <th>To</th>
            <th>Remarks</th>
            <th>Bill Code</th>
          </tr>
        </thead>
          <tbody>
            @php $i=0;$total=0; @endphp
            @forelse($billcodeitems as $bci)
              <tr>
                <td>{{++$i}}</td>
                <td>{{$bci->details}}</td>
                <td>{{$bci->pi}}</td>
                <td>{{$bci->color}}</td>
                <td>{{$bci->bookingQuantity}}</td>
                <td>{{$bci->bcQuantity}}</td>
                <td>@money($bci->bcUnitPrice)</td>
                <td>@money(($bci->bcQuantity)*($bci->bcUnitPrice))</td>
                <td>{{($bci->bcFrom)}}</td>
                <td>{{($bci->bcTo)}}</td>
                <td>{{($bci->bcRemarks)}}</td>
                <td>{{($bci->billcode)}}</td>
              </tr>
            @empty
              <tr>
                <td colspan="7"> No data found.</td>
              </tr>
            @endforelse
          </tbody>
      </table>

    </div>
  </div>

  </div>
</div>


<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
   <div class="x_panel">
    <div class="x_title">
      <h2><i class="fa fa-th"></i> <b>Finish Part</b></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table id="datatable-responsive2" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>DATE</th>
              <th>PI</th>
              <th>ITEM</th>
              <th>COLOR</th>
              <th>TWILL</th>
              <th>TC</th>
              <th>DELIVERY FACTORY</th>
              <th>COMMENTS</th>
              <th>BILL CODE</th>
            </tr>
          </thead>
          <tbody>
              @foreach($finish_parts as $chart)
              <tr>
                <td>{{date("d.m.Y", strtotime($chart->fDate))}}</td>
                <td>{{$chart->pi}}</td>
                <td>{{$chart->item}}</td>
                <td>{{$chart->color}}</td>
                <td>{{$chart->twill}}</td>
                <td>{{$chart->tc}}</td>
                <td>{{$chart->deliveryFactory}}</td>
                <td>{{$chart->comment}}</td>
                <td>{{$chart->billcode}}</td>
              </tr>
              @endforeach
              
          </tbody>
        </table>

    </div>
  </div>

  </div>
</div>


<div class="modal fade add_new_item" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2"><b><i class="fa fa-list"></i> New Item Information</b></h4>
      </div>
      <div class="modal-body">
      {!! Form::open(array('url' => 'add_item_to_order', 'method' => 'POST')) !!}

        <div class="form-group">
          <label for="details">Description of the goods</label>
          <input name="orderId" hidden value="{{$id}}" >
          <textarea name="details" class="form-control form-white" id="details" placeholder="Description of the goods" ></textarea>
        </div>

        <div class="form-group">
          <label for="style">Style</label>
          <input type="text" name="style" class="form-control form-white" id="style" placeholder="Style" required >
        </div>

        <div class="form-group">
          <label for="item">Item Name</label>
          <input type="text" name="item" class="form-control form-white" id="item" placeholder="Item (for short identification)" >
        </div>

        <div class="form-group">
          <label for="color">Color</label>
          <input type="text" name="color" class="form-control form-white" id="color" required placeholder="Colour" >
        </div>
        
        <div class="form-group">
          <label for="itemQuantity">Item Quantity</label>
          <input type="number" step="any" name="itemQuantity" class="form-control form-white" id="itemQuantity" required placeholder="Item Quantity" >
        </div>

        <div class="form-group">
          <label for="unitPrice">Unit Price</label>
          <input type="number" step="any" name="unitPrice" class="form-control form-white" id="unitPrice" required placeholder="Unit Price ($)" >
        </div>

        <hr>

        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
        </div>

        {{ Form::close() }}

      </div>
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>


<div class="modal fade change_status" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2"><b><i class="fa fa-list"></i> New Item Information</b></h4>
      </div>
      <div class="modal-body">
      {!! Form::open(array('url' => 'change_order_status', 'method' => 'POST')) !!}

        <input value="{{$id}}" name="orderId" hidden >
        <div class="form-group">
          <label for="status">Status</label>
          <select name="status" required class="form-control form-white" id="status" >
            <option value=""> Select</option>
            <option value="0" <?php if($st==0){echo "selected";} ?>> Pending</option>
            <option value="1" <?php if($st==1){echo "selected";} ?>> Completed</option>
            <option value="2" <?php if($st==2){echo "selected";} ?>> Rejected</option>
          </select>
        </div>
        <br />

        <hr>

        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
        </div>

        {{ Form::close() }}

      </div>
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>





{{-- Edit Model--}}

<div id="editModal" class="modal fade" role="dialog">

  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Order</h4>
      </div>
      <div class="edit-modal-body">
      </div>
    </div>

  </div>
</div>
{{--Edit Model End--}}

@endsection
@section('extra_scripts')
<script>
  $(document).ready(function() {
      $('.show_edit_modal').click(function (event) {
          event.preventDefault();
          var url=$(this).attr('href');
          $.ajax({
              url:url,
              method:'get',
              success:function (response) {
                  $('.edit-modal-body').html(response);

              }

          });
          $('#editModal').modal();
//          alert(url);

      });

    $('#datatable-responsive').DataTable({
      "pageLength": 50
    });

    $('#datatable-responsive1').DataTable({
      "pageLength": 50
    });

    $('#datatable-responsive2').DataTable({
      "pageLength": 50
    });

      
  });
  setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
</script>

  @endsection
@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    @if(Session::has('message'))
    <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
    @endif
    @if(Session::has('failedMessage'))
    <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
    @endif
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-cubes"></i> Order List </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th width="5%">Serial</th>
              <th>PI</th>
              <th>Customer Name</th>
              <th>Customer Phone</th>
              <th>Customer Email</th>
              <th>Order Date</th>
              <th>Status</th>
              <th>Created By</th>
              <th width="12%">Action</th>
            </tr>
          </thead>
          <tbody>
          @php $i=0; @endphp
          @foreach($orders as $order)
            <tr>
              <td>{{++$i}}</td>
              <td>{{$order->pi}}</td>
              <td>{{$order->customerName}}</td>
              <td>{{$order->customerPhone}}</td>
              <td>{{$order->customerEmail}}</td>
              <td>{{$order->orderDate}}</td>
              <td>
              <?php
                if(($order->status)==1){
                  echo "<span style='color:green;'>Completed</span>";
                }elseif(($order->status)==0){
                  echo "<span style='color:blue;'>Pending</span>";
                }elseif(($order->status)==2){
                  echo "<span style='color:red;'>Rejected</span>";
                }else{
                  echo "Undefined";
                }
              ?>
              </td>
              <td>{{$order->created_by}}</td>
              <td>
                <a href="{{route('orders.show',$order->id)}}"   title="View Order Details" class="btn btn-primary btn-sm  animated fadeIn"><i class="fa fa-eye"></i></a>
                <a href="{{route('order.order_details_pdf',$order->id)}}" title="Download as PDF" class="btn btn-success btn-sm animated fadeIn"><i class="fa fa-file-pdf-o"></i></a>
              </td>
            </tr>

            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
@endsection
@section('extra_scripts')
  <script>
      $(document).ready(function() {

        $('#datatable-responsive').DataTable({
        	 "pageLength": 50
        });
      setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);

      });
  </script>
@endsection
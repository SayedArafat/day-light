<style>
    .form-group{
        padding: 0 13px;
    }
</style>

{!! Form::open(['method'=>'PATCH','action'=>['orderController@update',$order->id]]) !!}
<br/>
<div class="form-group">
    <label for="details">Description of the goods</label>
    <textarea name="details" class="form-control form-white" id="details" placeholder="Description of the goods" >{{$order->details}}</textarea>
</div>

<div class="form-group">
    <label for="style">Style</label>
    <input type="text" name="style" value="{{$order->style}}" class="form-control form-white" id="style" placeholder="Style" required >
</div>

{{--<div class="form-group">--}}
    {{--<label for="item">Item Name</label>--}}
    {{--<input type="text" name="item" class="form-control form-white" id="item" placeholder="Item (for short identification)" >--}}
{{--</div>--}}

<div class="form-group">
    <label for="color">Color</label>
    <input type="text" value="{{$order->color}}" name="color" class="form-control form-white" id="color" required placeholder="Colour" >
</div>


<div class="form-group">
    <label for="orderQuantity">Item Quantity</label>
    <input type="number" step="any" value="{{$order->orderQuantity}}" name="orderQuantity" class="form-control form-white" id="orderQuantity" required placeholder="Item Quantity" >
</div>


<div class="form-group">
    <label for="unitPrice">Unit Price</label>
    <input type="number" step="any" value="{{$order->unitPrice}}" name="unitPrice" class="form-control form-white" id="unitPrice" required placeholder="Unit Price ($)" >
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-default">Update</button>
</div>
{!! Form::close() !!}

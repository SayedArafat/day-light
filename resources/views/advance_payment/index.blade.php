@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    
    @if(Session::has('message'))
      <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
    @endif
    @if(Session::has('failedMessage'))
       <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
    @endif

    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-money"></i> Advance Payment List </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".add_new"><i class="fa fa-plus-square "></i> Add New </button>
          </li>
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
         
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table id="datatable-responsive" class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Serial</th>
              <th>Name/Others</th>
              <th>Remarks</th>
              <th>Payment Date</th>
              <th>Advance Amount</th>
              <th>Expense Amount</th>
              <th>Balance Amount</th>
              <th>Created By</th>
              <th>Action</th>
            </tr>
          </thead>


          <tbody>
          @php $i=0; @endphp
          @foreach($advance_payment as $data)
            <tr>
              <td>{{++$i}}</td>
              <td>{{$data->employeeName}}</td>
              <td>{{$data->remarks}}</td>
              <td>{{$data->paymentDate}}</td>
              <td> @money(\Illuminate\Support\Facades\DB::table('tbadvance_payment_history')->where([['advancePaymentId','=',$data->id],['paymentType','=','1']])->sum('amount'))</td>
              <td>@money(\Illuminate\Support\Facades\DB::table('tbadvance_payment_history')->where([['advancePaymentId','=',$data->id],['paymentType','=','2']])->sum('amount'))</td>
              
              <td>@money((\Illuminate\Support\Facades\DB::table('tbadvance_payment_history')->where([['advancePaymentId','=',$data->id],['paymentType','=','1']])->sum('amount'))-(\Illuminate\Support\Facades\DB::table('tbadvance_payment_history')->where([['advancePaymentId','=',$data->id],['paymentType','=','2']])->sum('amount')))</td>

              <td>{{$data->created_by}}</td>
              <td>
                <a href="{{route('advancePayment.show',$data->id)}}"   title="View Details" class="btn btn-default btn-sm"><i class="fa fa-eye"></i></a>

                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target=".delete{{$data->id}}"><i class="fa fa-trash"></i></button>

              </td>
            </tr>


            <div class="modal fade delete{{$data->id}}" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">

                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2"><b>Confirm Delete</b></h4>
                  </div>
                  <div class="modal-body">
                    {!! Form::open(['method'=>'DELETE', 'action'=>['advancePaymentController@destroy', $data->id]]) !!}
                      <button type="submit" class="btn btn-danger"><i class="fa fa-check-circle"></i> Confirm</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    {!! Form::close() !!}
                    
                  </div>
                  <div class="modal-footer">
                  </div>

                </div>
              </div>
            </div>


            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


<div class="modal fade add_new" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2"><b><i class="fa fa-ils"></i> Give New Advance Payment</b></h4>
      </div>
      <div class="modal-body">
      {!! Form::open(array('action' => 'advancePaymentController@store', 'method' => 'POST')) !!}

        <div class="form-group">
          <label for="employeeId">Employee</label>
          <select name="employeeId" id="employeeId" required class="form-control" >
            <option selected value="">Select Employee</option>
            @foreach($employees as $employee)
              <option value="{{$employee->id}}">{{$employee->name}}</option>
            @endforeach
          </select>
        </div>

        <br />

        <div class="form-group">
          <label for="remarks">Remarks</label>
          <textarea  name="remarks" class="form-control form-white" id="remarks" placeholder="Remarks " ></textarea>
        </div>


        <div class="form-group">
          <label for="advanceDate">Advance Date</label>
          <input type="date" name="advanceDate" class="form-control form-white" id="advanceDate" required >
        </div>

        <br />

        <div class="form-group">
          <label for="advanceAmount">Advance Amount</label>
          <input name="advanceAmount" required class="form-control form-white" id="advanceAmount" placeholder="Advance Amount" />
        </div>


        <hr>

        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
        </div>

        {{ Form::close() }}

      </div>
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>


@endsection
@section('extra_scripts')
  <script>
      $(document).ready(function() {

        $('#datatable-responsive').DataTable({
        	 "pageLength": 50
        });

      });
      setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
@endsection
@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

<div class="clearfix"></div>

<div class="row">
  <div class="col-md-5 col-sm-5 col-xs-12">
     <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-user"></i> <b>Employee Details</b></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        @foreach($advance_payments as $advance_payment)
        <ul class="to_do">
          <li>Name: <b>{{$advance_payment->employeeName}}</b></li>
          <li>Phone: <b>{{$advance_payment->phone}}</b></li>
          <li>Email: <b>{{$advance_payment->email}}</b></li>
          <li>Address: <b>{{$advance_payment->currentAddress}}</b></li>
          <li>Remarks: <b>{{$advance_payment->remarks}}</b></li>
          <li>Payment Date: <b><?php echo date("d-M-Y", strtotime($advance_payment->paymentDate)); ?></b></li>
        </ul>
        @endforeach
      </div>
    </div>
  </div>

  <div class="col-md-5 col-sm-5 col-xs-12">
     <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-cubes"></i> <b>Actions</b></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        
        <a class="btn btn-danger btn-sm"  data-toggle="modal" data-target=".give_new_payment"><i class="fa fa-money"></i> Give New Payment</a>
        <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".new_expense"><i class="fa fa-money"></i> New Expense</a>

        <hr>

        <ul class="to_do"> 
          <li style="color:blue;">Total Advance Amount :  @money($paymentAmount)</li>
          <li style="color:red;">Total Expense Amount : @money($paymentReturnAmount)</li>
          <li style="color:green;">Balance Amount : @money($paymentAmount-$paymentReturnAmount)</li>
        </ul>

      </div>
    </div>
  </div>

</div>

<div class="row">
<div class="col-md-6 col-sm-6 col-xs-12">
   <div class="x_panel">
    <div class="x_title">
      <h2><i class="fa fa-money"></i> <b>Advance Amount Details</b></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table id="datatable-responsive" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th width="5%">Serial</th>
            <th>Payment Date</th>
            <th>Amount</th>
            <th>Remarks</th>
            <th>Created By</th>
          </tr>
        </thead>
        <tbody>
          @php $i=0; @endphp
          @foreach($loanAmounts as $loanAmount)
          <tr>
            <td>{{++$i}}</td>
            <td><?php echo date("d-M-Y", strtotime($loanAmount->aphDate)); ?></td>
            <td>@money($loanAmount->amount)</td>
            <td>{{$loanAmount->remarks}}</td>
            <td>{{$loanAmount->created_by}}</td>
          </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th colspan="2" style="text-align:right;">Total</th>
            <th>@money($paymentAmount)</th>
            <th colspan="2"></th>
          </tr>
        </tfoot>
      </table>

    </div>
  </div>

</div>

<div class="col-md-6 col-sm-6 col-xs-12">
 <div class="x_panel">
  <div class="x_title">
    <h2><i class="fa fa-money"></i> <b>Expenses List</b></h2>
    <ul class="nav navbar-right panel_toolbox">
      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
    </ul>
    <div class="clearfix"></div>
  </div>
  <div class="x_content">
    <table id="datatable-responsive" class="table table-striped table-bordered">
      <thead>
        <tr>
          <th width="5%">Serial</th>
          <th>Expense Title</th>
          <th>Category</th>
          <th>Amount</th>
          <th>Payment Date</th>
          <th>Remarks</th>
          <th>Created By</th>
        </tr>
      </thead>
      <tbody>
        @php $i=0; @endphp
        @foreach($loanReturnAmounts as $loanReturnAmount)
        <tr>
          <td>{{++$i}}</td>
          <td>{{$loanReturnAmount->expenseTitle}}</td>
          <td>{{$loanReturnAmount->categoryName}}</td>
          <td>@money($loanReturnAmount->amount)</td>
          <td><?php echo date("d-M-Y", strtotime($loanReturnAmount->aphDate)); ?></td>
          <td>{{$loanReturnAmount->remarks}}</td>
          <td>{{$loanReturnAmount->created_by}}</td>
        </tr>
        @endforeach
      </tbody>
        <tfoot>
          <tr>
            <th colspan="3" style="text-align:right;">Total</th>
            <th>@money($paymentReturnAmount)</th>
            <th colspan="3"></th>
          </tr>
        </tfoot>
    </table>

  </div>
</div>

</div>

</div>

<div class="modal fade new_expense" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2"><b><i class="fa fa-ils"></i> New Expense</b></h4>
      </div>
      <div class="modal-body">
      {!! Form::open(array('url' => 'advance_new_expense', 'method' => 'POST')) !!}

        <div class="form-group">
          <label for="title"> Expense Title </label>
          <input name="advancePaymentId" hidden value="{{$advancePaymentId}}" >
          <input name="expenseTitle" class="form-control form-white" id="title" placeholder="Expense title " required >
        </div>

        <br />

        <div class="form-group">
          <label for="categoryName"> Expense Category</label>
          <input name="categoryName" class="form-control form-white" id="categoryName" placeholder="Expense Category" required >
        </div>

        <br />

        <div class="form-group">
          <label for="amount"> Expense Amount</label>
          <input type="number" step="any" name="amount" class="form-control form-white" id="amount" placeholder="Expense Amount " required >
        </div>

        <br />

        <div class="form-group">
          <label for="aphDate">Expense Date</label>
          <input type="date" name="aphDate" class="form-control form-white" id="aphDate" required placeholder="Expense date" >
        </div>

        <br />

        <div class="form-group">
          <label for="remarks">Remarks</label>
          <textarea name="remarks" class="form-control form-white" id="remarks" placeholder="Remarks" ></textarea>
        </div>


        <hr>

        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
        </div>

        {{ Form::close() }}

      </div>
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>

<div class="modal fade give_new_payment" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2"><b><i class="fa fa-ils"></i> Give New Payment</b></h4>
      </div>
      <div class="modal-body">
      {!! Form::open(array('url' => 'give_new_advance_amount', 'method' => 'POST')) !!}

        <div class="form-group">
          <label for="amount">Amount</label>
          <input name="advancePaymentId" hidden value="{{$advancePaymentId}}" >
          <input type="number" step="any" name="amount" class="form-control form-white" id="amount" placeholder="Amount " required >
        </div>

        <br />

        <div class="form-group">
          <label for="loanDate">Payment Date</label>
          <input type="date" name="aphDate" class="form-control form-white" id="loanDate" required >
        </div>

        <br />

        <div class="form-group">
          <label for="remarks">Remarks</label>
          <textarea name="remarks" class="form-control form-white" id="remarks" placeholder="Remarks" ></textarea>
        </div>


        <hr>

        <div class="form-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
        </div>

        {{ Form::close() }}

      </div>
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>


@endsection
@section('extra_scripts')
<script>
  $(document).ready(function() {

    $('#datatable-responsive').DataTable({
      "pageLength": 50
    });

  });
  @endsection
@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            @if(Session::has('message'))
                <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
            @endif
            @if(Session::has('failedMessage'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
            @endif

            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        <i class="fa fa-money"></i>
                        Received Loan List From
                        {{\Carbon\Carbon::parse($request->start_date)->format('d M Y')}} To
                        {{\Carbon\Carbon::parse($request->end_date)->format('d M Y')}}
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Serial</th>
                            <th>Name/Others</th>
                            <th>Loan Date</th>
                            <th>Loan Amount</th>
                            <th>Return Amount</th>
                            <th>Balance</th>
                        </tr>
                        </thead>


                        <tbody>
                        @php $i=0;
                        @endphp
                        @foreach($received_loan as $rl)
                            <?php
                            $loan_amount=\Illuminate\Support\Facades\DB::table('tbreceived_loan_history')->where('loanId','=',$rl->id)->where('paymentType','=','1')->sum('amount');
                            $return_amount=\Illuminate\Support\Facades\DB::table('tbreceived_loan_history')->where('loanId','=',$rl->id)->where('paymentType','=','2')->sum('amount');
                            ?>
                            <tr>
                                <td>{{++$i}}</td>
                                <td>{{$rl->name}}</td>
                                <td>{{$rl->loanDate}}</td>
                                <td>
                                    {{ $loan_amount }}
                                </td>
                                <td>
                                    {{$return_amount}}
                                </td>
                                <td>
                                    {{$loan_amount-$return_amount}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        {{--<tr>--}}
                            {{--<td>Total</td>--}}
                            {{--<td></td>--}}
                            {{--<td></td>--}}
                            {{--<td></td>--}}
                            {{--<td></td>--}}
                            {{--<td></td>--}}
                        {{--</tr>--}}
                    </table>

                </div>
            </div>
        </div>
    </div>


@endsection
@section('extra_scripts')
    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
@endsection
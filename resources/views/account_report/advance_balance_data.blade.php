@extends('layouts.master')
@section('extra_css')

@endsection
@section('content')

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">

            @if(Session::has('message'))
                <p id="alert_message" class="alert alert-success">{{ Session::get('message') }}</p>
            @endif
            @if(Session::has('failedMessage'))
                <p id="alert_message" class="alert alert-danger">{{Session::get('failedMessage')}}</p>
            @endif

            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        <i class="fa fa-money"></i> Advance Payment List From
                        {{\Carbon\Carbon::parse($request->start_date)->format('d M Y')}} to
                        {{\Carbon\Carbon::parse($request->end_date)->format('d M Y')}}
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Serial</th>
                            <th>Employee/Others</th>
                            <th>Payment Date</th>
                            <th>Advance Amount</th>
                            <th>Expense Amount</th>
                            <th>Balance</th>
                        </tr>
                        </thead>


                        <tbody>
                        @php $i=0;
                        $am=0;
                        $em=0;
                        @endphp
                        @foreach($advance_payments as $advance_payment)
                            <tr>
                                <td>{{++$i}}</td>
                                <td>{{$advance_payment->name}}</td>
                                <td>{{$advance_payment->aphDate}}</td>
                                <td>
                                    @if($advance_payment->paymentType==1)
                                        {{$advance_payment->amount}}
                                        <?php
                                            $am+=$advance_payment->amount;
                                        ?>
                                    @endif
                                </td>
                                <td>
                                    @if($advance_payment->paymentType==2)
                                        {{$advance_payment->amount}}
                                        <?php
                                            $em+=$advance_payment->amount;
                                        ?>

                                    @endif
                                </td>
                                <td></td>
                            </tr>

                        @endforeach
                            <tr style="font-weight: bold; background: cornsilk;">
                                <td align="center" colspan="3">Total</td>
                                {{--<td></td>--}}
                                {{--<td></td>--}}
                                <td>{{$am}}</td>
                                <td>{{$em}}</td>
                                <td>{{$am-$em}}</td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>


@endsection
@section('extra_scripts')
    <script>
//        $(document).ready(function() {
//
//            $('#datatable-responsive').DataTable({
//                "pageLength": 50
//            });
//
//        });
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
@endsection
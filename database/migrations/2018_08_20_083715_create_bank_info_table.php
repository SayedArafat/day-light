<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbbank_info', function (Blueprint $table) {
            $table->increments('id');
            $table->text('bankName')->nullable();
            $table->string('bankAccountNo')->nullable();
            $table->string('bankSwiftCode')->nullable();
            $table->string('bankVatNo')->nullable();
            $table->string('bankAddress')->nullable();
            $table->text('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbbank_info');
    }
}

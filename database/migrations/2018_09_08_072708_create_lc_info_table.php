<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLcInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblc_info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lcNumber',100)->nullable();
            $table->date('lcDate')->nullable();
            $table->string('challanNo',100)->nullable();
            $table->tinyinteger('companyId')->nullable();
            $table->tinyinteger('companyBankId')->nullable();
            $table->integer('customerId')->nullable();
            $table->string('customerBankName')->nullable();
            $table->text('customerBankInfo')->nullable();
            $table->string('sightDays',6)->nullable();
            $table->string('contractNo',75)->nullable();
            $table->date('contractDate')->nullable();
            $table->text('valueReceived')->nullable();
            $table->string('IRCNo',100)->nullable();
            $table->string('TINNo',100)->nullable();
            $table->string('VatRegNo',100)->nullable();
            $table->tinyinteger('status')->nullable();
            $table->tinyinteger('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblc_info');
    }
}

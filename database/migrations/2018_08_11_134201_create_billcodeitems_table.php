<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillcodeitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbbillcodeitems', function (Blueprint $table) {
            $table->increments('id');
            $table->string('billcodeId',12)->nullable()->index();
            $table->string('bcPi_number',12)->nullable()->index();
            $table->string('bcOritem_id',12)->nullable()->index();
            $table->string('bcQuantity',10)->nullable();
            $table->string('bcUnitPrice',12)->nullable();
            $table->string('bcFrom')->nullable();
            $table->string('bcTo')->nullable();
            $table->string('bcRemarks')->nullable();
            $table->tinyinteger('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbbillcodeitems');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbcustomers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customerName')->nullable();
            $table->string('customerPhone')->nullable();
            $table->string('customerEmail')->nullable();
            $table->string('customerAddress')->nullable();
            $table->text('remarks')->nullable();
            $table->tinyInteger('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbcustomers');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceivedLoanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbreceived_loan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',250);
            $table->text('details')->nullable();
            $table->text('address')->nullable();
            $table->string('phone',50)->nullable();
            $table->string('email',50)->nullable();
            $table->text('remarks')->nullable();
            $table->date('loanDate')->nullable();
            $table->tinyInteger('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbreceived_loan');
    }
}

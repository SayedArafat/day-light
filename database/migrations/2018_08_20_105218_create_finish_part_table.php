<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinishPartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbfinish_part', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fDate')->nullable();
            $table->string('billCodeItemId',15)->nullable();
            $table->string('twill',10)->nullable();
            $table->string('tc',10)->nullable();
            $table->string('deliveryFactory',150)->nullable();
            $table->text('comment')->nullable();
            $table->tinyInteger('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbfinish_part');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvancePaymentHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbadvance_payment_history', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('advancePaymentId');
            $table->tinyInteger('paymentType')->nullable();
            $table->string('expenseTitle')->nullable();
            $table->string('categoryName')->nullable();
            $table->string('amount',10)->nullable();
            $table->text('remarks')->nullable();
            $table->date('aphDate')->nullable();
            $table->tinyInteger('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbadvance_payment_history');
    }
}

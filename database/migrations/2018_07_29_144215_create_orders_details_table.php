<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tborders_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rpiId')->index()->nullable();
            $table->string('item')->nullable();
            $table->string('style')->nullable();
            $table->text('details')->nullable();
            $table->string('color')->nullable();
            $table->string('orderQuantity',25)->nullable();
            $table->string('unitPrice',25)->nullable();
            $table->tinyInteger('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tborders_details');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomeSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbincome_sources', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('categoryId')->index()->nullable();
            $table->text('source')->nullable();
            $table->string('payment_method',100)->nullable();
            $table->string('bank_account')->nullable();
            $table->string('reference')->nullable();
            $table->text('description')->nullable();
            $table->date('incomeDate')->nullable();
            $table->string('amount',15)->nullable();
            $table->text('attachement')->nullable();
            $table->tinyInteger('createdBy')->nullable();
            $table->tinyInteger('updatedBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbincome_sources');
    }
}

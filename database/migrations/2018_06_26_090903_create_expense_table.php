<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpenseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbexpenselist', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',150);
            $table->tinyInteger('categoryId');
            $table->string('amount',25);
            $table->string('reference')->nullable();
            $table->text('description')->nullable();
            $table->date('expenseDate');
            $table->text('expenseAttachment')->nullable();
            $table->tinyInteger('createdBy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbexpenselist');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tborders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('companyId')->index()->nullable();
            $table->string('pi')->index()->unique()->nullable();
            $table->integer('customerId')->index()->nullable();
            $table->date('orderDate')->nullable();
            $table->date('deliveryDate')->nullable();
            $table->date('shippingDate')->nullable();
            $table->string('goodsOrigin')->nullable();
            $table->string('sources')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->text('remarks')->nullable();
            $table->string('bankId',10)->index()->nullable();
            $table->tinyInteger('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tborders');
    }
}

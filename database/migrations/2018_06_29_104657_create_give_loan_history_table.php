<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGiveLoanHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbgive_loan_history', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('loanId');
            $table->tinyInteger('paymentType')->nullable();;
            $table->string('amount',10)->nullable();
            $table->text('remarks')->nullable();
            $table->date('paymentDate')->nullable();
            $table->tinyInteger('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbgive_loan_history');
    }
}

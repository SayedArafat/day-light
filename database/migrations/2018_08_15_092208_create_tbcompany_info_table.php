<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbcompanyInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbcompany_info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('companyName')->nullable();
            $table->string('companyTagline')->nullable();
            $table->string('companyPhone')->nullable();
            $table->string('companyEmail')->nullable();
            $table->text('companyBAddress')->nullable();
            $table->text('companyFAddress')->nullable();
            $table->text('companyLogo')->nullable();
            $table->text('companysignature1')->nullable();
            $table->text('companysignature2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbcompany_info');
    }
}

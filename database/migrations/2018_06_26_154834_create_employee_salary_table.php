<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeSalaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbemployee_salary', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('empId');
            $table->string('amount',25)->default('0');
            $table->date('salaryMonth');
            $table->tinyInteger('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbemployee_salary');
    }
}

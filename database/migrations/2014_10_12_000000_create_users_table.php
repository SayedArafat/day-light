<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',200)->nullable();
            $table->string('employeeNumber',50)->nullable();
            $table->tinyInteger('userType')->default('0');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone',50)->nullable();
            $table->string('gender',25)->nullable();
            $table->string('accountStatus')->default('1');
            $table->text('currentAddress')->nullable();
            $table->date('joiningDate')->nullable();
            $table->tinyInteger('designationId')->nullable();
            $table->tinyInteger('departmentId')->nullable();
            $table->string('salary',25)->default('0');
            $table->text('photo')->nullable();
            $table->tinyInteger('createdBy')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

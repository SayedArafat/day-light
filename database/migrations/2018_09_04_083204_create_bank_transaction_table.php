<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbbank_transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyinteger('payType')->index()->nullable();
            $table->integer('bankId')->index()->nullable();
            $table->string('creditAmount',15)->nullable();
            $table->string('debitAmount',15)->nullable();
            $table->date('transDate')->nullable();
            $table->string('reference')->nullable();
            $table->text('description')->nullable();
            $table->tinyinteger('paymentMethod')->nullable();
            $table->tinyinteger('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbbank_transaction');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockFabricTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbstock_fabric', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sfRef')->nullable();
            $table->date('sfDate')->nullable();
            $table->string('sfItem')->nullable();
            $table->string('sfColor')->nullable();
            $table->string('sfPi')->nullable();
            $table->string('sfQty')->nullable();
            $table->string('sfDetails')->nullable();
            $table->string('sfRemarks')->nullable();
            $table->tinyinteger('createdBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbstock_fabric');
    }
}

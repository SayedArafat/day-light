<?php

use Illuminate\Database\Seeder;

class companyInfoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbcompany_info')->insert([
            'companyName'=>'Daylight Textile Corporation',
            'companyTagline'=>'100% Export Oriented Fabrics Manufacturer & Supplier',
            'companyPhone'=>'+88-02-48954746',
            'companyEmail'=>'info@daylighttextile-bd.com',
            'companyBAddress'=>'House#29(5th floor), Road#05, Sector#10, Uttara, Dhaka-1230.',
            'companyFAddress'=>'Monohorpur, Madabdi, Narshindhi, Bangladesh.',
            'companyLogo'=>'dlogo.png',

        ]);
    }
}

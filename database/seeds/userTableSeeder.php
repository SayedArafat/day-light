<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class userTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=>'Admin',
            'email'=>'admin@email.com',
            'password'=>bcrypt('123456'),
            'userType'=>'1',
            'accountStatus'=>'1',
            'photo'=>'default.jpg',

        ]);
    }
}

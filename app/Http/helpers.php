<?php
  function checkPermission($permissions){
    $userAccess = getMyPermission(auth()->user()->userType);
    foreach ($permissions as $key => $value) {
      if($value == $userAccess){
        return true;
      }
    }
    return false;
  }

  function getMyPermission($id)
  {
    switch ($id) {

      case 1:
        return 'superadmin';
        break;
        
      case 2:
        return 'admin';
        break;

      case 3:
        return 'accountant';
        break;

      case 4:
        return 'merchandiser';
        break;

      case 5:
        return 'commercial';
        break;

      case 0:
        return 'employee';
        break;

      default:
        return 'user';
        break;
    }
  }
?>
<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use PDF;
class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banks=DB::table('tbbank_info')->get();
        return view('bank.index',compact('banks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'bankName'=>'required',
            'bankAccountNo'=>'required',
            'bankSwiftCode'=>'required',
            'bankVatNo'=>'required'
        ]);

        $now=Carbon::now()->toDateTimeString();
        $str=DB::table('tbbank_info')->insert([
            'bankName'=>$request->bankName,
            'bankAccountNo'=>$request->bankAccountNo,
            'bankSwiftCode'=>$request->bankSwiftCode,
            'bankVatNo'=>$request->bankVatNo,
            'remarks'=>$request->remarks,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);

        if($str)
        {
            Session::flash('message','Bank Information has been successfully added. ');
        }else{
            Session::flash('failedMessage','Insertion Failed.');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'bankName'=>'required',
            'bankAccountNo'=>'required',
            'bankSwiftCode'=>'required',
            'bankVatNo'=>'required'
        ]);

        $now=Carbon::now()->toDateTimeString();
        $str=DB::table('tbbank_info')->where(['id'=>$id])->update([
            'bankName'=>$request->bankName,
            'bankAccountNo'=>$request->bankAccountNo,
            'bankSwiftCode'=>$request->bankSwiftCode,
            'bankVatNo'=>$request->bankVatNo,
            'remarks'=>$request->remarks,
            'updated_at'=>$now,
        ]);

        if($str)
        {
            Session::flash('message','Bank Information has been successfully updated. ');
        }else{
            Session::flash('failedMessage','Updation Failed.');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  
        $check=DB::table('tborders')->where(['bankId'=>$id])->get();
        if(count($check)){
            Session::flash('failedMessage',"Deletion failed. There are some others data already using this resource." );
        }
        else {
            $dept=DB::table('tbbank_info')->where(['id'=>$id])->delete();
            Session::flash('message','Bank Information Successfully Deleted.');
        }
        return redirect()->back();
    }
    
    public function all_bank_list()
    {
        $banks=DB::table('tbbank_info')->get();
        foreach($banks as $bank){
            $info=DB::table('tbbank_transaction')
            ->where([['tbbank_transaction.bankId', '=', $bank->id],['tbbank_transaction.payType', '=', 1]])
            ->sum('creditAmount');
            $bank->tcreditAmount=$info;

            $info1=DB::table('tbbank_transaction')
            ->where([['tbbank_transaction.bankId', '=', $bank->id],['tbbank_transaction.payType', '=', 2]])
            ->sum('debitAmount');
            $bank->tdebitAmount=$info1;
        }

        return view('bank.bank_list',compact('banks'));
    }

    public function bank_transaction_details($id)
    {
        $id = base64_decode($id);
        $bank_info=DB::table('tbbank_info')->where('id','=',$id)->first();

        $info=DB::table('tbbank_transaction')
        ->where([['tbbank_transaction.bankId', '=', $id],['tbbank_transaction.payType', '=', 1]])
        ->sum('creditAmount');
        $bank_info->tcreditAmount=$info;

        $info1=DB::table('tbbank_transaction')
        ->where([['tbbank_transaction.bankId', '=', $id],['tbbank_transaction.payType', '=', 2]])
        ->sum('debitAmount');
        $bank_info->tdebitAmount=$info1;

        $cash_in=DB::table('tbbank_transaction')
        ->where([['tbbank_transaction.bankId', '=', $id],['tbbank_transaction.payType', '=', 1]])
        ->get();

        $cash_out=DB::table('tbbank_transaction')
        ->where([['tbbank_transaction.bankId', '=', $id],['tbbank_transaction.payType', '=', 2]])
        ->get();

        return view('bank.bank_transaction_details',compact('bank_info','cash_in','cash_out'));
    }

    public function cash_in()
    {
        $banks=DB::table('tbbank_info')->get();
        return view('bank.cash_in',compact('banks'));
    }


    public function cash_in_store(Request $request)
    {
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbbank_transaction')->insert([
            'bankId'=>$request->bankId,
            'payType'=>$request->payment_type,
            'paymentMethod'=>$request->payment_method,
            'creditAmount'=>$request->amount,
            'transDate'=>$request->transDate,
            'reference'=>$request->reference,
            'description'=>$request->description,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);

        if($str)
        {
            Session::flash('message','Transaction(Cash In) has been successfully added. ');
        }else{
            Session::flash('failedMessage','Insertion failed.');
        }
        return redirect()->back();
    }

    public function cash_out()
    {
        $banks=DB::table('tbbank_info')->get();
        return view('bank.cash_out',compact('banks'));
    }


    public function cash_out_store(Request $request)
    {
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbbank_transaction')->insert([
            'bankId'=>$request->bankId,
            'payType'=>$request->payment_type,
            'paymentMethod'=>$request->payment_method,
            'debitAmount'=>$request->amount,
            'transDate'=>$request->transDate,
            'reference'=>$request->reference,
            'description'=>$request->description,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);

        if($str)
        {
            Session::flash('message','Transaction(Cash Out) has been successfully added. ');
        }else{
            Session::flash('failedMessage','Insertion failed.');
        }
        return redirect()->back();
    }
    
    public function transaction_history()
    {
        return view('bank.transaction_history_view');
    }

    public function transaction_history_data(Request $request)
    {
        $transaction_history=DB::table('tbbank_transaction')
        ->leftJoin('users','users.id','=','tbbank_transaction.createdBy')
        ->leftJoin('tbbank_info','tbbank_info.id','=','tbbank_transaction.bankId')
        ->select('tbbank_transaction.*','tbbank_info.bankName','tbbank_info.bankAccountNo','users.name as created_by')
        ->whereBetween('tbbank_transaction.transDate', [$request->start_date, $request->end_date])
        ->get();
        // dd($transaction_history);
        if(($request->submit_type)=='preview'){
            return view('bank.transaction_history_data',compact('transaction_history','request'));
        }elseif(($request->submit_type)=='pdf'){
            // return View('pdf.transaction_history_data_pdf',compact('transaction_history','request'));
            $pdf=PDF::loadView('pdf.transaction_history_data_pdf',compact('transaction_history','request'));
            $filename=time()."_transaction_history.pdf";
            return $pdf->download($filename);
        }else{
            return redirect('/error');
        }
    }
}

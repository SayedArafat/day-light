<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class IncomeSourceCategoryController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $icategories=DB::table('tbincome_source_category')
        ->leftJoin('users','users.id','=','tbincome_source_category.createdBy')
        ->select('tbincome_source_category.*','users.name as created_by')
        ->get();
        return view('income_category.index',compact('icategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'categoryName'=>'required'
        ]);
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbincome_source_category')->insert([
            'categoryName'=>$request->categoryName,
            'categoryDescription'=>$request->categoryDescription,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);

        if($str)
        {
            Session::flash('message','Income Source Category Has Been Successfully Inserted.');
        }else{
            Session::flash('failedMessage','Income Source Category Insertion Failed.');
        }
        
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'categoryName'=>'required'
        ]);
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbincome_source_category')->where(['id'=>$id])->update([
            'categoryName'=>$request->categoryName,
            'categoryDescription'=>$request->categoryDescription,
            'createdBy'=>$user->id,
            'updated_at'=>$now,

        ]);

        if($str)
        {
            Session::flash('message','Income Source category has been successfully updated.');
        }else{
            Session::flash('failedMessage','Income Source Category Updation Failed.');
        }
        
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check=DB::table('tbincome_sources')->where(['categoryId'=>$id])->get();
        if(count($check)){
            Session::flash('failedMessage',"Deletion failed. There are some others data already using this resource." );
        }
        else {
            $dept=DB::table('tbincome_source_category')->where(['id'=>$id])->delete();
            Session::flash('message','Income Source Category Successfully Deleted.');
        }
        return redirect()->back();
    }
}

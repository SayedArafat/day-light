<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class receiveLoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $loans=DB::table('tbreceived_loan')
        ->leftJoin('users','users.id','=','tbreceived_loan.createdBy')
        ->select('tbreceived_loan.*','users.name as created_by')
        ->get();
        return view('loans.receive_loan',compact('loans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[
            'name'=>'required',
            'loanDate'=>'required'
        ]);
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbreceived_loan')->insertGetId([
            'name'=>$request->name,
            'details'=>$request->details,
            'address'=>$request->address,
            'phone'=>$request->phone,
            'email'=>$request->email,
            'loanDate'=>Carbon::parse($request->loanDate)->format('Y-m-d'),
            'remarks'=>$request->remarks,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);

        $str=DB::table('tbreceived_loan_history')->insert([
            'loanId'=>$str,
            'paymentType'=>'1',
            'amount'=>$request->amount,
            'paymentDate'=>Carbon::parse($request->loanDate)->format('Y-m-d'),
            'remarks'=>NULL,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);
        if($str)
        {
            Session::flash('message','Give Loan Information Successfully Inserted ');
        }else{
            Session::flash('failedMessage','Give Loan Information Insertion Failed.');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paymentAmount = DB::table('tbreceived_loan_history')->where([['loanId','=',$id],['paymentType','=','1']])->sum('amount');
        $paymentReturnAmount = DB::table('tbreceived_loan_history')->where([['loanId','=',$id],['paymentType','=','2']])->sum('amount');
        $loanAmounts=DB::table('tbreceived_loan_history')
        ->where([['tbreceived_loan_history.loanId','=',$id],['tbreceived_loan_history.paymentType','=',1]])
        ->leftJoin('users','users.id','=','tbreceived_loan_history.createdBy')
        ->select('tbreceived_loan_history.*','users.name as created_by')
        ->get();

        $loanReturnAmounts=DB::table('tbreceived_loan_history')
        ->where([['tbreceived_loan_history.loanId','=',$id],['tbreceived_loan_history.paymentType','=',2]])
        ->leftJoin('users','users.id','=','tbreceived_loan_history.createdBy')
        ->select('tbreceived_loan_history.*','users.name as created_by')
        ->get();

        $loans=DB::table('tbreceived_loan')
        ->leftJoin('users','users.id','=','tbreceived_loan.createdBy')
        ->select('tbreceived_loan.*','users.name as created_by')
        ->where('tbreceived_loan.id','=',$id)
        ->get();
        $loanId = $id;

        return view('loans.receive_loan_details',compact('loans','loanAmounts','loanReturnAmounts','paymentAmount','paymentReturnAmount','loanId'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'name'=>'required',
            'loanDate'=>'required'
        ]);
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbreceived_loan')->where(['id'=>$id])->update([
            'name'=>$request->name,
            'details'=>$request->details,
            'address'=>$request->address,
            'phone'=>$request->phone,
            'email'=>$request->email,
            'loanDate'=>Carbon::parse($request->loanDate)->format('Y-m-d'),
            'remarks'=>$request->remarks,
            'createdBy'=>$user->id,
            'updated_at'=>$now,

        ]);

        if($str)
        {
            Session::flash('message','Receive Loan Information Successfully Updated. ');
        }else{
            Session::flash('failedMessage','Receive Loan Information Updatation Failed.');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check=DB::table('tbreceived_loan_history')->where(['loanId'=>$id])->get();
        if(count($check)){
            Session::flash('failedMessage',"Deletion failed. There are some others data already using this resource." );
        }
        else {
            $dept=DB::table('tbreceived_loan')->where(['id'=>$id])->delete();
            Session::flash('message','Receive Loan information Successfully Deleted.');
        }
        return redirect()->back();
    }

    public function giveNewLoan(Request $request)
    {
      
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbreceived_loan_history')->insert([
            'loanId'=>$request->loanId,
            'paymentType'=>'1',
            'amount'=>$request->amount,
            'paymentDate'=>Carbon::parse($request->loanDate)->format('Y-m-d'),
            'remarks'=>NULL,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);

        if($str)
        {
            Session::flash('message','New Payment Information Successfully Inserted ');
        }else{
            Session::flash('failedMessage','New Payment Information Insertion Failed.');
        }
        return redirect()->back();
    }

    public function giveReturnLoan(Request $request)
    {
      
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbreceived_loan_history')->insert([
            'loanId'=>$request->loanId,
            'paymentType'=>'2',
            'amount'=>$request->amount,
            'paymentDate'=>Carbon::parse($request->loanDate)->format('Y-m-d'),
            'remarks'=>NULL,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);

        if($str)
        {
            Session::flash('message','Return Loan Payment Information Successfully Inserted ');
        }else{
            Session::flash('failedMessage','Return Loan Payment Information Insertion Failed.');
        }
        return redirect()->back();
    }
}

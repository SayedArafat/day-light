<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use File;

class ExpensesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expenseCategories=DB::table('tbexpensecategory')->get();
         $expenses=DB::table('tbexpenselist')
        ->leftJoin('tbexpensecategory','tbexpensecategory.id','=','tbexpenselist.categoryId')
        ->leftJoin('users','users.id','=','tbexpenselist.createdBy')
        ->select('tbexpenselist.*','users.name as created_by','tbexpensecategory.categoryName as categoryName')
        ->get();
        return view('expenses.index',compact('expenses','expenseCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $expenseCategories=DB::table('tbexpensecategory')->get();

        return view('expenses.create',compact('expenseCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[
            'categoryId'=>'required',
            'title'=>'required',
            'amount'=>'required',
            'expenseDate'=>'required',
        ]);

        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        
        if($request->file('expenseAttachment')) {
            if ($request->file('expenseAttachment')->getClientSize() > 2000000) {
                Session::flash('fileSize', "Expense Added failed. File Size Limit Exceeded.");
                return redirect()->back();
            }
        }

        if($file=$request->file('expenseAttachment')){
            $expenseAttachment=time()."_".$file->getClientOriginalName();
            $file->move('expense_attachment',$expenseAttachment);
        }
        else{
            $expenseAttachment=NULL;
        }

        $str=DB::table('tbexpenselist')->insert([
            'title'=>$request->title,
            'categoryId'=>$request->categoryId,
            'amount'=>$request->amount,
            'reference'=>$request->reference,
            'description'=>$request->description,
            'expenseDate'=>Carbon::parse($request->expenseDate)->format('Y-m-d'),
            'expenseAttachment'=>$expenseAttachment,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);

        if($str)
        {
            Session::flash('message','Expense has been successfully added.');
        }else{
            Session::flash('failedMessage','Expense Insertion Failed.');
        }
        
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'categoryId'=>'required',
            'title'=>'required',
            'amount'=>'required',
            'expenseDate'=>'required',
        ]);

        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        
        if($request->file('expenseAttachment')) {
            if ($request->file('expenseAttachment')->getClientSize() > 2000000) {
                Session::flash('fileSize', "Expense update failed. File Size Limit Exceeded.");
                return redirect()->back();
            }
        }

        if($request->hasFile('expenseAttachment')){
            $file=$request->file('expenseAttachment');
            if(($request->expenseAttachmentPre!='')||($request->expenseAttachmentPre!=NULL)){
                $fileUrl ='expense_attachment/'.$request->expenseAttachmentPre;
                if(file_exists($fileUrl))
                    unlink($fileUrl);
            }

            $expenseAttachment=time()."_".$file->getClientOriginalName();
            $file->move('expense_attachment',$expenseAttachment);
        }
        else{
            $expenseAttachment=$request->expenseAttachmentPre;
        }

        $str=DB::table('tbexpenselist')->where(['id'=>$id])->update([
            'title'=>$request->title,
            'categoryId'=>$request->categoryId,
            'amount'=>$request->amount,
            'reference'=>$request->reference,
            'description'=>$request->description,
            'expenseDate'=>Carbon::parse($request->expenseDate)->format('Y-m-d'),
            'expenseAttachment'=>$expenseAttachment,
            'createdBy'=>$user->id,
            'updated_at'=>$now,
        ]);

        if($str)
        {
            Session::flash('message','Expense has been successfully updated.');
        }else{
            Session::flash('failedMessage','Expense Deletion Failed.');
        }
        
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $expenseAttachment = DB::table('tbexpenselist')->where('id', $id)->first();;
        $fileUrl ='expense_attachment/'.$expenseAttachment->expenseAttachment;
        if(file_exists($fileUrl))
            unlink($fileUrl);
        

        DB::table('tbexpenselist')->where(['id'=>$id])->delete();
        Session::flash('message','Expense Successfully Deleted.');
        return redirect()->back();
    }

    public function todays_expenses()
    {
        $expenseCategories=DB::table('tbexpensecategory')->get();
         $expenses=DB::table('tbexpenselist')
        ->leftJoin('tbexpensecategory','tbexpensecategory.id','=','tbexpenselist.categoryId')
        ->leftJoin('users','users.id','=','tbexpenselist.createdBy')
        ->whereDate('expenseDate', '=', Carbon::today()->toDateString())
        ->select('tbexpenselist.*','users.name as created_by','tbexpensecategory.categoryName as categoryName')
        ->get();
        return view('expenses.todays_expenses',compact('expenses','expenseCategories'));
    }

}

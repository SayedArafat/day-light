<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class IncomeSourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
//        return "hello world";
        $incomeCategories=DB::table('tbincome_source_category')->get();
//        return $incomeCategories;

        $income_sources=DB::table('tbincome_sources')
        ->leftJoin('tbincome_source_category','tbincome_source_category.id','=','tbincome_sources.categoryId')
        ->leftJoin('tborders','tbincome_sources.source','=','tborders.id')
        ->leftJoin('users','users.id','=','tbincome_sources.createdBy')
        ->select('tbincome_sources.*','tborders.pi','users.name as created_by','tbincome_source_category.categoryName as categoryName')
        ->get();
//        return $income_sources;
        return view('incomes.index',compact('income_sources','incomeCategories'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $incomeCategories=DB::table('tbincome_source_category')->get();
        $orders=DB::table('tborders')->where('status','=','0')->get(['id','pi']);
        $banks=DB::table('tbbank_info')->get(['id','bankName','bankAccountNo']);
        return view('incomes.create',compact('incomeCategories','orders','banks'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'categoryId'=>'required',
//            'source'=>'required',
            'amount'=>'required',
            'incomeDate'=>'required',
        ]);

        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        
        if($request->file('incomeAttachment')) {
            if ($request->file('incomeAttachment')->getClientSize() > 2000000) {
                Session::flash('fileSize', "Store failed. File Size Limit Exceeded.");
                return redirect()->back();
            }
        }

        if($file=$request->file('incomeAttachment')){
            $incomeAttachment=time()."_".$file->getClientOriginalName();
            $file->move('income_attachment',$incomeAttachment);
        }
        else{
            $incomeAttachment=NULL;
        }

        $str=DB::table('tbincome_sources')->insert([
            'source'=>$request->source,
            'payment_method'=>$request->payment_type,
            'bank_account'=>$request->bank_account,
            'categoryId'=>$request->categoryId,
            'amount'=>$request->amount,
            'reference'=>$request->reference,
            'description'=>$request->description,
            'incomeDate'=>Carbon::parse($request->incomeDate)->format('Y-m-d'),
            'attachement'=>$incomeAttachment,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);

        if($str)
        {
            Session::flash('message','Income has been successfully added.');
        }else{
            Session::flash('failedMessage','Income Insertion Failed.');
        }
        
        return redirect("/incomes");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $incomeCategories=DB::table('tbincome_source_category')->get();
        $income_details=DB::table('tbincome_sources')->where('tbincome_sources.id','=',$id)
            ->leftJoin('tbincome_source_category','tbincome_source_category.id','=','tbincome_sources.categoryId')
            ->leftJoin('tborders','tbincome_sources.source','=','tborders.id')
            ->leftJoin('users','users.id','=','tbincome_sources.createdBy')
            ->leftJoin('tbbank_info','tbincome_sources.bank_account','=','tbbank_info.id')
            ->select('tbincome_sources.*','tborders.pi','users.name as created_by','tbbank_info.bankName','tbbank_info.bankAccountNo','tbincome_source_category.categoryName as categoryName')
            ->first();
        return view('incomes.show',compact('incomeCategories','income_details'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banks=DB::table('tbbank_info')->get(['id','bankName','bankAccountNo']);
        $incomeCategories=DB::table('tbincome_source_category')->get();
        $pi_numbers=DB::table('tborders')->where('status','=','0')->get(['id','pi']);
        $income_details=DB::table('tbincome_sources')->where('tbincome_sources.id','=',$id)
            ->leftJoin('tbincome_source_category','tbincome_source_category.id','=','tbincome_sources.categoryId')
            ->leftJoin('tborders','tbincome_sources.source','=','tborders.id')
            ->leftJoin('users','users.id','=','tbincome_sources.createdBy')
            ->leftJoin('tbbank_info','tbincome_sources.bank_account','=','tbbank_info.id')
            ->select('tbincome_sources.*','tborders.pi','users.name as created_by','tbbank_info.bankName','tbbank_info.bankAccountNo','tbincome_source_category.categoryName as categoryName')
            ->first();
        return view('incomes.edit',compact('incomeCategories','income_details','pi_numbers','banks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        return $request->all();
        $this->validate($request,[
            'categoryId'=>'required',
//            'source'=>'required',
            'amount'=>'required',
            'incomeDate'=>'required',
        ]);
        if($request->file('incomeAttachment')) {
            if ($request->file('incomeAttachment')->getClientSize() > 2000000) {
                Session::flash('fileSize', "Store failed. File Size Limit Exceeded.");
                return redirect()->back();
            }
        }
        if($file=$request->file('incomeAttachment')){
            $incomeAttachment=time()."_".$file->getClientOriginalName();
            $file->move('income_attachment',$incomeAttachment);
            $income_source=DB::table('tbincome_sources')->where('id','=',$id)->first();
            if(!empty($income_source->attachement)){
                $path=public_path()."/income_attachment/".$income_source->attachement;
                if(file_exists($path)){
                    unlink($path);

                }
            }
        }
        else{
            $incomeAttachment=NULL;
        }
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbincome_sources')->where('id','=',$id)->update([
            'source'=>$request->source,
            'payment_method'=>$request->payment_type,
            'bank_account'=>$request->bank_account,
            'categoryId'=>$request->categoryId,
            'amount'=>$request->amount,
            'reference'=>$request->reference,
            'description'=>$request->description,
            'incomeDate'=>Carbon::parse($request->incomeDate)->format('Y-m-d'),
            'attachement'=>$incomeAttachment,
            'updatedBy'=>$user->id,
            'updated_at'=>$now,
        ]);

        if($str)
        {
            Session::flash('message','Income Information has been successfully updated.');
        }else{
            Session::flash('failedMessage','Update Failed.');
        }

        return redirect("/incomes");
    }

    public function show_delete($id){
        $income=DB::table('tbincome_sources')->where('tbincome_sources.id','=',$id)
            ->leftJoin('tborders','tbincome_sources.source','=','tborders.id')
            ->select('tbincome_sources.id','tborders.pi')
            ->first();
        return view('incomes.show_delete',compact('income'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $income_source=DB::table('tbincome_sources')->where('id','=',$id)->first();
        if(!empty($income_source->attachement)){
            $path=public_path()."/income_attachment/".$income_source->attachement;
            if(file_exists($path)){
                unlink($path);

            }
        }
        $t=DB::table('tbincome_sources')->where('id','=',$id)->delete();
        if($t){
            Session::flash('message','Income Delete Successful');
        }
        else{
            Session::flash('failedMessage','Delete Failed.');

        }
        return redirect("/incomes");
    }

    public function todays_income(){
        $incomeCategories=DB::table('tbincome_source_category')->get();
//        return $incomeCategories;

        $income_sources=DB::table('tbincome_sources')
            ->leftJoin('tbincome_source_category','tbincome_source_category.id','=','tbincome_sources.categoryId')
            ->leftJoin('tborders','tbincome_sources.source','=','tborders.id')
            ->leftJoin('users','users.id','=','tbincome_sources.createdBy')
            ->where('tbincome_sources.incomeDate','=',Carbon::now()->toDateString())
            ->select('tbincome_sources.*','tborders.pi','users.name as created_by','tbincome_source_category.categoryName as categoryName')
            ->get();
//        return $income_sources;
        return view('incomes.todays_income',compact('income_sources','incomeCategories'));
    }
}

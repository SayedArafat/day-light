<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use PDF;
use Illuminate\Database\QueryException;

class orderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders=DB::table('tborders')
        ->leftJoin('tbcustomers','tbcustomers.id','=','tborders.customerId')
        ->leftJoin('users','users.id','=','tborders.createdBy')
        ->select('tborders.*','users.name as created_by','tbcustomers.customerName','tbcustomers.customerPhone','tbcustomers.customerEmail','tbcustomers.customerAddress')
        ->get();
        return view('orders.index',compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers=DB::table('tbcustomers')->get();
        return view('orders.create',compact('customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       $this->validate($request,[
            'pi'=>'required',
            'customerId'=>'required'
        ]);
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        try{
            $str=DB::table('tborders')->insert([
            'pi'=>$request->pi,
            'customerId'=>$request->customerId,
            'orderDate'=>$request->orderDate,
            'deliveryDate'=>$request->deliveryDate,
            'goodsOrigin'=>$request->goodsOrigin,
            'status'=>0,
            'remarks'=>$request->remarks,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);

        if($str)
        {
            Session::flash('message','New order information Successfully Inserted. ');
        }else{
            Session::flash('failedMessage','New order information Insertion Failed.');
        }

        }catch(QueryException $e){
           
            Session::flash('failedMessage','PI number cannot be duplicated.');
        }
        
        return redirect('/order_list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orders=DB::table('tborders')
        ->leftJoin('tbcustomers','tbcustomers.id','=','tborders.customerId')
        ->leftJoin('users','users.id','=','tborders.createdBy')
        ->select('tborders.*','users.name as created_by','tbcustomers.customerName','tbcustomers.customerPhone','tbcustomers.customerEmail','tbcustomers.customerAddress','tbcustomers.remarks')
        ->where('tborders.id','=',$id)
        ->get();

        $finish_parts=DB::table('tbfinish_part')
        ->leftJoin('tbbillcodeitems','tbbillcodeitems.id','=','tbfinish_part.billCodeItemId')
        ->leftJoin('tbbillcode','tbbillcode.id','=','tbbillcodeitems.billcodeId')
        ->leftJoin('tborders_details','tborders_details.id','=','tbbillcodeitems.bcOritem_id')
        ->leftJoin('tborders','tborders.id','=','tbbillcodeitems.bcPi_number')
        ->select('tbfinish_part.*','tbbillcode.billcode','tbbillcode.bDate','tborders_details.item','tborders_details.color','tborders.pi')
        ->where('tbbillcodeitems.bcPi_number', $id)
        ->get();
        
        $billcodeitems=DB::table('tbbillcodeitems')
        ->leftJoin('users','users.id','=','tbbillcodeitems.createdBy')
        ->leftJoin('tborders','tborders.id','=','tbbillcodeitems.bcPi_number')
        ->leftJoin('tbbillcode','tbbillcode.id','=','tbbillcodeitems.billcodeId')
        ->leftJoin('tborders_details','tborders_details.id','=','tbbillcodeitems.bcOritem_id')
        ->select('tbbillcodeitems.*','tbbillcode.billcode','tborders.pi','tborders_details.details','tborders_details.color','tborders_details.orderQuantity as bookingQuantity','users.name as created_by')
        ->where('tbbillcodeitems.bcPi_number','=',$id)
        ->get();

        $orderItems=DB::table('tborders_details')->where('rpiId','=',$id)->get();
        
        return view('orders.order_details',compact('orders','orderItems','billcodeitems','finish_parts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order=DB::table('tborders_details')->where('id','=',$id)->first();
//        return $order;
        return view('orders.modal.edit',compact('order'));
//        return $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
           'style'=>'required',
           'color'=>'required',
           'orderQuantity'=>'required',
           'unitPrice'=>'required',
        ]);
//        return $id;

        DB::table('tborders_details')->where('id','=',$id)->update([
            'details'=>$request->details,
            'style'=>$request->style,
            'color'=>$request->color,
            'orderQuantity'=>$request->orderQuantity,
            'delivered_quantity'=>0,
            'unitPrice'=>$request->unitPrice,

        ]);


        Session::flash('message','Order information updated.');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function add_item_to_order(Request $request)
    {
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tborders_details')->insert([
            'rpiId'=>$request->orderId,
            'item'=>$request->item,
            'style'=>$request->style,
            'details'=>$request->details,
            'color'=>$request->color,
            'orderQuantity'=>$request->itemQuantity,
            'unitPrice'=>$request->unitPrice,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);

       if($str)
        {
            Session::flash('message','New Item Successfully Added. ');
        }else{
            Session::flash('failedMessage','New Item information Insertion Failed.');
        }

        return redirect()->back();
    }


    public function change_order_status(Request $request)
    {
        $str=DB::table('tborders')->where(['id'=>$request->orderId])->update([
            'status'=>$request->status,
        ]);

       if($str)
        {
            Session::flash('message','Status Successfully Updated. ');
        }else{
            Session::flash('failedMessage','Status Updation Failed.');
        }

        return redirect()->back();
    }


    public function get_item_by_pi($id)
    {
        $item_by_pi=DB::table('tborders_details')->where('rpiId','=',$id)->get();
        return view('billcodes.item_by_pi',compact('item_by_pi'));
    }
    
    public function download_pdf($id)
    {
        $orders=DB::table('tborders')
        ->leftJoin('tbbank_info','tbbank_info.id','=','tborders.bankId')
        ->leftJoin('tbcustomers','tbcustomers.id','=','tborders.customerId')
        ->leftJoin('users','users.id','=','tborders.createdBy')
        ->select('tborders.*','tbbank_info.bankName','tbbank_info.bankAccountNo','tbbank_info.bankSwiftCode','tbbank_info.bankVatNo','users.name as created_by','tbcustomers.customerName','tbcustomers.customerPhone','tbcustomers.customerEmail','tbcustomers.customerAddress','tbcustomers.remarks')
        ->where('tborders.id','=',$id)
        ->first();

        $orderItems=DB::table('tborders_details')->where('rpiId','=',$id)->get();
        
        // return view('pdf.order_details_pdf',compact('orders','orderItems'));

        $pdf=PDF::loadView('pdf.order_details_pdf',compact('orders','orderItems'));
        $filename="PI_".$orders->pi.".pdf";
        return $pdf->download($filename);
    }

}

<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class BillcodeItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        return $request->all();
//        return base64_decode($request->billcodeId);

        $this->validate($request,[
            'billcodeId'=>'required',
            'bcPi_number'=>'required',
            'bcOritem_id'=>'required',
            'bcQuantity'=>'required',
            'bcUnitPrice'=>'required'
        ]);

        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbbillcodeitems')->insert([
            'billcodeId'=>$request->billcodeId,
//            'billcodeId'=>base64_decode($request->billcodeId),
            'bcPi_number'=>$request->bcPi_number,
            'bcOritem_id'=>$request->bcOritem_id,
            'bcQuantity'=>$request->bcQuantity,
            'bcUnitPrice'=>$request->bcUnitPrice,
            'bcFrom'=>$request->bcFrom,
            'bcTo'=>$request->bcTo,
            'bcRemarks'=>$request->bcRemarks,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);

        if($str)
        {
            Session::flash('message','Item has been successfully added to billcode. ');
        }else{
            Session::flash('failedMessage','Failed.');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}

<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class DepartmentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        $departments=DB::table('tbdepartments')
        ->leftJoin('users','users.id','=','tbdepartments.createdBy')
        ->select('tbdepartments.*','users.name as created_by')
        ->get();
        return view('department.index',compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request,[
            'departmentName'=>'required'
        ]);
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbdepartments')->insert([
            'departmentName'=>$request->departmentName,
            'departmentDescription'=>$request->departmentDescription,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);

        if($str)
        {
            Session::flash('message','Department Inserted Successfully');
        }else{
            Session::flash('failedMessage','Department Insertion Failed.');
        }
        
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'departmentName'=>'required'
        ]);
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbdepartments')->where(['id'=>$id])->update([
            'departmentName'=>$request->departmentName,
            'departmentDescription'=>$request->departmentDescription,
            'createdBy'=>$user->id,
            'updated_at'=>$now,
        ]);
        if($str)
        {
            Session::flash('message','Department information updated successfully');
        }else{
            Session::flash('failedMessage','Department Updation Failed.');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check=DB::table('users')->where(['departmentId'=>$id])->get();
        if(count($check)){
            Session::flash('failedMessage',"Deletion failed. There are already some employees in this department." );
        }
        else {
            $dept=DB::table('tbdepartments')->where(['id'=>$id])->delete();
            Session::flash('message','Department Successfully Deleted.');
        }
        return redirect()->back();
    }
}

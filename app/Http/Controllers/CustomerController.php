<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers=DB::table('tbcustomers')
        ->leftJoin('users','users.id','=','tbcustomers.createdBy')
        ->select('tbcustomers.*','users.name as created_by')
        ->get();
        return view('customers.index',compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
            'customerName'=>'required',
            'customerPhone'=>'required'
        ]);
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbcustomers')->insert([
            'customerName'=>$request->customerName,
            'customerPhone'=>$request->customerPhone,
            'customerEmail'=>$request->customerEmail,
            'customerAddress'=>$request->customerAddress,
            'remarks'=>$request->remarks,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);

        if($str)
        {
            Session::flash('message','Customer information Successfully Inserted. ');
        }else{
            Session::flash('failedMessage','Customer information Insertion Failed.');
        }
        
        return redirect('/customer_list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { 
        $customers=DB::table('tbcustomers')
        ->leftJoin('users','users.id','=','tbcustomers.createdBy')
        ->select('tbcustomers.*','users.name as created_by')
        ->where('tbcustomers.id','=',$id)
        ->get();
        return view('customers.details',compact('customers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customers=DB::table('tbcustomers')
        ->leftJoin('users','users.id','=','tbcustomers.createdBy')
        ->select('tbcustomers.*','users.name as created_by')
        ->where('tbcustomers.id','=',$id)
        ->first();
        return view('customers.edit',compact('customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request,[
            'customerName'=>'required',
            'customerPhone'=>'required'
        ]);
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbcustomers')->where(['id'=>$id])->update([
            'customerName'=>$request->customerName,
            'customerPhone'=>$request->customerPhone,
            'customerEmail'=>$request->customerEmail,
            'customerAddress'=>$request->customerAddress,
            'remarks'=>$request->remarks,
            'createdBy'=>$user->id,
            'updated_at'=>$now,

        ]);

        if($str)
        {
            Session::flash('message','Customer Information Successfully Updated. ');
        }else{
            Session::flash('failedMessage','Customer Information Updation Failed.');
        }
        
        return redirect('/customer_list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check=DB::table('tborders')->where(['customerId'=>$id])->get();
        if(count($check)){
            Session::flash('failedMessage',"Deletion failed. There are already some order for this customer ." );
        }
        else {
            $dept=DB::table('tbcustomers')->where(['id'=>$id])->delete();
            Session::flash('message','Customer Information Successfully Deleted.');
        }
        return redirect()->back();
    }
}

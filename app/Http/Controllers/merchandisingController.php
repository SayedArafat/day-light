<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use PDF;

class merchandisingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function production_sheet()
    {
        return view('merchandising.production_sheet');
    }

    public function production_sheet_data(Request $request)
    {
        $production_sheets=DB::table('tborders')
        ->leftJoin('tbcustomers','tbcustomers.id','=','tborders.customerId')
        ->leftJoin('tborders_details','tborders.id','=','tborders_details.rpiId')
        ->select('tborders.pi','tbcustomers.customerName','tborders_details.style','tborders_details.color','tborders_details.item','tborders_details.orderQuantity','tborders.orderDate','tborders.deliveryDate','tborders.shippingDate','tborders.sources','tborders.status','tborders.orderDate','tborders_details.id as orderItemId')
        ->whereBetween('tborders.orderDate', [$request->start_date, $request->end_date])
        ->get();
        foreach($production_sheets as $production_sheet){
            $data=DB::table('tbfinish_part')
                ->leftJoin('tbbillcodeitems','tbbillcodeitems.id','=','tbfinish_part.billCodeItemId')
                ->leftJoin('tborders_details','tborders_details.id','=','tbbillcodeitems.bcOritem_id')
                ->select(DB::raw("SUM(tbfinish_part.twill) as twillsum"),DB::raw("SUM(tbfinish_part.tc) as tcsum"))
                ->where('tborders_details.id','=',$production_sheet->orderItemId)
                ->first();
                $production_sheet->twillsum=$data->twillsum;
                $production_sheet->tcsum=$data->tcsum;
        }

        return view('merchandising.production_sheet_data',compact('production_sheets'));
    }

    public function cost_sheet()
    {
        return view('merchandising.cost_sheet');
    }

    public function greige_chart_view()
    {
        return view('merchandising.greige_chart_view');
    }

    public function greige_chart_data(Request $request)
    {
        $greige_charts=DB::table('tbbillcodeitems')
        ->leftJoin('tbbillcode','tbbillcode.id','=','tbbillcodeitems.billcodeId')
        ->leftJoin('tborders_details','tborders_details.id','=','tbbillcodeitems.bcOritem_id')
        ->select('tbbillcodeitems.*','tbbillcode.billcode','tbbillcode.bDate','tborders_details.item')
        ->whereBetween('tbbillcode.bDate', [$request->start_date, $request->end_date])
        ->get();

        if(($request->submit_type)=='preview'){
            return view('merchandising.greige_chart_data',compact('greige_charts','request'));
        }elseif(($request->submit_type)=='pdf'){
            // return View('pdf.greige_chart_data_pdf',compact('greige_charts','request'));
            $pdf=PDF::loadView('pdf.greige_chart_data_pdf',compact('greige_charts','request'));
            $filename=time()."_greige_chart.pdf";
            return $pdf->download($filename);
        }else{
            return redirect('/error');
        }

    }

    public function stock_fabric()
    {
        return view('merchandising.stock_fabric.index');
    }

    public function stock_fabric_data(Request $request)
    {
        $stock_fabrics=DB::table('tbstock_fabric')
        ->leftJoin('users','users.id','=','tbstock_fabric.createdBy')
        ->select('tbstock_fabric.*','users.name as created_by')
        ->whereBetween('tbstock_fabric.sfDate', [$request->start_date, $request->end_date])
        ->get();

        if(($request->submit_type)=='preview'){
            return view('merchandising.stock_fabric.stock_fabric_data',compact('stock_fabrics','request'));
        }elseif(($request->submit_type)=='pdf'){
            // return View('pdf.greige_chart_data_pdf',compact('greige_charts','request'));
            $pdf=PDF::loadView('pdf.stock_fabric_data_pdf',compact('stock_fabrics','request'));
            $filename=time()."_stock_fabrics.pdf";
            return $pdf->download($filename);
        }else{
            return redirect('/error');
        }
    }

    public function new_stock_fabric()
    {
        return view('merchandising.stock_fabric.create');
    }

    public function stock_fabric_store(Request $request)
    {
        
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbstock_fabric')->insert([
            'sfRef'=>$request->sfRef,
            'sfDate'=>$request->sfDate,
            'sfItem'=>$request->sfItem,
            'sfColor'=>$request->sfColor,
            'sfPi'=>$request->sfPi,
            'sfQty'=>$request->sfQty,
            'sfDetails'=>$request->sfDetails,
            'sfRemarks'=>$request->sfRemarks,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);

        if($str)
        {
            Session::flash('message','Stock information Successfully Inserted. ');
        }else{
            Session::flash('failedMessage','Stock information Insertion Failed.');
        }
        
        return redirect('/new_stock_fabric');
    }

}

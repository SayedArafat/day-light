<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class advancePaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees=DB::table('users')->get();
        $advance_payment=DB::table('tbadvance_payment')
        ->leftJoin('users as createBy','createBy.id','=','tbadvance_payment.createdBy')
        ->leftJoin('users as employeeInfo','employeeInfo.id','=','tbadvance_payment.empId')
        ->select('tbadvance_payment.*','createBy.name as created_by','employeeInfo.name as employeeName')
        ->get();
        return view('advance_payment.index',compact('advance_payment','employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $this->validate($request,[
            'employeeId'=>'required',
            'advanceDate'=>'required',
            'advanceAmount'=>'required'
        ]);
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbadvance_payment')->insertGetId([
            'empId'=>$request->employeeId,
            'remarks'=>$request->remarks,
            'paymentDate'=>Carbon::parse($request->advanceDate)->format('Y-m-d'),
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);

        $str=DB::table('tbadvance_payment_history')->insert([
            'advancePaymentId'=>$str,
            'paymentType'=>'1',
            'amount'=>$request->advanceAmount,
            'aphDate'=>Carbon::parse($request->advanceDate)->format('Y-m-d'),
            'remarks'=>NULL,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);
        if($str)
        {
            Session::flash('message','Advance Payment Information Successfully Inserted ');
        }else{
            Session::flash('failedMessage','Advance Payment Information Insertion Failed.');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paymentAmount = DB::table('tbadvance_payment_history')->where([['advancePaymentId','=',$id],['paymentType','=','1']])->sum('amount');
        $paymentReturnAmount = DB::table('tbadvance_payment_history')->where([['advancePaymentId','=',$id],['paymentType','=','2']])->sum('amount');
        $loanAmounts=DB::table('tbadvance_payment_history')
        ->where([['tbadvance_payment_history.advancePaymentId','=',$id],['tbadvance_payment_history.paymentType','=',1]])
        ->leftJoin('users','users.id','=','tbadvance_payment_history.createdBy')
        ->select('tbadvance_payment_history.*','users.name as created_by')
        ->get();

        $loanReturnAmounts=DB::table('tbadvance_payment_history')
        ->where([['tbadvance_payment_history.advancePaymentId','=',$id],['tbadvance_payment_history.paymentType','=',2]])
        ->leftJoin('users','users.id','=','tbadvance_payment_history.createdBy')
        ->select('tbadvance_payment_history.*','users.name as created_by')
        ->get();

        
        $advance_payments=DB::table('tbadvance_payment')
        ->leftJoin('users as createBy','createBy.id','=','tbadvance_payment.createdBy')
        ->leftJoin('users as employeeInfo','employeeInfo.id','=','tbadvance_payment.empId')
        ->select('tbadvance_payment.*','createBy.name as created_by','employeeInfo.name as employeeName','employeeInfo.email as email','employeeInfo.phone as phone','employeeInfo.currentAddress as currentAddress')
        ->get();

        $advancePaymentId = $id;

        return view('advance_payment.advance_payment_details',compact('advance_payments','loanAmounts','loanReturnAmounts','paymentAmount','paymentReturnAmount','advancePaymentId'));
 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function advanceNewExpense(Request $request)
    {
      
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbadvance_payment_history')->insert([
            'advancePaymentId'=>$request->advancePaymentId,
            'expenseTitle'=>$request->expenseTitle,
            'categoryName'=>$request->categoryName,
            'paymentType'=>'2',
            'amount'=>$request->amount,
            'aphDate'=>Carbon::parse($request->aphDate)->format('Y-m-d'),
            'remarks'=>$request->remarks,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);

        if($str)
        {
            Session::flash('message','Expense Information Successfully Inserted ');
        }else{
            Session::flash('failedMessage','Expense Information Insertion Failed.');
        }
        return redirect()->back();
    }

    public function advanceNewPayment(Request $request)
    {
      
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbadvance_payment_history')->insert([
            'advancePaymentId'=>$request->advancePaymentId,
            'paymentType'=>'1',
            'amount'=>$request->amount,
            'aphDate'=>Carbon::parse($request->aphDate)->format('Y-m-d'),
            'remarks'=>$request->remarks,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);

        if($str)
        {
            Session::flash('message','New Payment Information Successfully Inserted ');
        }else{
            Session::flash('failedMessage','New Payment Information Insertion Failed.');
        }
        return redirect()->back();
    }
}

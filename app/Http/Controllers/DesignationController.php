<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class DesignationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        $designations=DB::table('tbdesignations')
        ->leftJoin('users','users.id','=','tbdesignations.createdBy')
        ->select('tbdesignations.*','users.name as created_by')
        ->get();
        return view('designation.index',compact('designations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request,[
            'designationName'=>'required'
        ]);
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbdesignations')->insert([
            'designationName'=>$request->designationName,
            'designationDescription'=>$request->designationDescription,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);
        if($str)
        {
            Session::flash('message','Designation Successfully Inserted ');
        }else{
            Session::flash('failedMessage','Designation Insertion Failed.');
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'designationName'=>'required'
        ]);
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbdesignations')->where(['id'=>$id])->update([
            'designationName'=>$request->designationName,
            'designationDescription'=>$request->designationDescription,
            'createdBy'=>$user->id,
            'updated_at'=>$now,

        ]);
        if($str)
        {
            Session::flash('message','Designation Successfully Updated ');
        }else{
            Session::flash('failedMessage','Designation Updation Failed.');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check=DB::table('users')->where(['designationId'=>$id])->get();
        if(count($check)){
            Session::flash('failedMessage',"Deletion failed. There are some others data already using this resource." );
        }
        else {
            $dept=DB::table('tbdesignations')->where(['id'=>$id])->delete();
            Session::flash('message','Designation Successfully Deleted.');
        }
        return redirect()->back();
    }
}

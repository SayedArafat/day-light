<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pendingOrder=DB::table('tborders')->where('status','=','0')->count();
        $completedOrder=DB::table('tborders')->where('status','=','1')->count();

        $users=DB::table('users')->count();

        $gloans=DB::table('tbgive_loan')
        ->leftJoin('users','users.id','=','tbgive_loan.createdBy')
        ->select('tbgive_loan.*','users.name as created_by')
        ->take(5)
        ->orderBy('tbgive_loan.loanDate', 'DESC')
        ->get();


        $rloans=DB::table('tbreceived_loan')
        ->leftJoin('users','users.id','=','tbreceived_loan.createdBy')
        ->select('tbreceived_loan.*','users.name as created_by')
        ->take(5)
        ->orderBy('tbreceived_loan.loanDate', 'DESC')
        ->get();

        $advances=DB::table('tbadvance_payment')
        ->leftJoin('users as createBy','createBy.id','=','tbadvance_payment.createdBy')
        ->leftJoin('users as employeeInfo','employeeInfo.id','=','tbadvance_payment.empId')
        ->select('tbadvance_payment.*','createBy.name as created_by','employeeInfo.name as employeeName')
        ->take(5)
        ->orderBy('tbadvance_payment.paymentDate', 'DESC') 
        ->get();


        return view('dashboard',compact('users','gloans','rloans','advances','pendingOrder','completedOrder'));

    }
    public function index2()
    {
        return view('home');
    }
}

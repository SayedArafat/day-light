<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Mpdf\Mpdf;
use PDF;

class CommercialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers=DB::table('tbcustomers')->get();
        $companies=DB::table('tbcompany_info')->get();
        $bank_info=DB::table('tbbank_info')->get();
        return view('commercial.new_lc',compact('customers','bank_info','companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tblc_info')->insertGetId([
            'lcNumber'=>$request->lcNumber,
            'lcDate'=>$request->lcDate,
            'challanNo'=>$request->challanNo,
            'companyId'=>$request->companyId,
            'companyBankId'=>$request->companyBankId,
            'customerId'=>$request->customerId,
            'customerBankName'=>$request->customerBankName,
            'customerBankInfo'=>$request->customerBankInfo,
            'sightDays'=>$request->sightDays,
            'contractNo'=>$request->contractNo,
            'contractDate'=>$request->contractDate,
            'valueReceived'=>$request->valueReceived,
            'IRCNo'=>$request->IRCNo,
            'TINNo'=>$request->TINNo,
            'VatRegNo'=>$request->VatRegNo,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);

        if($str)
        {
            Session::flash('message','New L/C Successfully Generated. ');
        }else{
            Session::flash('failedMessage','L/C Generation Failed.');
        }
        
        return redirect('/lc_details/'.base64_encode($str));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = base64_decode($id);
        $lc_details = DB::table('tblc_info')
        ->leftJoin('tbbank_info','tbbank_info.id','=','tblc_info.companyBankId')
        ->leftJoin('tbcustomers','tbcustomers.id','=','tblc_info.customerId')
        ->leftJoin('tbcompany_info','tbcompany_info.id','=','tblc_info.companyId')
        ->leftJoin('users','users.id','=','tblc_info.createdBy')
        ->select('tblc_info.*','tbbank_info.bankName','tbbank_info.bankAccountNo','tbcustomers.customerName','users.name as created_by')
        ->where('tblc_info.id','=',$id)
        ->first();
        // dd($lc_details);
        return view('commercial.lc_details',compact('lc_details'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function negotiation_application_pdf($id)
    {
        $id=base64_decode($id);
        $lc_details = DB::table('tblc_info')
        ->leftJoin('tbbank_info','tbbank_info.id','=','tblc_info.companyBankId')
        ->leftJoin('tbcustomers','tbcustomers.id','=','tblc_info.customerId')
        ->leftJoin('tbcompany_info','tbcompany_info.id','=','tblc_info.companyId')
        ->leftJoin('users','users.id','=','tblc_info.createdBy')
        ->select('tblc_info.*','tbbank_info.bankName','tbbank_info.bankAccountNo','tbbank_info.bankAddress','tbcustomers.customerName','tbcustomers.customerAddress','users.name as created_by')
        ->where('tblc_info.id','=',$id)
        ->first();

        $company_info=DB::table('tbcompany_info')->where('id','=',$lc_details->companyId)->first();
        // dd($lc_details);
        // return view('pdf.commercial.negotiation_application_pdf',compact('lc_details','company_info'));
        $pdf=PDF::loadView('pdf.commercial.negotiation_application_pdf',compact('lc_details','company_info'));
//        $pdf=PDF::loadView('pdf.commercial.test');
        $pdf->showImageErrors = true;
        $filename=time()."_negotiation_application.pdf";
//        return view('pdf.commercial.negotiation_application_pdf',compact('lc_details','company_info'));
//        return view('pdf.commercial.test');
//        return $pdf->output($filename);
        $pdf->debug = true;
        return $pdf->download($filename);

    }

    public function bill_of_exchange_pdf($id)
    {
        $id=base64_decode($id);
        $lc_details = DB::table('tblc_info')
        ->leftJoin('tbbank_info','tbbank_info.id','=','tblc_info.companyBankId')
        ->leftJoin('tbcustomers','tbcustomers.id','=','tblc_info.customerId')
        ->leftJoin('tbcompany_info','tbcompany_info.id','=','tblc_info.companyId')
        ->leftJoin('users','users.id','=','tblc_info.createdBy')
        ->select('tblc_info.*','tbbank_info.bankName','tbbank_info.bankAccountNo','tbbank_info.bankAddress','tbcustomers.customerName','tbcustomers.customerAddress','users.name as created_by')
        ->where('tblc_info.id','=',$id)
        ->first();
        $date=Carbon::now()->toDateString();

        $html="<!DOCTYPE html>
<html>
<head>
	<title>Bill of Exchange</title>
</head>
<body>
	<style type='text/css'>
		*{
			padding: 0;
			margin: 0;
		}
		.table2{
			border-collapse: collapse;
			width: 100%;

		}

		.table2 td{
			font-size: 12px;
			padding: 10px 5px;
		}
		.table2 th{
			font-size: 11px;
			padding: 3px 1px;
		}
		.table3{
			border-collapse: collapse;
			width: 100%;
			font-size: 13px;
		}
		.table1{
			text-align: left;
			width: 100%;
		}
		.caddress{
			font-size: 11px;
			width: 80%;
		}
		.ptext{
			line-height: 25px;
		}
		.tag{
			width: 2%;
		}
		.imptext{
			font-weight: bold;
			text-decoration: underline;
		}
	</style>

	<table class='table2'>
		<tr>
			<td class='tag'>
				<h1>Daylight</h1>
			</td>
			<td width='2%'></td>
			<td>
				<table class='table3'>
					<tr>
						<td colspan='2' style='text-align:center;'><h2>BILL OF EXCHANGE</h2></td>
					</tr>
					<tr>
						<td>
							No: $lc_details->challanNo
						</td>
						<td style='text-align:right;'>
							Date: $date
						</td>
					</tr>
					<tr>
						<td colspan='2' class='ptext' style='text-align:justify;'>
							Documentary Credit No. <span class='imptext'>$lc_details->lcNumber, Date: $lc_details->lcDate</span> Of <span class='imptext'>$lc_details->customerBankName,$lc_details->customerBankInfo.</span> Exchange for <span class='imptext'>US$00.00</span> at <span class='imptext'>$lc_details->sightDays</span> Days Sight of this First Of Exchange (second of the Same Tenor and Date being unpaid)  <span class='imptext'>Under the export Contract no. $lc_details->contractNo DATED $lc_details->contractDate</span>. Pay to The Order of <span class='imptext'>$lc_details->customerName.</span> Sum of <span class='imptext'>U.S. Dollar $00.00</span> , Value Received <span class='imptext'>$lc_details->valueReceived</span> Drawn Under $lc_details->bankName, $lc_details->bankAddress.
						</td>
					</tr>
					<tr>
						<td colspan='2'>
							<b>To of $lc_details->customerName, $lc_details->customerAddress</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<br />
	<br />
	<br />
	<br />
	<br />
	<br />

	<table class='table2'>
		<tr>
			<td class='tag'>
				<h1>Daylight</h1>
			</td>
			<td width='2%'></td>
			<td>
				<table class='table3'>
					<tr>
						<td colspan='2' style='text-align:center;'><h2>BILL OF EXCHANGE</h2></td>
					</tr>
					<tr>
						<td>
							No: $lc_details->challanNo
						</td>
						<td style='text-align:right;'>
							Date:$date
						</td>
					</tr>
					<tr>
						<td colspan='2' class='ptext' style='text-align:justify;'>
							Documentary Credit No. <span class='imptext'>$lc_details->lcNumber, Date: $lc_details->lcDate</span> Of <span class='imptext'>{{$lc_details->customerBankName}},$lc_details->customerBankInfo.</span> Exchange for <span class='imptext'>US$00.00</span> at <span class='imptext'>$lc_details->sightDays</span> Days Sight of this First Of Exchange (second of the Same Tenor and Date being unpaid)  <span class='imptext'>Under the export Contract no. $lc_details->contractNo DATED $lc_details->contractDate</span>. Pay to The Order of <span class='imptext'>$lc_details->customerName.</span> Sum of <span class='imptext'>U.S. Dollar $00.00</span> , Value Received <span class='imptext'>$lc_details->valueReceived</span> Drawn Under $lc_details->bankName, $lc_details->bankAddress.
						</td>
					</tr>
					<tr>
						<td colspan='2'>
							<b>To of $lc_details->customerName, $lc_details->customerAddress</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</body>
</html>";
$mpdf = new mPDF();
$mpdf->WriteHTML($html);

//call watermark content aand image
$mpdf->SetWatermarkText('Day Light');
$mpdf->showWatermarkText = true;
$mpdf->watermarkTextAlpha = 0.3;
$filename=time()."_bill_of_exchange_pdf.pdf";


//save the file put which location you need folder/filname
$mpdf->output($filename, 'D');


//out put in browser below output function
//$mpdf->Output();

//        $company_info=DB::table('tbcompany_info')->where('id','=',$lc_details->companyId)->first();
//        // dd($lc_details);
//        // return view('pdf.commercial.bill_of_exchange_pdf',compact('lc_details','company_info'));
//        $pdf=PDF::loadView('pdf.commercial.bill_of_exchange_pdf',compact('lc_details','company_info'));
//        $filename=time()."_bill_of_exchange_pdf.pdf";
//        return $pdf->download($filename);
    }

    public function lc_list_view()
    {
        return view('commercial.lc_view');
    }

    public function lc_list_data(Request $request)
    {
        $lc_list = DB::table('tblc_info')
        ->leftJoin('tbbank_info','tbbank_info.id','=','tblc_info.companyBankId')
        ->leftJoin('tbcustomers','tbcustomers.id','=','tblc_info.customerId')
        ->leftJoin('tbcompany_info','tbcompany_info.id','=','tblc_info.companyId')
        ->leftJoin('users','users.id','=','tblc_info.createdBy')
        ->select('tblc_info.*','tbbank_info.bankName','tbbank_info.bankAccountNo','tbcustomers.customerName','users.name as created_by')
        ->whereBetween('tblc_info.lcDate', [$request->start_date, $request->end_date])
        ->get();
        // dd($lc_list);
        if(($request->submit_type)=='preview'){
        return view('commercial.lc_list_data',compact('lc_list','request'));
        }elseif(($request->submit_type)=='pdf'){
            // return View('pdf.lc_list_data_pdf',compact('lc_list','request'));
            $pdf=PDF::loadView('pdf.lc_list_data_pdf',compact('lc_list','request'));
            $filename=time()."_lc_list.pdf";
            return $pdf->download($filename);
        }else{
            return redirect('/error');
        }
    }
}

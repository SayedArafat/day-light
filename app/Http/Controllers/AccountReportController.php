<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class AccountReportController extends Controller
{
    function __construct()
    {

    }
    public function advance_balance(){
        $employees=DB::table('users')->pluck('name','id')->all();
        return view('account_report.advance_balance',compact('employees'));
//        return "Advance Balance";
    }
    public function advance_balance_data(Request $request){

        if ($request->emp_id == 0) {
            $advance_payments = DB::table('tbadvance_payment_history')
                ->join('tbadvance_payment', 'tbadvance_payment_history.advancePaymentId', '=', 'tbadvance_payment.id')
                ->join('users', 'tbadvance_payment.empId', '=', 'users.id')
                ->select('tbadvance_payment_history.id', 'tbadvance_payment_history.paymentType', 'tbadvance_payment_history.amount',
                    'tbadvance_payment_history.aphDate', 'users.name')
                ->orderBy('tbadvance_payment.paymentDate')
                ->whereBetween('tbadvance_payment.paymentDate', [$request->start_date, $request->end_date])
                ->get();
        } else {
            $advance_payments = DB::table('tbadvance_payment_history')
                ->join('tbadvance_payment', 'tbadvance_payment_history.advancePaymentId', '=', 'tbadvance_payment.id')
                ->join('users', 'tbadvance_payment.empId', '=', 'users.id')
                ->select('tbadvance_payment_history.id', 'tbadvance_payment_history.paymentType', 'tbadvance_payment_history.amount',
                    'tbadvance_payment_history.aphDate', 'users.name')
                ->orderBy('tbadvance_payment.paymentDate')
                ->where('users.id', '=', $request->emp_id)
                ->whereBetween('tbadvance_payment.paymentDate', [$request->start_date, $request->end_date])
                ->get();

        }
        if($request->submit_type=="preview") {
//        return $advance_payments;
            return view('account_report.advance_balance_data', compact('advance_payments','request'));
        }
        else{
            $pdf = PDF::loadView('pdf.advance_balance_pdf', compact('advance_payments','request'));
            $name = time() . "_advance_balance_report.pdf";
            return $pdf->download($name);

        }

    }

    public function net_balance(){
        return "KK";
    }

    public function loans_report(){
        return view('account_report.loans_report');
    }
    public function loans_report_data(Request $request){
//        return $request->all();
        if($request->loan_type=="received"){
//            $given_loan=DB::table('tbreceived_loan_history')
//                ->leftJoin('tbreceived_loan','loanId','=','tbreceived_loan.id')
//                ->whereBetween('tbreceived_loan.loanDate',[$request->start_date,$request->end_date])
//                ->get();
            $received_loan=DB::table('tbreceived_loan')
                ->whereBetween('tbreceived_loan.loanDate',[$request->start_date,$request->end_date])
                ->get();
            if($request->submit_type=="preview") {
//            return "kk";
                return view('account_report.received_loans_report_data', compact('received_loan', 'request'));
            }
            elseif ($request->submit_type=="pdf"){
                $pdf = PDF::loadView('pdf.received_loan_pdf', compact('received_loan','request'));
                $name = time() . "_received_loan_report.pdf";
                return $pdf->download($name);
            }

        }
        elseif ($request->loan_type=="given"){
            $given_loan=DB::table('tbgive_loan')
                ->whereBetween('tbgive_loan.loanDate',[$request->start_date,$request->end_date])
                ->get();
            if($request->submit_type=="preview") {
//            return "kk";
                return view('account_report.loans_report_data', compact('given_loan', 'request'));
            }
            elseif ($request->submit_type=="pdf"){
                $pdf = PDF::loadView('pdf.loan_pdf', compact('given_loan','request'));
                $name = time() . "given_loan_report.pdf";
                return $pdf->download($name);
            }
        }

    }
}

<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AjaxController extends Controller
{
    public function get_billcodes_by_pi($id)
    {
        $billcodes_by_pi=DB::table('tbbillcodeitems')
        ->leftJoin('tbbillcode','tbbillcode.id','=','tbbillcodeitems.billcodeId')
        ->select('tbbillcodeitems.billcodeId','tbbillcode.billcode')
        ->where('tbbillcodeitems.bcPi_number','=',$id)
        ->distinct('tbbillcode.billcode')
        ->get();

        return view('ajax.billcodes_by_pi',compact('billcodes_by_pi'));
    }

    public function get_items_by_billcode($id)
    {
        $items_by_billcode=DB::table('tbbillcodeitems')
        ->leftJoin('users','users.id','=','tbbillcodeitems.createdBy')
        ->leftJoin('tborders','tborders.id','=','tbbillcodeitems.bcPi_number')
        ->leftJoin('tborders_details','tborders_details.id','=','tbbillcodeitems.bcOritem_id')
        ->select('tbbillcodeitems.*','tborders.pi','tborders_details.details','tborders_details.color','tborders_details.style','tborders_details.orderQuantity as bookingQuantity','users.name as created_by')
        ->where('tbbillcodeitems.billcodeId','=',$id)
        ->get();

        return view('ajax.items_by_billcode',compact('items_by_billcode'));
    }
}

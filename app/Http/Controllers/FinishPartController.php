<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use PDF;

class FinishPartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dyeing.index');
    }

    public function finish_part_data(Request $request)
    {
        $finish_parts=DB::table('tbfinish_part')
        ->leftJoin('tbbillcodeitems','tbbillcodeitems.id','=','tbfinish_part.billCodeItemId')
        ->leftJoin('tbbillcode','tbbillcode.id','=','tbbillcodeitems.billcodeId')
        ->leftJoin('tborders_details','tborders_details.id','=','tbbillcodeitems.bcOritem_id')
        ->leftJoin('tborders','tborders.id','=','tbbillcodeitems.bcPi_number')
        ->select('tbfinish_part.*','tbbillcode.billcode','tbbillcode.bDate','tborders_details.item','tborders_details.color','tborders.pi')
        ->whereBetween('tbfinish_part.fDate', [$request->start_date, $request->end_date])
        ->get();

        if(($request->submit_type)=='preview'){
            return view('dyeing.dyeing_chart_data',compact('finish_parts','request'));
        }elseif(($request->submit_type)=='pdf'){
            // return View('pdf.greige_chart_data_pdf',compact('greige_charts','request'));
            $pdf=PDF::loadView('pdf.dyeing_chart_data_pdf',compact('finish_parts','request'));
            $filename=time()."_dyeing_chart.pdf";
            return $pdf->download($filename);
        }else{
            return redirect('/error');
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $orderPiList=DB::table('tborders')
        ->select('tborders.id','tborders.pi')
        ->where('status','=','0')
        ->get();

        return view('dyeing.create',compact('orderPiList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
            'billCodeItemId'=>'required',
            'fDate'=>'required'
        ]);

        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbfinish_part')->insert([
            'billCodeItemId'=>$request->billCodeItemId,
            'fDate'=>$request->fDate,
            'twill'=>$request->twill,
            'tc'=>$request->tc,
            'deliveryFactory'=>$request->deliveryFactory,
            'comment'=>$request->comment,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);

       if($str)
        {
            Session::flash('message','New Information Successfully Added. ');
        }else{
            Session::flash('failedMessage','New Information Insertion Failed.');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

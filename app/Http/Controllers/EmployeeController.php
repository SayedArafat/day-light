<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class EmployeeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $employees=DB::table('users')
            ->leftJoin('tbdesignations', 'users.designationId','=','tbdesignations.id')
            ->leftJoin('tbdepartments','users.departmentId','=','tbdepartments.id')
            ->select('users.*','tbdesignations.designationName','tbdepartments.departmentName')
            ->get();
        return view('employee.index',compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments=DB::table('tbdepartments')->get();
        $designations=DB::table('tbdesignations')->get();

        return view('employee.create',compact('departments','designations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[
            'name'=>'required',
            'userType'=>'required',
            'email'=>'required',
            'password'=>'required',
            'gender'=>'required',
            'departmentId'=>'required',
            'photo'=>'mimes:png,jpg,jpeg',
        ]);

        if($request->file('photo')) {
            if ($request->file('photo')->getClientSize() > 2000000) {
                Session::flash('fileSize', "Employee create failed. File Size Limit Exceeded");
                return redirect('employee/create');
            }
        }

        if($file=$request->file('photo')){
            $photo=time()."_".$file->getClientOriginalName();
            $file->move('Employee_Photo',$photo);
        }
        else{
            $photo="default.jpg";
        }


        $now=Carbon::now()->toDateTimeString();

        $user=Auth::user();
        $checkemail=DB::table('users')->where(['email'=>$request->email])->count();
        if($checkemail==0){
            $str=DB::table('users')->insert([
                'name'=>$request->name,
                'employeeNumber'=>$request->employeeNumber,
                'userType'=>$request->userType,
                'email'=>$request->email,
                'password'=>bcrypt($request->password),
                'phone'=>$request->phone,
                'gender'=>$request->gender,
                'accountStatus'=>'0',
                'currentAddress'=>$request->address,
                'joiningDate'=>Carbon::parse($request->joiningDate)->format('Y-m-d'),
                'designationId'=>$request->designationId,
                'departmentId'=>$request->departmentId,
                'salary'=>$request->salary,
                'photo'=>$photo,
                'createdBy'=>$user->id,
                'created_at'=>$now,
                'updated_at'=>$now,

            ]);

            if($str)
            {
                Session::flash('message','Employee information has been successfully added.');
            }else{
                Session::flash('failedMessage','Employee Insertion Failed.');
            }
        }else{
           Session::flash('failedMessage','Sorry, Email address already exits.');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function access_role()
    {
        $employees=DB::table('users')
            ->leftJoin('tbdesignations', 'users.designationId','=','tbdesignations.id')
            ->leftJoin('tbdepartments','users.departmentId','=','tbdepartments.id')
            ->select('users.*','tbdesignations.designationName','tbdepartments.departmentName')
            ->get();
        return view('employee.access_role',compact('employees')); 
    }
}

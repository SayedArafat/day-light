<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use PDF;

class billcodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $billcodes=DB::table('tbbillcode')
        ->leftJoin('users','users.id','=','tbbillcode.createdBy')
        ->select('tbbillcode.*','users.name as created_by')
        ->get();
        return view('billcodes.index',compact('billcodes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $billcodeData=DB::table('tbbillcode')->latest()->first();
        if($billcodeData){
            $billcode = ($billcodeData->billcode)+1;
        }else{
            $billcode = 101;
        }
        return view('billcodes.create',compact('billcode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[
            'billcode'=>'required',
            'bDate'=>'required'
        ]);

        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbbillcode')->insert([
            'billcode'=>$request->billcode,
            'bDate'=>Carbon::parse($request->bDate)->format('Y-m-d'),
            'remarks'=>$request->remarks,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,

        ]);

        if($str)
        {
            Session::flash('message','Billcode '.$request->billcode.' Successfully Generated. ');
        }else{
            Session::flash('failedMessage','Billcode Generation Failed.');
        }
        return redirect('/bill_code_list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        return $id;
        $billcodeitems=DB::table('tbbillcodeitems')
        ->leftJoin('users','users.id','=','tbbillcodeitems.createdBy')
        ->leftJoin('tborders','tborders.id','=','tbbillcodeitems.bcPi_number')
        ->leftJoin('tborders_details','tborders_details.id','=','tbbillcodeitems.bcOritem_id')
        ->select('tbbillcodeitems.*','tborders.pi','tborders_details.details','tborders_details.color','tborders_details.orderQuantity as bookingQuantity','users.name as created_by')
        ->where('tbbillcodeitems.billcodeId','=',base64_decode($id))
        ->get();

        $billcodeDetails=DB::table('tbbillcode')
        ->where('tbbillcode.id','=',base64_decode($id))
        ->first();
        
        $orderId=DB::table('tborders')
        ->select('tborders.id','tborders.pi')
        ->where('status','=','0')
        ->get();

        $billcodeId=$id;
        return view('billcodes.details',compact('orderId','billcodeId','billcodeitems','billcodeDetails'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'billcode'=>'required',
            'bDate'=>'required'
        ]);

        $now=Carbon::now()->toDateTimeString();
        $user=Auth::user();
        $str=DB::table('tbbillcode')->where(['id'=>$id])->update([
            'billcode'=>$request->billcode,
            'bDate'=>Carbon::parse($request->bDate)->format('Y-m-d'),
            'remarks'=>$request->remarks,
            'createdBy'=>$user->id,
            'updated_at'=>$now,

        ]);

        if($str)
        {
            Session::flash('message','Billcode '.$request->billcode.' Has Been Successfully Updated. ');
        }else{
            Session::flash('failedMessage','Billcode Updation Failed.');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function download_pdf($id)
    {
        
        $billcodeitems=DB::table('tbbillcodeitems')
        ->leftJoin('users','users.id','=','tbbillcodeitems.createdBy')
        ->leftJoin('tborders','tborders.id','=','tbbillcodeitems.bcPi_number')
        ->leftJoin('tborders_details','tborders_details.id','=','tbbillcodeitems.bcOritem_id')
        ->select('tbbillcodeitems.*','tborders.pi','tborders_details.details','tborders_details.color','tborders_details.orderQuantity as bookingQuantity','users.name as created_by')
        ->where('tbbillcodeitems.billcodeId','=',$id)
        ->get();

        $billcodeDetails=DB::table('tbbillcode')
        ->where('tbbillcode.id','=',$id)
        ->first();

        $totbcQuantity=0;
        $tunit=0;
        foreach($billcodeitems as $item){
            $totbcQuantity+=$item->bcQuantity;
            $tunit+=$item->bcUnitPrice;
        }
        $tunit=$tunit/$billcodeitems->count();
        $billcodeitems->totbcQuantity=$totbcQuantity;
        $billcodeitems->tunit=$tunit;
        $orderId=DB::table('tborders')
        ->select('tborders.id','tborders.pi')
        ->get();

        $billcodeId=$id;
        // dd($billcodeitems->tunit);
        // return view('billcodes.download_pdf',compact('orderId','billcodeId','billcodeitems','billcodeDetails'));
        $pdf=PDF::loadView('billcodes.download_pdf',compact('orderId','billcodeId','billcodeitems','billcodeDetails'));
        $filename=time()."_billcode.pdf";
        return $pdf->download($filename);
        
    }


}

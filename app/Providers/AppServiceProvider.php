<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Blade;
use DB;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        //

        Blade::directive('money', function ($amount) {
            return "<?php echo '৳ ' . number_format($amount, 2); ?>";
        });

        Blade::directive('dollarMoney', function ($amount) {
            return "<?php echo '$ ' . number_format($amount, 2); ?>";
        });

        Blade::directive('quantity', function ($quantity) {
            return "<?php echo ($quantity). ' yds'; ?>";
        });


        // Company Information
        $companyInfo=DB::table('tbcompany_info')->first();
        view()->share('companyInfo',$companyInfo);
 
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

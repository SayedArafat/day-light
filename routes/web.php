<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['uses'=>'HomeController@index']);
Route::get('/home',['uses'=>'HomeController@index2']);


Route::get('/tbl', function () {
    return view('employee.index');
});
Route::get('/logout', 'Auth\LoginController@logout');

Auth::routes();

Route::group(['middleware'=>'auth'], function () {
	
	Route::get('/dashboard',['middleware'=>'check-permission:user|admin|superadmin','uses'=>'HomeController@index']);
	
	//Manage Employee
	Route::resource('employee', 'EmployeeController');
	Route::get('/employee_list',['middleware'=>'check-permission:admin|superadmin','uses'=>'EmployeeController@index']);
	Route::get('/access_role',['middleware'=>'check-permission:admin|superadmin','uses'=>'EmployeeController@access_role']);
	Route::get('/employee/create',['middleware'=>'check-permission:admin|superadmin','uses'=>'EmployeeController@create']);
	
	//Manage Company
	Route::resource('companyInfo', 'CompanyInformation');
	Route::get('/CompanyInformation',['middleware'=>'check-permission:admin|superadmin','uses'=>'CompanyInformation@index'])->name('company_list');
	
	//Manage Designation
	Route::resource('designation', 'DesignationController');
	Route::get('/designation',['middleware'=>'check-permission:admin|superadmin','uses'=>'DesignationController@index']);
	
	//Manage Department
	Route::resource('department', 'DepartmentController');
	Route::get('/department',['middleware'=>'check-permission:admin|superadmin','uses'=>'DepartmentController@index']);
	Route::get('/department/create',['middleware'=>'check-permission:admin|superadmin','uses'=>'DepartmentController@create'])->name('department.add');
	Route::POST('/department/store',['middleware'=>'check-permission:admin|superadmin','uses'=>'DepartmentController@store'])->name('department.store');

	//Manage Bank Information
	Route::resource('bank', 'BankController');
	Route::get('/bank_list',['middleware'=>'check-permission:admin|superadmin','uses'=>'BankController@index']);
	Route::get('/bank/create',['middleware'=>'check-permission:admin|superadmin','uses'=>'BankController@create'])->name('bank.add');
	Route::POST('/bank/store',['middleware'=>'check-permission:admin|superadmin','uses'=>'BankController@store'])->name('bank.store');
	Route::get('/all_bank_list',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'BankController@all_bank_list']);
    Route::get('/bank_transaction_details/{id}',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'BankController@bank_transaction_details'])->name('bank.bank_transaction_details');    
	
	// Cash In (Deposit Money)
    Route::get('/cash_in',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'BankController@cash_in'])->name('cash_in');
	Route::POST('/cash_in_store',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'BankController@cash_in_store'])->name('bank.cash_in_store');

	// Cash out (Withdrow Money)
    Route::get('/cash_out',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'BankController@cash_out'])->name('cash_out');
	Route::POST('/cash_out_store',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'BankController@cash_out_store'])->name('bank.cash_out_store');

	Route::get('/transaction_history',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'BankController@transaction_history']);
	Route::POST('/transaction_history_data',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'BankController@transaction_history_data'])->name('transaction_history_data');

	//Manage Expenses
	Route::resource('expenses', 'ExpensesController');
	Route::get('/expenses',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'ExpensesController@index']);
	Route::get('/todays_expense_history',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'ExpensesController@todays_expenses']);
	Route::get('/new_expense',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'ExpensesController@create']);

	//Manage Expense Category
	Route::resource('expenseCategory', 'ExpenseCategoryController');
	Route::get('/expense_category',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'ExpenseCategoryController@index']);

	//Manage Income Category
	Route::resource('incomeSourceCategory', 'IncomeSourceCategoryController');
	Route::get('/incomes_category',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'IncomeSourceCategoryController@index']);


	//Manage Expenses
	Route::resource('incomes', 'IncomeSourceController');
	Route::get('/incomes_delete/{income}',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'IncomeSourceController@show_delete'])->name('incomes.delete');
	Route::get('/incomes',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'IncomeSourceController@index']);
	Route::get('/todays_income_history',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'IncomeSourceController@todays_income']);
	Route::get('/new_incomes',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'IncomeSourceController@create']);

	//Manage Give Loan
	Route::resource('giveLoan', 'giveLoanController');
	Route::get('/give_loan',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'giveLoanController@index']);
	Route::POST('/give_new_loan_payment',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'giveLoanController@giveNewLoan']);
	Route::POST('/give_return_loan_payment',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'giveLoanController@giveReturnLoan']);

	//Manage Receive Loan
	Route::resource('receiveLoan', 'receiveLoanController');
	Route::get('/receive_loan',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'receiveLoanController@index']);
	Route::POST('/receive_new_loan_payment',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'receiveLoanController@giveNewLoan']);
	Route::POST('/receive_return_loan_payment',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'receiveLoanController@giveReturnLoan']);

	//Manage Advance Payment
	Route::resource('advancePayment', 'advancePaymentController');
	Route::get('/manage_advance_payment',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'advancePaymentController@index']);
	Route::POST('/advance_new_expense',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'advancePaymentController@advanceNewExpense']);
	Route::POST('/give_new_advance_amount',['middleware'=>'check-permission:admin|superadmin|accountant','uses'=>'advancePaymentController@advanceNewPayment']);

	//Manage Bill Code
	Route::resource('billcodes', 'billcodeController');
	Route::get('/generate_bill_code',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'billcodeController@create']);
	Route::get('/bill_code_list',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'billcodeController@index']);
    Route::get('/barcode_pdf/{id}',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'billcodeController@download_pdf'])->name('billcode.download_pdf');

	//Manage Bill Code Items
	Route::resource('billcodeItems', 'BillcodeItemController');
	Route::POST('/billcodes_item/store',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'BillcodeItemController@store'])->name('billcodesitem.store');
    
    //AJAX Part
    Route::get('/billcodes/pi/items/{id}',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'orderController@get_item_by_pi']);
    Route::get('/ajax/pi-billcodes/{id}',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'AjaxController@get_billcodes_by_pi']);
    Route::get('/ajax/billcode-item/{id}',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'AjaxController@get_items_by_billcode']);


	//Manage Finish Part
	Route::resource('finishPart', 'FinishPartController');
	Route::get('/dyeing_chart',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'FinishPartController@index']);
	Route::get('/new_dyeing_information',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'FinishPartController@create'])->name('finishPart.add');
	Route::POST('/finish_part/store',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'FinishPartController@store'])->name('finishPart.store');
	Route::POST('/dyeing_chart_data',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'FinishPartController@finish_part_data'])->name('finish_part_data');

	//Manage Orders
	Route::resource('orders', 'orderController');
	Route::get('/order_list',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'orderController@index']);
	Route::get('/new_order',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'orderController@create']);
	Route::POST('/add_item_to_order',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'orderController@add_item_to_order']);
    Route::get('/order_pdf/{id}',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'orderController@download_pdf'])->name('order.order_details_pdf');
	Route::POST('/change_order_status',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'orderController@change_order_status']);
	
	//Manage Customer
	Route::resource('customer', 'CustomerController');
	Route::get('/customer_list',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'CustomerController@index']);
	Route::get('/new_customer',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'CustomerController@create']);
	//Report
    //Account report

    Route::get('/report/advance_balance',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'AccountReportController@advance_balance'])->name('report.advance_balance');
    Route::post('/report/advance_balance',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'AccountReportController@advance_balance_data']);
    Route::get('/report/loans',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'AccountReportController@loans_report'])->name('report.loans');
    Route::post('/report/loans',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'AccountReportController@loans_report_data']);


		
	//Manage Report
	Route::resource('report', 'reportController');

	// Merchandising Area
	Route::resource('merchandising', 'merchandisingController');
	Route::get('/production_report',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'merchandisingController@production_sheet']);
	Route::get('/cost_report',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'merchandisingController@cost_sheet']);
	Route::get('/greige_chart',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'merchandisingController@greige_chart_view']);
	Route::POST('/greige_chart_data',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'merchandisingController@greige_chart_data'])->name('greige_chart_data');
	Route::POST('/production_sheet',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'merchandisingController@production_sheet_data'])->name('production_sheet_data');

	Route::get('/new_stock_fabric',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'merchandisingController@new_stock_fabric'])->name('merchandising.stock_fabric.add');
	Route::get('/stock_fabric',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'merchandisingController@stock_fabric']);
	Route::POST('/stock_fabric_store',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'merchandisingController@stock_fabric_store'])->name('stock_fabric_store');
	Route::POST('/stock_fabric',['middleware'=>'check-permission:admin|superadmin|merchandiser','uses'=>'merchandisingController@stock_fabric_data'])->name('stock_fabric_data');


	//Commercial Management Area 
	Route::resource('commercialTask', 'CommercialController');
	Route::get('/new_lc',['middleware'=>'check-permission:admin|superadmin|commercial','uses'=>'CommercialController@create'])->name('lc.new_lc');
	Route::get('/lc_list',['middleware'=>'check-permission:admin|superadmin|commercial','uses'=>'CommercialController@lc_list_view'])->name('lc.list');
	Route::POST('/lc_data',['middleware'=>'check-permission:admin|superadmin|commercial','uses'=>'CommercialController@lc_list_data'])->name('lc.lc_list_data');
    Route::get('/lc_details/{id}',['middleware'=>'check-permission:admin|superadmin|commercial','uses'=>'CommercialController@show'])->name('lc.show');
    Route::get('/negotiation_application/{id}',['middleware'=>'check-permission:admin|superadmin|commercial','uses'=>'CommercialController@negotiation_application_pdf'])->name('commercial.negotiation_application_pdf');
    Route::get('/bill_of_exchange/{id}',['middleware'=>'check-permission:admin|superadmin|commercial','uses'=>'CommercialController@bill_of_exchange_pdf'])->name('commercial.bill_of_exchange_pdf');

    Route::get('/net_balance',['middleware'=>'check-permission:admin|superadmin','uses'=>'AccountReportController@net_balance'])->name('net_balance');


});